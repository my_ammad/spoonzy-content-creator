@extends('layouts.app')

@section('title') {{trans('general.verify_account')}} -@endsection

@section('content')
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="feather icon-check-circle mr-2"></i> {{trans('general.verify_account')}}</h2>
          <p class="lead text-muted mt-0">{{Auth::user()->verified_id != 'yes' ? trans('general.verified_account_desc_influencer') : trans('general.verified_account')}}</p>
        </div>
      </div>
      <div class="row">

        @include('includes.cards-settings')

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          @if (session('status'))
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    {{ session('status') }}
                  </div>
                @endif

          @include('errors.errors-forms')

        @if ($settings->requests_verify_account == 'on'
            && auth()->user()->verified_id != 'yes'
            && auth()->user()->verificationRequests() != 1
            && auth()->user()->verified_id != 'reject')

            @if (auth()->user()->countries_id != ''
                && auth()->user()->birthdate != ''
                && auth()->user()->cover != ''
                && auth()->user()->cover != $settings->cover_default
                && auth()->user()->avatar != $settings->avatar
              )

          <div class="alert alert-warning mr-1">
          <span class="alert-inner--text"><i class="fa fa-exclamation-triangle"></i> {{trans('general.warning_verification_info')}}</span>
        </div>

          <form method="POST" id="formVerify" action="{{ route('influencer_verification.send') }}" accept-charset="UTF-8" enctype="multipart/form-data">

            @csrf

          <div class="form-group">
            <div class="input-group mb-4">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-camera-retro"></i></span>
              </div>
              <input class="form-control" name="instagram_username" placeholder="{{trans('general.instagram_username')}}" value="{{old('instagram_username')}}" type="text">
            </div>
        </div>

            <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                  </div>
                  <input class="form-control" name="instagram_profile_link" placeholder="{{trans('general.instagram_profile_link')}}" value="{{old('instagram_profile_link')}}" type="text">
                </div>
              </div>

              <div class="form-group">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-calendar-week"></i></span>
                    </div>
                    <input class="form-control" name="birth_date" placeholder="{{trans('general.birth_date')}}" value="{{old('birth_date')}}" type="text">
                  </div>
                </div>

                <div class="form-group">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-envelope-open-text"></i></span>
                      </div>
                      <input class="form-control" name="email_address" placeholder="{{trans('general.email_address')}}" value="{{old('email_address')}}" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-link"></i></span>
                      </div>
                      <input class="form-control" name="link_content_creation_web" placeholder="{{trans('general.link_content_creation_web')}}" value="{{old('link_content_creation_web')}}" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-blog"></i></span>
                      </div>
                      <input class="form-control" name="platform_name_content_creation" placeholder="{{trans('general.platform_name_content_creation')}}" value="{{old('platform_name_content_creation')}}" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-link"></i></span>
                      </div>
                      <input class="form-control" name="promote_link" placeholder="{{trans('general.promote_link')}}" value="{{old('promote_link')}}" type="text">
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-link"></i></span>
                      </div>
                      <input class="form-control" name="twitter_link" placeholder="{{trans('general.twitter_link')}}" value="{{old('twitter_link')}}" type="text">
                    </div>
                  </div>

                {{-- @if (auth()->user()->countries_id == 1)
                  <div class="mb-3 text-center">
                    <span class="btn-block mb-2" id="previewImageFormW9"></span>

                      <input type="file" name="form_w9" id="fileVerifiyAccountFormW9" accept="application/pdf" class="visibility-hidden">
                      <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFileFormW9">{{trans('general.upload_form_w9')}} (PDF) {{trans('general.maximum')}}: {{Helper::formatBytes($settings->file_size_allowed_verify_account * 1024)}}</button>

                    <small class="text-muted btn-block">{{trans('general.form_w9_required')}}</small>
                    <h6 class="btn-block text-center font-weight-bold">
                      <a href="https://www.irs.gov/pub/irs-pdf/fw9.pdf" target="_blank">{{ __('general.complete_form_W9_here') }} <i class="feather icon-external-link"></i></a>
                    </h6>
                  </div>

                @endif --}}
                <div class="mb-3 text-center">
                    <span class="btn-block mb-2" id="previewImageInstaPhoto"></span>

                      <input type="file" name="fileVerifiyAccountInstaPhoto" id="fileVerifiyAccountInstaPhoto" accept="image/*,application/x-zip-compressed" class="visibility-hidden">
                      <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFileInstaPhoto">{{trans('general.upload_image')}} (JPG, PNG, GIF) {{trans('general.or')}} ZIP - {{trans('general.maximum')}}: {{Helper::formatBytes($settings->file_size_allowed_verify_account * 1024)}}</button>

                      <small class="text-muted btn-block">{{trans('general.insta_photo_upload_instruction')}}</small>
                  </div>

                <div class="mb-3 text-center">
                    <span class="btn-block mb-2" id="previewImageConCrSsWeb"></span>

                      <input type="file" name="fileVerifiyAccountConCrSsWeb" id="fileVerifiyAccountConCrSsWeb" accept="image/*,application/x-zip-compressed" class="visibility-hidden">
                      <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFilePhotoConCrSsWeb">{{trans('general.upload_image')}} (JPG, PNG, GIF) {{trans('general.or')}} ZIP - {{trans('general.maximum')}}: {{Helper::formatBytes($settings->file_size_allowed_verify_account * 1024)}}</button>

                      <small class="text-muted btn-block">{{trans('general.content_creation_web_ss_instruction')}}</small>
                </div>

                <div class="mb-3 text-center">
                    <span class="btn-block mb-2" id="previewImageTwittterSs"></span>

                      <input type="file" name="fileVerifiyAccountTwittterSs" id="fileVerifiyAccountTwittterSs" accept="image/*,application/x-zip-compressed" class="visibility-hidden">
                      <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFilePhotoTwittterSs">{{trans('general.upload_image')}} (JPG, PNG, GIF) {{trans('general.or')}} ZIP - {{trans('general.maximum')}}: {{Helper::formatBytes($settings->file_size_allowed_verify_account * 1024)}}</button>

                      <small class="text-muted btn-block">{{trans('general.twitter_massage_ss_instruction')}}</small>
                </div>

                <button class="btn btn-1 btn-success btn-block" id="sendData" type="submit">{{trans('general.send_approval')}}</button>
          </form>

        @else

          <div class="alert alert-danger">
          <span class="alert-inner--text"><i class="fa fa-exclamation-triangle mr-1"></i> {{trans('general.complete_profile_alert')}}</span>

          <ul class="list-unstyled">
            <br>

            @if (auth()->user()->avatar == $settings->avatar)
              <li>
                <i class="far fa-times-circle"></i> {{ __('general.set_avatar') }} <a href="{{ url(auth()->user()->username) }}" class="text-white link-border">{{ __('general.upload') }} <i class="feather icon-arrow-right"></i></a>
              </li>
            @endif

            @if (auth()->user()->cover == '' || auth()->user()->cover == $settings->cover_default)
            <li>
              <i class="far fa-times-circle"></i> {{ __('general.set_cover') }} <a href="{{ url(auth()->user()->username) }}" class="text-white link-border">{{ __('general.upload') }} <i class="feather icon-arrow-right"></i></a>
            </li>
          @endif

          @if (auth()->user()->countries_id == '')
            <li>
              <i class="far fa-times-circle"></i> {{ __('general.set_country') }} <a href="{{ url('settings/page') }}" class="text-white link-border">{{ __('admin.edit') }} <i class="feather icon-arrow-right"></i></a>
            </li>
            @endif

            @if (auth()->user()->birthdate == '')
            <li>
              <i class="far fa-times-circle"></i> {{ __('general.set_birthdate') }} <a href="{{ url('settings/page') }}" class="text-white link-border">{{ __('admin.edit') }} <i class="feather icon-arrow-right"></i></a>
            </li>
          @endif
          </ul>
        </div>

            @endif

        @elseif (auth()->user()->verificationRequests() == 1)
          <div class="alert alert-primary alert-dismissible text-center fade show" role="alert">
            <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
          <span class="alert-inner--text">{{trans('admin.pending_request_verify')}}</span>
        </div>
      @elseif (auth()->user()->verified_id == 'reject')
        <div class="alert alert-danger alert-dismissible text-center fade show" role="alert">
          <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
        <span class="alert-inner--text">{{trans('admin.rejected_request')}}</span>
      </div>
    @elseif (auth()->user()->verified_id != 'yes' && $settings->requests_verify_account == 'off')
      <div class="alert alert-primary alert-dismissible text-center fade show" role="alert">
        <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
      <span class="alert-inner--text">{{trans('general.info_receive_verification_requests')}}</span>
    </div>

        @else
          <div class="alert alert-success alert-dismissible text-center fade show" role="alert">
            <span class="alert-inner--icon mr-2"><i class="feather icon-check-circle"></i></span>
          <span class="alert-inner--text">{{trans('general.verified_account_success')}}</span>
        </div>

        @endif

        </div><!-- end col-md-6 -->
      </div>
    </div>
  </section>
@endsection
