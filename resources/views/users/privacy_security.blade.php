@extends('layouts.app')

@section('title') {{trans('general.privacy_security')}} -@endsection

@section('content')
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" class="bi bi-shield-check mr-2" viewBox="0 0 16 16">
  <path d="M5.338 1.59a61.44 61.44 0 0 0-2.837.856.481.481 0 0 0-.328.39c-.554 4.157.726 7.19 2.253 9.188a10.725 10.725 0 0 0 2.287 2.233c.346.244.652.42.893.533.12.057.218.095.293.118a.55.55 0 0 0 .101.025.615.615 0 0 0 .1-.025c.076-.023.174-.061.294-.118.24-.113.547-.29.893-.533a10.726 10.726 0 0 0 2.287-2.233c1.527-1.997 2.807-5.031 2.253-9.188a.48.48 0 0 0-.328-.39c-.651-.213-1.75-.56-2.837-.855C9.552 1.29 8.531 1.067 8 1.067c-.53 0-1.552.223-2.662.524zM5.072.56C6.157.265 7.31 0 8 0s1.843.265 2.928.56c1.11.3 2.229.655 2.887.87a1.54 1.54 0 0 1 1.044 1.262c.596 4.477-.787 7.795-2.465 9.99a11.775 11.775 0 0 1-2.517 2.453 7.159 7.159 0 0 1-1.048.625c-.28.132-.581.24-.829.24s-.548-.108-.829-.24a7.158 7.158 0 0 1-1.048-.625 11.777 11.777 0 0 1-2.517-2.453C1.928 10.487.545 7.169 1.141 2.692A1.54 1.54 0 0 1 2.185 1.43 62.456 62.456 0 0 1 5.072.56z"/>
  <path d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
</svg> {{trans('general.privacy_security')}}</h2>
          <p class="lead text-muted mt-0">{{trans('general.desc_privacy')}}</p>
        </div>
      </div>
      <div class="row">

        @include('includes.cards-settings')

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          @if (session('status'))
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    {{ session('status') }}
                  </div>
                @endif

          @include('errors.errors-forms')

          @if ($sessions)
          <h5>{{ __('general.login_sessions') }}</h5>
              <div class="card mb-4">
                <div class="card-body">
                  <small class="w-100 d-block"><strong>{{ __('general.last_login_record') }}</strong></small>
                  <p class="card-text">{{ $sessions->user_agent }}</p>
                  <p>
                    <span>IP: {{ $sessions->ip_address }}

            <span class="w-100 d-block mt-2">
              @if ($current_session_id == $sessions->id)
                <button type="button" :disabled="true" class="btn btn-sm btn-primary e-none">{{ __('general.this_device') }}</button>
                @else
                  <form method="POST" action="{{ url('logout/session', $sessions->id) }}">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger"><i class="feather icon-trash-2"></i> {{ __('general.delete') }}</button>
                  </form>
                @endif
            </span>
                  </p>
                </div>
              </div>
          @endif

          @if (auth()->user()->verified_id == 'yes')

            <h5>{{ __('general.privacy') }}</h5>

            <form method="POST" action="{{ url('privacy/security') }}">

              @csrf

              <div class="form-group">
                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_profile" value="yes" @if (auth()->user()->hide_profile == 'yes') checked @endif id="customSwitch1">
                    <label class="custom-control-label switch" for="customSwitch1">{{ __('general.hide_profile') }} {{ __('general.info_hide_profile') }}</label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_last_seen" value="yes" @if (auth()->user()->hide_last_seen == 'yes') checked @endif id="customSwitch2">
                    <label class="custom-control-label switch" for="customSwitch2">{{ __('general.hide_last_seen') }}</label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_count_subscribers" value="yes" @if (auth()->user()->hide_count_subscribers == 'yes') checked @endif id="customSwitch3">
                    <label class="custom-control-label switch" for="customSwitch3">{{ __('general.hide_count_subscribers') }}</label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_my_country" value="yes" @if (auth()->user()->hide_my_country == 'yes') checked @endif id="customSwitch4">
                    <label class="custom-control-label switch" for="customSwitch4">{{ __('general.hide_my_country') }}</label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="show_my_birthdate" value="yes" @if (auth()->user()->show_my_birthdate == 'yes') checked @endif id="customSwitch5">
                    <label class="custom-control-label switch" for="customSwitch5">{{ __('general.show_my_birthdate') }}</label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="private_profile" value="yes" @if (auth()->user()->private_profile == 'yes') checked @endif id="customSwitch6">
                    <label class="custom-control-label switch" for="customSwitch6">Private Profile</label>
                  </div>
                </div>

              </div><!-- End form-group -->

              <button class="btn btn-1 btn-success btn-block" onClick="this.form.submit(); this.disabled=true; this.innerText='{{ __('general.please_wait')}}';" type="submit">{{ __('general.save_changes')}}</button>

            </form>
          @endif

        </div><!-- end col-md-6 -->
      </div>
    </div>
  </section>
@endsection
