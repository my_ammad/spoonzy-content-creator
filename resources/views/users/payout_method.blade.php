@extends('layouts.app')

@section('title') {{trans('users.payout_method')}} -@endsection

@section('content')
<section class="section section-sm">
  <div class="container">
    <div class="row justify-content-center text-center mb-sm">
      <div class="col-lg-8 py-5">
        <h2 class="mb-0 font-montserrat"><i class="bi bi-credit-card mr-2"></i> {{trans('users.payout_method')}}</h2>
        <p class="lead text-muted mt-0">{{trans('general.default_payout_method')}}:
        @if(Auth::user()->payment_gateway == 'Stripe') <strong class="text-success">Stripe/Direct Deposit</strong>
         @endif
          @if(Auth::user()->payment_gateway != '') <strong class="text-success">{{Auth::user()->payment_gateway == 'PayPal' ? 'PayPal' : null}}</strong>
          @else <strong class="text-danger">{{trans('general.none')}}</strong> @endif
        </p>
      </div>
    </div>
    <div class="row">

      @include('includes.cards-settings')

      <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">
        @if (session('status'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          {{ session('status') }}
        </div>
        @endif

        @if (session('error_msg'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          {{ session('error_msg') }}
        </div>
        @endif

        @if (session('info'))
        <div class="alert alert-info">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          {{ session('info') }}
        </div>
        @endif

        @include('errors.errors-forms')

        @if (auth()->user()->verified_id != 'yes')
        <div class="alert alert-danger mb-3">
          <ul class="list-unstyled m-0">
            <li><i class="fa fa-exclamation-triangle"></i> {{trans('general.verified_account_info')}} <a href="{{url('settings/verify/account')}}" class="text-white link-border">{{trans('general.verify_account')}}</a></li>
          </ul>
        </div>
        @endif

        @if (true)
        <div class="row justify-content-center">

          @php

          // PayPal
          $buttonPayPal = null;
          $formPayPal = null;

          // Bank
          $buttonBank = null;
          $formBank = null;

          // Stripe
          $formStripe = null;
          $buttonStripe=null;

          if ($errors->has('stripe_details')) {

          // Stripe
          $buttonStripe = ' active';
          $formStripe = ' active show';

          // PayPal
          $buttonPayPal = null;
          $formPayPal = null;

          // Bank
          $buttonBank = null;
          $formBank = null;


          }
          if ($errors->has('bank_details')) {

          // Bank
          $buttonBank = ' active';
          $formBank = ' active show';

          // PayPal
          $buttonPayPal = null;
          $formPayPal = null;

          // Stripe
          $buttonStripe =null;
          $formStripe = null;


          }

          if ($errors->has('email_paypal') || $errors->has('email_paypal_confirmation')) {

          // PayPal
          $buttonPayPal = ' active';
          $formPayPal = ' active show';

          // Bank
          $buttonBank = null;
          $formBank = null;

          // Stripe
          $buttonStripe =null;
          $formStripe = null;
          }

          @endphp

          <div class="col-md-12">
            <div class="nav-wrapper">
              <ul class="nav nav-pills nav-fill flex-md-row" role="tablist">
                @if( $settings->payout_method_paypal == 'on' )
                <li class="nav-item">
                  <a class="nav-link link-nav mb-sm-4 mb-md-0 mb-2 p-4{{$buttonPayPal}}" id="btnPayPal" data-toggle="tab" href="#formPayPal" role="tab" aria-controls="formPayPal" aria-selected="true">
                    <i class="fab fa-paypal mr-2"></i> PayPal
                    @if (auth()->user()->payment_gateway == 'PayPal') <span class="badge badge-pill badge-success">{{ __('general.default') }}</span> @endif
                  </a>
                </li>
                @endif

                @if( $settings->payout_method_bank == 'on' )
                <li class="nav-item">
                  <a class="nav-link link-nav mb-sm-4 mb-md-0 p-4{{$buttonBank}}" id="btnBank" data-toggle="tab" href="#formBank" role="tab" aria-controls="formBank" aria-selected="false">
                    <i class="fa fa-university mr-2"></i> {{trans('users.bank_transfer')}}
                    @if (auth()->user()->payment_gateway == 'Bank') <span class="badge badge-pill badge-success">{{ __('general.default') }}</span> @endif
                  </a>
                </li>
                @endif
                @if( $settings->payout_method_stripe == 'on' )
                <li class="nav-item">
                  <a class="nav-link link-nav mb-sm-4 mb-md-0 p-4{{$buttonStripe}}" id="btnStripe" data-toggle="tab" href="#formStripe" role="tab" aria-controls="formBank" aria-selected="false">
                    <i class="fa fa-university mr-2"></i> Stripe/Direct Deposit
                    @if (auth()->user()->payment_gateway == 'Stripe') <span class="badge badge-pill badge-success">{{ __('general.default') }}</span> @endif
                  </a>
                </li>
                @endif
              </ul>
            </div><!-- END COL-MD-12 -->
          </div><!-- ./ ROW -->
        </div><!-- ./ nav-wrapper -->

        <div class="tab-content">

          @if( $settings->payout_method_paypal == 'on' )
          <!-- FORM PAYPAL -->
          <div id="formPayPal" class="tab-pane fade{{$formPayPal}}" role="tabpanel">
            <form method="POST" action="{{ url('settings/payout/method/paypal') }}">
              @csrf

              <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fab fa-paypal"></i></span>
                  </div>
                  <input class="form-control" name="email_paypal" value="{{Auth::user()->paypal_account == '' ? old('email_paypal') : Auth::user()->paypal_account}}" placeholder="{{trans('general.email_paypal')}}" required type="email">
                </div>
              </div>

              <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-envelope"></i></span>
                  </div>
                  <input class="form-control" name="email_paypal_confirmation" placeholder="{{trans('general.confirm_email_paypal')}}" required type="email">
                </div>
              </div>
              <button class="btn btn-1 btn-success btn-block" type="submit">{{trans('general.save_payout_method')}}</button>
            </form>
          </div>
          @endif

          @if( true )
          <!-- FORM BANK TRANSFER -->
          <div id="formBank" class="tab-pane fade{{$formBank}}" role="tabpanel">

            <div class="mt-3 col-md-8 offset-md-2">
              <?php
              $type = ['CHECKING', 'SAVINGS'];
              $name = "";
              $routing_no = "";
              $ac_no = "";
              $ac_type = "";
              $country = "";
              $city = "";
              $address = "";
              $post_code = "";
              if (isset($wise)) {
                $name = $wise->name;
                $routing_no = $wise->routing_no;
                $ac_no = $wise->ac_no;
                $ac_type = $wise->ac_type;
                $country = $wise->country;
                $city = $wise->city;
                $address = $wise->address;
                $post_code = $wise->post_code;
              }
              ?>
              <form action="{{ url('settings/payments/wise') }}" method="POST">
                @csrf
                <div class="form-group">
                  <label>Account Holder Name</label>
                  <input class="form-control" required name="name" value="{{ $name }}">
                </div>
                <div class="form-group">
                  <label>ACH Routing number</label>
                  <input class="form-control" required name="abartn" value="{{ $routing_no }}">
                </div>
                <div class="form-group">
                  <label>Bank account number</label>
                  <input class="form-control" name="ac_no" value="{{ $ac_no }}" required>
                </div>
                <div class="form-group">
                  <label>Acount Type</label>
                  <select class="form-control" name="ac_type">
                    <option value="">Please Select</option>
                    <?php
                    foreach ($type as $type) {
                      $selected = "";
                      if ($ac_type == $type) {
                        $selected = "selected";
                      }
                      echo "<option $selected>$type</option>";
                    }
                    ?>
                  </select>
                </div>
                <h5>Address</h5>
                <div class="row">
                  <div class="form-group col-md-6">
                    <label>Country</label>
                    <select class="form-control" name="country">
                      <option value="">Please Select</option>
                      <option value="US" @if($country) selected @endif>United State</option>
                    </select>
                  </div>
                  <div class="form-group col-md-6">
                    <label>City</label>
                    <input class="form-control" name="city" value="{{ $city }}" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Post code</label>
                    <input class="form-control" name="post_code" value="{{ $post_code }}" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label>Address</label>
                    <input class="form-control" name="address" value="{{ $address }}" required>
                  </div>
                </div>
                <div class="form-group">
                  <button class="btn btn-success btn-sm">Submit</button>
                </div>
              </form>
            </div>
            <!--     
          <form method="POST"  action="{{ url('settings/payout/method/bank') }}">

            @csrf
              <div class="form-group">
                <textarea name="bank_details" rows="5" cols="40" class="form-control" required placeholder="{{trans('users.bank_details')}}">{{Auth::user()->bank == '' ? old('bank_details') : Auth::user()->bank}}</textarea>
                </div>
                <button class="btn btn-1 btn-success btn-block" type="submit">{{trans('general.save_payout_method')}}</button>
          </form>
          !-->
          </div>
          @endif

          @if( true )
          <!-- FORM BANK TRANSFER -->
          <div id="formStripe" class="tab-pane fade{{$formStripe}}" role="tabpanel">
            <form method="POST" action="{{ url('settings/payout/method/stripe') }}">

              @csrf
              <div class="form-group">
                @if($stripeConnect)
                <button class="btn btn-1 btn-success btn-block" type="submit">Change Stripe Account</button>
                <input type="hidden" name='statusClick' value="stripeChange">
                @endif

                @if($stripeSubmited && !$stripeConnect)
                <button class="btn btn-1 btn-success btn-block" type="submit" disabled>Your Account under review</button>
                @endif

                @if(!$stripeConnect and !$stripeSubmited)
                <button class="btn btn-1 btn-success btn-block" type="submit">Connect Stripe</button>
                @endif
                <div style="
    padding-top: 30px;
">
                  <h4>Directions to link your Stripe account: </h4>
                  <p><span style="
    font-weight: bold;
">Step 1:</span> If you do not already have a Stripe account then you must either create on on Stripe.com or create your Stripe account via the portal below and link a payment method. If you already have an account then you may skip this step and move on to step 2.</p>
                  <p><span style="
    font-weight: bold;
">Step 2:</span> Link your existing Stripe account via the portal and fill out all information and fill it all out accurately. For the website portion of site, copy and paste your AllAdmirers.com page link. Also be sure to sign up as an individual NOT a business.
                    .</p>
                    <p><span style="
    font-weight: bold;
">Step 3:</span> Link a payout method/bank directly on Stripe to receive direct deposits.  This can be done either on Stripe.com or through our Stripe login portal.</p>
                    <p><span style="
    font-weight: bold;
">Step 4:</span> After you connect your Stripe account, you may be required to verify your account again directly through Stripe, after you submit your information.  To check, you must either login directly via our portal or on Stripe.com AFTER linking your Stripe account to AllAdmirers.  You will then have to submit additional information DIRECTLY through Stripe, NOT to us and they will verify your account after some time.</p>
<p style="
    font-weight: bold;
    font-size: 15px;
">(Normally, you will not require a business EIN at all for verification and will only need to re-verify your personal information directly on Stripe.  However, if you do specifically need an EIN for verification, you can create a business EIN for free via  https://www.irs.gov/businesses/small-businesses-self-employed/apply-for-an-employer-identification-number-ein-online .  Typically, you will only need an EIN if you sign up as a business rather than an individual (you should have signed up as an individual rather than a business on Stripe for our platform, as previously mentioned.) You will then need to select the option to save the EIN as a PDF, which is the option you choose and you can submit the PDF attachment directly through Stripe, as a verification document, Stripe specifically requires an EIN from you.)</p>
                </div>
            </form>
          </div>
          @endif


        </div><!-- ./ TAB-CONTENT -->
        @endif

      </div><!-- end col-md-6 -->

    </div>
  </div>
</section>
@endsection