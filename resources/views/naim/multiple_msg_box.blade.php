@extends('layouts.app')

@section('title')Multiple Messages -@endsection

@section('css')
    <script type="text/javascript">
        var subscribed_active = true;
        var user_id_chat = 1;
        var msg_count_chat = 0;
    </script>

    <style>
        .menuMobile {
            display: none;
        }

        @media screen and (max-width: 991px) {
            nav.navbar {
                display: none;
            }
        }

        footer.footer_background_color,
        .footer_background_color {
            display: none;
        }
    </style>
@endsection

@section('content')
    <section class="section section-sm pb-0 h-100 section-msg position-fixed">
        <div class="container h-100">
            <div class="row justify-content-center h-100">

                <div class="col-md-9 h-100 p-0">

                    <div class="card w-100 rounded-0 h-100 border-top-0">
                        <div class="card-header bg-white pt-4">
                            <div class="media">
                                <a href="{{url('messages')}}" class="mr-3"><i class="fa fa-arrow-left"></i></a>
                                @foreach($selectedUsers as $user)
                                    <span>
                                    <a href="{{url($user->username)}}" class="mr-3">
                                        {{--<strong style="position: absolute;top: 0px;">{{$user->hide_name == 'yes' ? $user->username : $user->name}}</strong>--}}
                                        <img src="{{Helper::getFile(config('path.avatar').$user->avatar)}}"
                                             class="rounded-circle" width="40" height="40">
                                         </a>
                                        </span>
                                @endforeach

                            </div>

                        </div>

                        <div class="content px-4 py-3 d-scrollbars container-msg" id="contentDIV" data="{{$user->id}}">
                        </div><!-- contentDIV -->

                        <div class="card-footer bg-white position-relative">

                            <div class="w-100 display-none" id="previewFile">
                                <div class="previewFile d-inline"></div>
                                <a href="javascript:;" class="text-danger" id="removeFile"><i
                                        class="fa fa-times-circle"></i></a>
                            </div>

                            <div class="progress-upload-cover" style="width: 0%; top:0;"></div>

                            <div class="blocked display-none"></div>

                            <!-- Alert -->
                            <div class="alert alert-danger my-3" id="errorMsg" style="display: none;">
                                <ul class="list-unstyled m-0" id="showErrorMsg"></ul>
                            </div><!-- Alert -->

                            <form action="{{ route('sendMutipleMsg') }}" method="post" accept-charset="UTF-8"
                                  id="formSendMsg" enctype="multipart/form-data">
                                <input type="hidden" name="ids" id="ids" value="{{$ids}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="file" name="photo" id="file"
                                       accept="image/*,video/mp4,video/x-m4v,video/quicktime,audio/mp3"
                                       style="visibility: hidden;">
                                <input type="file" name="zip" id="zipFile" accept="application/x-zip-compressed"
                                       class="visibility-hidden">

                                <div class="w-100 mr-2">
                                        <textarea class="form-control textareaAutoSize border-0"
                                                  data-post-length="{{$settings->update_length}}" rows="1"
                                                  placeholder="{{trans('general.write_something')}}" id="message"
                                                  name="message"></textarea>
                                </div>

                                <div class="form-group display-none mt-2" id="price">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">{{$settings->currency_symbol}}</span>
                                        </div>
                                        <input class="form-control isNumber" autocomplete="off" name="price"
                                               placeholder="{{trans('general.price')}}" type="text">
                                    </div>
                                </div><!-- End form-group -->

                                <div class="w-100">
                                    <span id="previewImage"></span>
                                    <a href="javascript:void(0)" id="removePhoto"
                                       class="text-danger p-1 px-2 display-none btn-tooltip" data-toggle="tooltip"
                                       data-placement="top" title="{{trans('general.delete')}}"><i
                                            class="fa fa-times-circle"></i></a>
                                </div>

                                <div class="justify-content-between align-items-center">

                                    <button type="button"
                                            class="btn btn-upload btn-tooltip e-none align-bottom @if (auth()->user()->dark_mode == 'off') text-primary @else text-white @endif rounded-pill"
                                            data-toggle="tooltip" data-placement="top"
                                            title="{{trans('general.upload_media')}}"
                                            onclick="$('#file').trigger('click')">
                                        <i class="feather icon-image f-size-25"></i>
                                    </button>

                                    <button type="button"
                                            class="btn btn-upload btn-tooltip e-none align-bottom @if (auth()->user()->dark_mode == 'off') text-primary @else text-white @endif rounded-pill"
                                            data-toggle="tooltip" data-placement="top"
                                            title="{{trans('general.upload_file_zip')}}"
                                            onclick="$('#zipFile').trigger('click')">
                                        <i class="bi bi-file-earmark-zip f-size-25"></i>
                                    </button>

                                    @if (auth()->user()->verified_id == 'yes')
                                        <button type="button" id="setPrice"
                                                class="btn btn-upload btn-tooltip e-none align-bottom @if (auth()->user()->dark_mode == 'off') text-primary @else text-white @endif rounded-pill"
                                                data-toggle="tooltip" data-placement="top"
                                                title="{{trans('general.set_price_for_msg')}}">
                                            <i class="fa fa-tag f-size-25"></i>
                                        </button>
                                    @endif

                                    @if ($user->verified_id == 'yes')
                                        <button type="button"
                                                class="btn btn-upload btn-tooltip e-none align-bottom @if (auth()->user()->dark_mode == 'off') text-primary @else text-white @endif rounded-pill"
                                                data-toggle="modal" title="{{trans('general.tip')}}"
                                                data-target="#tipForm"
                                                data-cover="{{Helper::getFile(config('path.cover').$user->cover)}}"
                                                data-avatar="{{Helper::getFile(config('path.avatar').$user->avatar)}}"
                                                data-name="{{$user->name}}" data-userid="{{$user->id}}">
                                            <i class="fa fa-donate mr-1 mr-lg-0 f-size-25"></i>
                                        </button>
                                    @endif

                                    <div class="d-inline-block float-right mt-3">
                                        <button type="submit" id="button-reply-msg" disabled
                                                data-send="{{ trans('auth.send') }}"
                                                data-wait="{{ trans('general.send_wait') }}"
                                                class="btn btn-sm btn-primary rounded-circle float-right e-none">
                                            <i class="far fa-paper-plane"></i>
                                        </button>
                                    </div>

                                </div><!-- media -->
                            </form>

                        </div><!-- card footer -->
                    </div><!-- card -->
                </div><!-- end col-md-6 -->
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script src="{{ asset('public/js/messages.js') }}?v={{$settings->version}}"></script>
@endsection
