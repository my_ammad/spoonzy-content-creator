@extends('admin.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           {{ trans('admin.admin') }} <i class="fa fa-angle-right margin-separator"></i> {{ trans('admin.verification_requests') }}
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

		    @if(Session::has('success_message'))
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> {{ Session::get('success_message') }}
		    </div>
		@endif

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	@if ($verify)
                   {{-- <tr>
                      <th class="active">ID</th>
                      <th class="active">{{ trans('admin.user') }}</th>
                      <th class="active">{{ trans('general.address') }}</th>
                      <th class="active">{{ trans('general.city') }}</th>
                      <th class="active">{{ trans('general.country') }}</th>
                      <th class="active">{{ trans('general.zip') }}</th>
                      <th class="active">{{ trans('general.image') }}</th>
                      <th class="active">{{ trans('general.form_w9') }}</th>
                      <th class="active">{{ trans('admin.date') }}</th>
                      <th class="active">{{ trans('admin.actions') }}</th>
                    </tr> --}}

                    <tr>
                        <td>Id</td>
                        <td>{{ $verify->id }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user') }}</td>
                        <td> @if ( ! isset($verify->user()->username))
                            <em>{{ trans('general.no_available') }}</em>
                          @else
                          <a href="{{ url($verify->user()->username) }}" target="_blank">{{ $verify->user()->name }}
                            <i class="fa fa-external-link-square-alt"></i>
                          </a>
                        @endif</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_inta_username') }}</td>
                        <td>{{ $verify->instagram_username }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_inta_profile_link') }}</td>
                        <td>{{ $verify->instagram_profile_link }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_birthdate') }}</td>
                        <td>{{ $verify->birth_date }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_email') }}</td>
                        <td>{{ $verify->email_address }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_con_creation_web_link') }}</td>
                        <td>{{ $verify->link_content_creation_web }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_content_crea_web') }}</td>
                        <td>{{ $verify->platform_name_content_creation }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_promotelink') }}</td>
                        <td>{{ $verify->promote_link }}</td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_twitter_link') }}</td>
                        <td>{{ $verify->twitter_link }}</td>
                    </tr>

                    <tr>
                        <td>{{ trans('admin.user_inta_ss') }}</td>
                        <td>
                            @if ($verify->instagram_ss)
                              <a href="{{ Helper::getFile(config('path.verification').$verify->instagram_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                              @else
                              <em>{{ __('general.not_applicable') }}</em>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>{{ trans('admin.user_content_creation_weblink') }}</td>
                        <td>
                            @if ($verify->content_creation_ss)
                              <a href="{{ Helper::getFile(config('path.verification').$verify->content_creation_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                              @else
                              <em>{{ __('general.not_applicable') }}</em>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td>{{ trans('admin.user_twitter_ss') }}</td>
                        <td>
                            @if ($verify->twitter_ss)
                              <a href="{{ Helper::getFile(config('path.verification').$verify->twitter_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                              @else
                              <em>{{ __('general.not_applicable') }}</em>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center"><a href="{{ url()->previous() }}" class="btn btn-danger btn-sm padding-btn">{{ trans('admin.back') }}</a></td>

                    </tr>


                    {{-- <tr>
                      <td>{{ $verify->id }}</td>
                      <td>
                        @if ( ! isset($verify->user()->username))
                          <em>{{ trans('general.no_available') }}</em>
                        @else
                        <a href="{{ url($verify->user()->username) }}" target="_blank">{{ $verify->user()->name }}
                          <i class="fa fa-external-link-square-alt"></i>
                        </a>
                      @endif
                      </td>
                      <td>{{ $verify->instagram_username }}</td>
                      <td>{{ $verify->instagram_profile_link }}</td>
                      <td>{{ $verify->birth_date }}</td>
                      <td>{{ $verify->email_address }}</td>
                      <td>{{ $verify->link_content_creation_web }}</td>
                      <td>{{ $verify->platform_name_content_creation }}</td>
                      <td>{{ $verify->promote_link }}</td>
                      <td>{{ $verify->twitter_link }}</td>
                      <td>
                        @if ($verify->instagram_ss)
                          <a href="{{ Helper::getFile(config('path.verification').$verify->instagram_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                          @else
                          <em>{{ __('general.not_applicable') }}</em>
                        @endif
                    </td>
                    <td>
                        @if ($verify->content_creation_ss)
                          <a href="{{ Helper::getFile(config('path.verification').$verify->content_creation_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                          @else
                          <em>{{ __('general.not_applicable') }}</em>
                        @endif
                    </td>
                    <td>
                        @if ($verify->twitter_ss)
                          <a href="{{ Helper::getFile(config('path.verification').$verify->twitter_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                          @else
                          <em>{{ __('general.not_applicable') }}</em>
                        @endif
                    </td>
                    </tr><!-- /.TR -->

                    <tr>
                        <td>{{ $verify->id }}</td>
                        <td>
                          @if ( ! isset($verify->user()->username))
                            <em>{{ trans('general.no_available') }}</em>
                          @else
                          <a href="{{ url($verify->user()->username) }}" target="_blank">{{ $verify->user()->name }}
                            <i class="fa fa-external-link-square-alt"></i>
                          </a>
                        @endif
                        </td>
                        <td>{{ $verify->instagram_username }}</td>
                        <td>{{ $verify->instagram_profile_link }}</td>
                        <td>{{ $verify->birth_date }}</td>
                        <td>{{ $verify->email_address }}</td>
                        <td>{{ $verify->link_content_creation_web }}</td>
                        <td>{{ $verify->platform_name_content_creation }}</td>
                        <td>{{ $verify->promote_link }}</td>
                        <td>{{ $verify->twitter_link }}</td>
                        <td>
                          @if ($verify->instagram_ss)
                            <a href="{{ Helper::getFile(config('path.verification').$verify->instagram_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                            @else
                            <em>{{ __('general.not_applicable') }}</em>
                          @endif
                      </td>
                      <td>
                          @if ($verify->content_creation_ss)
                            <a href="{{ Helper::getFile(config('path.verification').$verify->content_creation_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                            @else
                            <em>{{ __('general.not_applicable') }}</em>
                          @endif
                      </td>
                      <td>
                          @if ($verify->twitter_ss)
                            <a href="{{ Helper::getFile(config('path.verification').$verify->twitter_ss) }}" target="_blank">{{ trans('admin.see_document_id') }} <i class="fa fa-external-link-square-alt"></i></a>
                            @else
                            <em>{{ __('general.not_applicable') }}</em>
                          @endif
                      </td>
                      </tr><!-- /.TR --> --}}

                    @else
                    	<h3 class="text-center no-found">{{ trans('general.no_results_found') }}</h3>
                    @endif

                  </tbody>
                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
@endsection
