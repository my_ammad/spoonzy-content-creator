<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;" align="center">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <!-- Logo -->
                    <tr>
                        <td style="padding: 25px 0; text-align: center;">
                            <a style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;" href="{{url('/')}}" target="_blank">
                                {{$title_site}}
                            </a>
                        </td>
                    </tr>

        <!-- Email Body -->
        <tr>
            <td style="width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;" width="100%">
                <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0;" align="center" width="570" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; padding: 35px;">
                            <!-- Greeting -->
                            <h1 style="margin-top: 0; color: #2F3133; font-size: 19px; font-weight: bold; text-align: left;">
                              {{trans('emails.hello')}} {{$fullname}}
                            </h1>

                            <!-- Intro -->
                            <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">
                                <p>Congratulations on your account being verified. Please note that according to both the law and the terms that you agreed upon that United States citizens are required to file their income and taxes to the IRS that specifically are directly related their earnings on the AllAdmirers platform. It is each user's responsibility to fill out their own form that we provide them with and file their own taxes and paperwork. We respect the privacy of all of our users so you will send the forms attached to the IRS and NOT to us since these forms and your taxes are your own responsibility. As per our terms, you agree to submit all forms and paperwork to the IRS in a on time and whenever your taxes are due to the IRS. </p>
                                <p>Attached are W-9, 1099-NEC, Schedule C 1040 and Schedule SE 1040 forms. </p>
                                <p>Our business address to fill out is:</p>
                                <p>400 E 57th St, New York, NY 10022</p>
                                <p>and our Payer's tin is: 87-2145233. </p>
                                <p>If you are not a United States resident nor citizen then please note that the aforementioned does not apply to you.</p>
                                <p>Best,</p>
                                <p>AllAdmirers Admin Team</p>
                            </p>

                            <!-- Salutation -->
                            <!--<p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 1.5em;">-->
                            <!--    {{trans('emails.regards')}}<br>{{$title_site}}-->
                            <!--</p>-->
                          </td>
                    </tr>
                </table>
            </td>
        </tr>

          <!-- Footer -->
            <tr>
                <td>
                    <table style="width: auto; max-width: 570px; margin: 0 auto; padding: 0; text-align: center;" align="center" width="570" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="font-family: Arial, &#039;Helvetica Neue&#039;, Helvetica, sans-serif; color: #AEAEAE; padding: 35px; text-align: center;">
                                <p style="margin-top: 0; color: #74787E; font-size: 12px; line-height: 1.5em;">
                                    &copy; <?php echo date('Y'); ?>
                                    {{$title_site}}
                                    {{trans('emails.rights_reserved')}}
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</body>
</html>
