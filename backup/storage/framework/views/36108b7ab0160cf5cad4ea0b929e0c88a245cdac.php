

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css')); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('public/plugins/colorpicker/bootstrap-colorpicker.min.css')); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin')); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.theme')); ?>

          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo e(trans('admin.theme')); ?></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="post" action="<?php echo e(url('panel/admin/theme')); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                  <?php if(session('success_message')): ?>
                    <div class="box-body">
          		    <div class="alert alert-success">
          		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
          								<span aria-hidden="true">×</span>
          								</button>
          		        <i class="fa fa-check margin-separator"></i> <?php echo e(session('success_message'), false); ?>

          		    </div>
                  </div>
          		<?php endif; ?>

              <?php if(session('error_max_upload_size')): ?>
                <div class="alert alert-danger">
        		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        								<span aria-hidden="true">×</span>
        								</button>
        		      <i class="fa fa-warning margin-separator"></i>  <?php echo e(trans('general.max_upload_files', ['post_size' => ini_get("post_max_size")."B"] ), false); ?>

        		    </div>
              <?php endif; ?>

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

          <!-- Start Box Body -->
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-2 control-label"><?php echo e(trans('general.home_style'), false); ?></label>
              <div class="col-sm-10">
                <div class="radio">
                <label class="padding-zero">
                  <input type="radio" value="0" name="home_style" <?php if($settings->home_style == 0): ?> checked="checked" <?php endif; ?> checked>
                  <img src="<?php echo e(url('/public/img/homepage-1.jpg'), false); ?>">
                </label>
              </div>
              <div class="radio">
                <label class="padding-zero">
                  <input type="radio" value="1" name="home_style" <?php if($settings->home_style == 1): ?> checked="checked" <?php endif; ?>>
                  <img src="<?php echo e(url('/public/img/homepage-2.jpg'), false); ?>">
                </label>
              </div>
              </div>
            </div>
          </div><!-- /.box-body -->


                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.logo'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->logo), false); ?>" class="logo-theme">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="logo" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.note_logo_white'), false); ?></p>
                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 487x144 px (PNG)</p>

              <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer" id="fileContainerLogo">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.logo_blue'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->logo_2), false); ?>" class="w-150">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="logo_2" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 487x144 px (PNG)</p>

              <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer" id="fileContainerLogo">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Favicon</label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->favicon), false); ?>">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="favicon" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 48x48 px (PNG)</p>

                      <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.index_image_top'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->home_index), false); ?>" class="w-200">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="index_image_top" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 884x592 px</p>

                      <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.background'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->bg_gradient), false); ?>" class="w-400">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="background" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 1441x480 px</p>

                        <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
          					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
          					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
          					    </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.image_index_1'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->img_1), false); ?>" class="w-120">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="image_index_1" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 120x120 px</p>

                        <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
          					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
          					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
          					    </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.image_index_2'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->img_2), false); ?>" class="w-120">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="image_index_2" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 120x120 px</p>

                        <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
          					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
          					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
          					    </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.image_index_3'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->img_3), false); ?>" class="w-120">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="image_index_3" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 120x120 px</p>

                        <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
          					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
          					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
          					    </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.image_index_4'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(url('/public/img', $settings->img_4), false); ?>" class="w-120">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="image_index_4" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 362x433 px</p>

                        <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
          					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
          					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
          					    </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.avatar_default'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="btn-block margin-bottom-10">
                          <img src="<?php echo e(Helper::getFile(config('path.avatar').$settings->avatar), false); ?>" class="w-200">
                        </div>

                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="avatar" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 250x250 px</p>

                      <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.cover_default'), false); ?></label>
                      <div class="col-sm-10">
                          <div class="btn-block margin-bottom-10">
                            <div style="max-width: 400px; height: 150px; display: block; border-radius: 6px; background: #505050 <?php if($settings->cover_default): ?> url('<?php echo e(Helper::getFile(config('path.cover').$settings->cover_default), false); ?>') no-repeat center center; background-size: cover; <?php endif; ?> ;">
                            </div>
                          </div>
                          
                      	<div class="btn btn-info box-file">
                      		<input type="file" accept="image/*" name="cover_default" class="filePhoto" />
                      		<i class="glyphicon glyphicon-cloud-upload myicon-right"></i>
                          <span class="text-file"><?php echo e(trans('general.choose_image'), false); ?></span>
                      		</div>

                      <p class="help-block"><?php echo e(trans('general.recommended_size'), false); ?> 1500x800 px</p>

                      <div class="btn-default btn-lg btn-border btn-block pull-left text-left display-none fileContainer">
					     	<i class="glyphicon glyphicon-paperclip myicon-right"></i>
					     	<small class="myicon-right file-name-file"></small> <i class="icon-cancel-circle far fa-times-circle delete-image btn pull-right" title="<?php echo e(trans('general.delete'), false); ?>"></i>
					     </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <!-- Color Picker -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo app('translator')->get('admin.default_color'); ?>:</label>

                        <div class="col-sm-2">
                        <div class="input-group my-colorpicker2">
                          <div class="input-group-addon">
                            <i></i>
                          </div>
                          <input type="text" name="color" value="<?php echo e($settings->color_default, false); ?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                      </div>
                      </div>
                      <!-- /.form group -->
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <!-- Color Picker -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo app('translator')->get('general.navbar_background_color'); ?>:</label>

                        <div class="col-sm-2">
                        <div class="input-group my-colorpicker2">
                          <div class="input-group-addon">
                            <i></i>
                          </div>
                          <input type="text" name="navbar_background_color" value="<?php echo e($settings->navbar_background_color, false); ?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                      </div>
                      </div>
                      <!-- /.form group -->
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <!-- Color Picker -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo app('translator')->get('general.navbar_text_color'); ?>:</label>

                        <div class="col-sm-2">
                        <div class="input-group my-colorpicker2">
                          <div class="input-group-addon">
                            <i></i>
                          </div>
                          <input type="text" name="navbar_text_color" value="<?php echo e($settings->navbar_text_color, false); ?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                      </div>
                      </div>
                      <!-- /.form group -->
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <!-- Color Picker -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo app('translator')->get('general.footer_background_color'); ?>:</label>

                        <div class="col-sm-2">
                        <div class="input-group my-colorpicker2">
                          <div class="input-group-addon">
                            <i></i>
                          </div>
                          <input type="text" name="footer_background_color" value="<?php echo e($settings->footer_background_color, false); ?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                      </div>
                      </div>
                      <!-- /.form group -->
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <!-- Color Picker -->
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo app('translator')->get('general.footer_text_color'); ?>:</label>

                        <div class="col-sm-2">
                        <div class="input-group my-colorpicker2">
                          <div class="input-group-addon">
                            <i></i>
                          </div>
                          <input type="text" name="footer_text_color" value="<?php echo e($settings->footer_text_color, false); ?>" class="form-control">
                        </div>
                        <!-- /.input group -->
                      </div>
                      </div>
                      <!-- /.form group -->
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.button_style'), false); ?></label>
                      <div class="col-sm-10">
                        <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" value="rounded" name="button_style" <?php if($settings->button_style == 'rounded'): ?> checked="checked" <?php endif; ?> checked>
                          <?php echo e(trans('general.button_style_rounded'), false); ?>

                        </label>
                      </div>
                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" value="normal" name="button_style" <?php if($settings->button_style == 'normal'): ?> checked="checked" <?php endif; ?>>
                          <?php echo e(trans('admin.normal'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <hr>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-success pull-right"><?php echo e(trans('admin.save')); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
        		</div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/theme.blade.php ENDPATH**/ ?>