<?php $__currentLoopData = $updates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $response): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <?php if(auth()->guard()->check()): ?>
        <?php
            $checkUserSubscription = auth()->user()->checkSubscription($response->user());
            $checkPayPerView = auth()->user()->payPerView()->where('updates_id', $response->id)->first();
        ?>
    <?php endif; ?>

    <div class="card mb-3 card-updates" data="<?php echo e($response->id, false); ?>">
        <div class="card-body">
            <div
                class="pinned_post text-muted small w-100 mb-2 <?php echo e($response->fixed_post == '1' && request()->path() == $response->user()->username ? 'pinned-current' : 'display-none', false); ?>">
                <i class="bi bi-pin mr-2"></i> <?php echo e(trans('general.pinned_post'), false); ?>

            </div>
            <div class="media">
		<span class="rounded-circle mr-3">
			<a href="<?php echo e(url($response->user()->username), false); ?>">
				<img src="<?php echo e(Helper::getFile(config('path.avatar').$response->user()->avatar), false); ?>"
                     alt="<?php echo e($response->user()->hide_name == 'yes' ? $response->user()->username : $response->user()->name, false); ?>"
                     class="rounded-circle avatarUser" width="60" height="60">
				</a>
		</span>

                <div class="media-body">
                    <h5 class="mb-0 font-montserrat">
                        <a href="<?php echo e(url($response->user()->username), false); ?>">
                            <?php echo e($response->user()->hide_name == 'yes' ? $response->user()->username : $response->user()->name, false); ?>

                        </a>

                        <?php if($response->user()->verified_id == 'yes'): ?>
                            <small class="verified" title="<?php echo e(trans('general.verified_account'), false); ?>" data-toggle="tooltip"
                                   data-placement="top">
                                <i class="feather icon-check-circle"></i>
                            </small>
                        <?php endif; ?>

                        <small class="text-muted"><?php echo e('@'.$response->user()->username, false); ?></small>

                        <?php if(auth()->check() && auth()->user()->id == $response->user()->id): ?>
                            <a href="javascript:void(0);" class="text-muted float-right" id="dropdown_options"
                               role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>

                            <!-- Target -->
                            <button class="d-none copy-url" id="url<?php echo e($response->id, false); ?>"
                                    data-clipboard-text="<?php echo e(url($response->user()->username.'/post', $response->id), false); ?>"><?php echo e(trans('general.copy_link'), false); ?></button>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_options">
                                <?php if(request()->path() != $response->user()->username.'/post/'.$response->id): ?>
                                    <a class="dropdown-item"
                                       href="<?php echo e(url($response->user()->username.'/post', $response->id), false); ?>"><?php echo e(trans('general.go_to_post'), false); ?></a>
                                <?php endif; ?>

                                <a class="dropdown-item pin-post" href="javascript:void(0);"
                                   data-id="<?php echo e($response->id, false); ?>">
                                    <?php echo e($response->fixed_post == '0' ? trans('general.pin_to_your_profile') : trans('general.unpin_from_profile'), false); ?>

                                </a>

                                <button class="dropdown-item"
                                        onclick="$('#url<?php echo e($response->id, false); ?>').trigger('click')"><?php echo e(trans('general.copy_link'), false); ?></button>
                                <a class="dropdown-item"
                                   href="<?php echo e(url('update/edit',$response->id), false); ?>"><?php echo e(trans('general.edit_post'), false); ?></a>
                                <?php echo Form::open([
                                    'method' => 'POST',
                                    'url' => "update/delete/$response->id",
                                    'class' => 'd-inline'
                                ]); ?>


                                <?php if(isset($inPostDetail)): ?>
                                    <?php echo Form::hidden('inPostDetail', 'true'); ?>

                                <?php endif; ?>

                                <?php echo Form::button(trans('general.delete_post'), ['class' => 'dropdown-item actionDelete']); ?>

                                <?php echo Form::close(); ?>


                                <a class="dropdown-item"
                                   href="<?php echo e(route('single_post_inspection', [$response->id, $response->user()->id]), false); ?>" target="_blank">Post
                                    Inspection</a>
                            </div>
                        <?php endif; ?>

                        <?php if(auth()->check()
                            && auth()->user()->id != $response->user()->id
                            && $response->locked == 'yes'
                            && $checkUserSubscription && $response->price == 0.00

                            || auth()->check()
                                && auth()->user()->id != $response->user()->id
                                && $response->locked == 'yes'
                                && $checkUserSubscription
                                && $checkUserSubscription->free == 'yes'
                                && $response->price != 0.00
                                && $checkPayPerView

                            || auth()->check()
                                && auth()->user()->id != $response->user()->id
                                && $response->price != 0.00
                                && $checkPayPerView

                            || auth()->check() && auth()->user()->id != $response->user()->id && auth()->user()->role == 'admin' && auth()->user()->permission == 'all'
                            || auth()->check() && auth()->user()->id != $response->user()->id && $response->locked == 'no'
                            ): ?>
                            <a href="javascript:void(0);" class="text-muted float-right" id="dropdown_options"
                               role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown_options">

                                <!-- Target -->
                                <button class="d-none copy-url" id="url<?php echo e($response->id, false); ?>"
                                        data-clipboard-text="<?php echo e(url($response->user()->username.'/post', $response->id), false); ?>"><?php echo e(trans('general.copy_link'), false); ?></button>

                                <?php if(request()->path() != $response->user()->username.'/post/'.$response->id): ?>
                                    <a class="dropdown-item"
                                       href="<?php echo e(url($response->user()->username.'/post', $response->id), false); ?>"><?php echo e(trans('general.go_to_post'), false); ?></a>
                                <?php endif; ?>

                                <button class="dropdown-item"
                                        onclick="$('#url<?php echo e($response->id, false); ?>').trigger('click')"><?php echo e(trans('general.copy_link'), false); ?></button>

                                <button type="button" class="dropdown-item" data-toggle="modal"
                                        data-target="#reportUpdate<?php echo e($response->id, false); ?>">
                                    <?php echo e(trans('admin.report'), false); ?>

                                </button>

                            </div>

                            <div class="modal fade modalReport" id="reportUpdate<?php echo e($response->id, false); ?>" tabindex="-1"
                                 role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-danger modal-xs">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6 class="modal-title font-weight-light" id="modal-title-default"><i
                                                    class="fas fa-flag mr-1"></i> <?php echo e(trans('admin.report_update'), false); ?></h6>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>

                                        <!-- form start -->
                                        <form method="POST" action="<?php echo e(url('report/update', $response->id), false); ?>"
                                              enctype="multipart/form-data">
                                            <div class="modal-body">
                                            <?php echo csrf_field(); ?>
                                            <!-- Start Form Group -->
                                                <div class="form-group">
                                                    <label><?php echo e(trans('admin.please_reason'), false); ?></label>
                                                    <select name="reason" class="form-control custom-select">
                                                        <option value="copyright"><?php echo e(trans('admin.copyright'), false); ?></option>
                                                        <option
                                                            value="privacy_issue"><?php echo e(trans('admin.privacy_issue'), false); ?></option>
                                                        <option
                                                            value="violent_sexual"><?php echo e(trans('admin.violent_sexual_content'), false); ?></option>
                                                    </select>
                                                </div><!-- /.form-group-->
                                            </div><!-- Modal body -->

                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-xs btn-white sendReport">
                                                    <i></i> <?php echo e(trans('admin.report_update'), false); ?></button>
                                                <button type="button" class="btn e-none text-white ml-auto"
                                                        data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                                            </div>
                                        </form>
                                    </div><!-- Modal content -->
                                </div><!-- Modal dialog -->
                            </div><!-- Modal -->
                        <?php endif; ?>
                    </h5>

                    <small class="timeAgo text-muted" data="<?php echo e(date('c', strtotime($response->date)), false); ?>"></small>
                    <?php if($response->locked == 'yes'): ?>

                        <small class="text-muted" title="<?php echo e(trans('users.content_locked'), false); ?>">

                            <i class="feather icon-lock mr-1"></i>

                            <?php if(auth()->check() && $response->price != 0.00): ?>
                                <?php echo e(Helper::amountFormatDecimal($response->price), false); ?>

                            <?php endif; ?>
                        </small>
                    <?php endif; ?>
                </div><!-- media body -->
            </div><!-- media -->
        </div><!-- card body -->

        <?php if(auth()->check() && auth()->user()->id == $response->user()->id
            || $response->locked == 'yes' && $response->image != ''
            || $response->locked == 'yes' && $response->video != ''
            || $response->locked == 'yes' && $response->music != ''
            || $response->locked == 'yes' && $response->file != ''
            || $response->locked == 'yes' && $response->video_embed != ''

            || auth()->check() && $response->locked == 'yes'
            && $checkUserSubscription
            && $response->price == 0.00

            || auth()->check() && $response->locked == 'yes'
            && $checkUserSubscription
            && $checkUserSubscription->free == 'yes'
            && $response->price != 0.00
            && $checkPayPerView

            || auth()->check() && $response->locked == 'yes'
            && $response->price != 0.00
            && $checkPayPerView

            || auth()->check() && auth()->user()->role == 'admin' && auth()->user()->permission == 'all'
            || $response->locked == 'no'
            ): ?>
            <div class="card-body pt-0 pb-3">
                <p class="mb-0 update-text position-relative text-word-break">
                    <?php echo Helper::linkText(Helper::checkText($response->description, $response->video_embed)); ?>

                </p>
            </div>
        <?php endif; ?>

        <?php if(auth()->check() && auth()->user()->id == $response->user()->id

        || auth()->check() && $response->locked == 'yes'
        && $checkUserSubscription
        && $response->price == 0.00

        || auth()->check() && $response->locked == 'yes'
        && $checkUserSubscription
        && $checkUserSubscription->free == 'yes'
        && $response->price != 0.00
        && $checkPayPerView

        || auth()->check() && $response->locked == 'yes'
        && $response->price != 0.00
        && $checkPayPerView

        || auth()->check() && auth()->user()->role == 'admin' && auth()->user()->permission == 'all'
        || $response->locked == 'no'
        ): ?>

            <div class="btn-block">

                <?php if($response->image != ''): ?>

                    <?php
                        $urlImg =  Helper::getFile(config('path.images').$response->image);
                    ?>

                    <a onclick="inspectPost('<?php echo e($response->id, false); ?>','Image', '<?php echo e($urlImg, false); ?>', '<?php echo e($response->user_id, false); ?>')" href="<?php echo e($urlImg, false); ?>"
                       data-group="gallery<?php echo e($response->id, false); ?>" class="js-smartPhoto w-100">
                        <img src="<?php echo e($urlImg, false); ?>?w=130&h=100" data-src="<?php echo e($urlImg, false); ?>?w=960&h=980"
                             class="img-fluid lazyload d-inline-block w-100" alt="<?php echo e(e($response->description), false); ?>">
                    </a>
                <?php endif; ?>

                <?php if($response->video != ''): ?>
                    <video data-userId="<?php echo e($response->user_id, false); ?>" data-id="<?php echo e($response->id, false); ?>" id="video-<?php echo e($response->id, false); ?>"
                           class="post-videos js-player w-100 <?php if(!request()->ajax()): ?>invisible <?php endif; ?>" controls>
                        <source src="<?php echo e(Helper::getFile(config('path.videos').$response->video), false); ?>" type="video/mp4"/>
                    </video>
                <?php endif; ?>

                <?php if($response->music != ''): ?>
                    <div class="mx-3 border rounded">
                        <audio id="music-<?php echo e($response->id, false); ?>"
                               class="js-player w-100 <?php if(!request()->ajax()): ?>invisible <?php endif; ?>" controls>
                            <source src="<?php echo e(Helper::getFile(config('path.music').$response->music), false); ?>" type="audio/mp3">
                            Your browser does not support the audio tag.
                        </audio>
                    </div>
                <?php endif; ?>

                <?php if($response->file != ''): ?>
                    <a href="<?php echo e(url('download/file', $response->id), false); ?>" class="d-block text-decoration-none">
                        <div class="card mb-3 mx-3">
                            <div class="row no-gutters">
                                <div class="col-md-2 text-center bg-primary">
                                    <i class="far fa-file-archive m-4 text-white" style="font-size: 48px;"></i>
                                </div>
                                <div class="col-md-10">
                                    <div class="card-body">
                                        <h5 class="card-title text-primary text-truncate mb-0">
                                            <?php echo e($response->file_name, false); ?>.zip
                                        </h5>
                                        <p class="card-text">
                                            <small class="text-muted"><?php echo e($response->file_size, false); ?></small>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>

                <?php if($response->video_embed != '' && in_array(Helper::videoUrl($response->video_embed), array('youtube.com','www.youtube.com','youtu.be','www.youtu.be'))): ?>
                    <div class="embed-responsive embed-responsive-16by9 mb-2">
                        <iframe class="embed-responsive-item" height="360"
                                src="https://www.youtube.com/embed/<?php echo e(Helper::getYoutubeId($response->video_embed), false); ?>"
                                allowfullscreen></iframe>
                    </div>
                <?php endif; ?>

                <?php if($response->video_embed != '' && in_array(Helper::videoUrl($response->video_embed), array('vimeo.com','player.vimeo.com'))): ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item"
                                src="https://player.vimeo.com/video/<?php echo e(Helper::getVimeoId($response->video_embed), false); ?>"
                                allowfullscreen></iframe>
                    </div>
                <?php endif; ?>

            </div><!-- btn-block -->

        <?php else: ?>

            <div class="btn-block p-sm text-center content-locked pt-lg pb-lg px-3">
                <span class="btn-block text-center mb-3"><i class="feather icon-lock ico-no-result border-0"></i></span>

                <?php if($response->user()->price != 0.00 && $response->price == 0.00
                        || $response->user()->free_subscription == 'yes' && $response->price == 0.00): ?>
                    <a href="javascript:void(0);" <?php if(auth()->guard()->guest()): ?> data-toggle="modal" data-target="#loginFormModal"
                       <?php else: ?> <?php if($response->user()->free_subscription == 'yes'): ?> data-toggle="modal"
                       data-target="#subscriptionFreeForm" <?php else: ?> data-toggle="modal" data-target="#subscriptionForm"
                       <?php endif; ?> <?php endif; ?> class="btn btn-primary w-100">
                        <?php echo e(trans('general.content_locked_user_logged'), false); ?>

                    </a>
                <?php elseif($response->user()->price != 0.00 && $response->price != 0.00
                        || $response->user()->free_subscription == 'yes' && $response->price != 0.00): ?>
                    <a href="javascript:void(0);" <?php if(auth()->guard()->guest()): ?> data-toggle="modal" data-target="#loginFormModal"
                       <?php else: ?> data-toggle="modal" data-target="#payPerViewForm" data-mediaid="<?php echo e($response->id, false); ?>"
                       data-price="<?php echo e(Helper::amountFormatDecimal($response->price), false); ?>"
                       data-pricegross="<?php echo e($response->price, false); ?>" <?php endif; ?> class="btn btn-primary w-100">
                        <?php if(auth()->guard()->guest()): ?>
                            <?php echo e(trans('general.content_locked_user_logged'), false); ?>

                        <?php else: ?>
                            <i class="feather icon-unlock mr-1"></i> <?php echo e(trans('general.unlock_post_for'), false); ?> <?php echo e(Helper::amountFormatDecimal($response->price), false); ?>

                        <?php endif; ?>
                    </a>
                <?php else: ?>
                    <a href="javascript:void(0);" class="btn btn-primary disabled w-100">
                        <?php echo e(trans('general.subscription_not_available'), false); ?>

                    </a>
                <?php endif; ?>


                <?php if($response->image != ''): ?>
                    <h6 class="btn-block mt-2 font-weight-light"><i
                            class="feather icon-image"></i> <?php echo e(__('general.photo'), false); ?></h6>
                <?php endif; ?>

                <?php if($response->video != '' || $response->video_embed): ?>
                    <h6 class="btn-block mt-2 font-weight-light"><i
                            class="feather icon-video"></i> <?php echo e(__('general.video'), false); ?></h6>
                <?php endif; ?>

                <?php if($response->music != ''): ?>
                    <h6 class="btn-block mt-2 font-weight-light"><i
                            class="feather icon-mic"></i> <?php echo e(__('general.audio'), false); ?></h6>
                <?php endif; ?>

                <?php if($response->file != ''): ?>
                    <h6 class="btn-block mt-2 font-weight-light"><i
                            class="far fa-file-archive"></i> <?php echo e(__('general.file'), false); ?> - <?php echo e($response->file_size, false); ?></h6>
                <?php endif; ?>

                <?php if($response->image == ''
                        && $response->video == ''
                        && ! $response->video_embed
                        && $response->music == ''
                        && $response->file == ''
                        ): ?>
                    <h6 class="btn-block mt-2 font-weight-light"><i
                            class="feather icon-file-text"></i> <?php echo e(__('admin.text'), false); ?></h6>
                <?php endif; ?>

            </div>
        <?php endif; ?>

        <div class="card-footer bg-white border-top-0">
            <h4>
                <?php
                    $likeActive = auth()->check() && auth()->user()->likes()->where('updates_id', $response->id)->where('status','1')->first();
                    $bookmarkActive = auth()->check() && auth()->user()->bookmarks()->where('updates_id', $response->id)->first();

                    if(auth()->check() && auth()->user()->id == $response->user()->id

                    || auth()->check() && $response->locked == 'yes'
                    && $checkUserSubscription
                    && $response->price == 0.00

                    || auth()->check() && $response->locked == 'yes'
                    && $checkUserSubscription
                    && $checkUserSubscription->free == 'yes'
                    && $response->price != 0.00
                    && $checkPayPerView

                    || auth()->check() && $response->locked == 'yes'
                    && $response->price != 0.00
                    && $checkPayPerView

                    || auth()->check() && auth()->user()->role == 'admin' && auth()->user()->permission == 'all'
                    || auth()->check() && $response->locked == 'no') {
                        $buttonLike = 'likeButton';
                        $buttonBookmark = 'btnBookmark';
                    } else {
                        $buttonLike = null;
                        $buttonBookmark = null;
                    }
                ?>

                <a href="javascript:void(0);" <?php if(auth()->guard()->guest()): ?> data-toggle="modal" data-target="#loginFormModal"
                   <?php endif; ?> class="btnLike <?php if($likeActive): ?>active <?php endif; ?> <?php echo e($buttonLike, false); ?> text-muted mr-2"
                   <?php if(auth()->guard()->check()): ?> data-id="<?php echo e($response->id, false); ?>" <?php endif; ?>>
                    <i class="<?php if($likeActive): ?>fas <?php else: ?> far <?php endif; ?> fa-heart"></i> <small><strong
                            class="countLikes"><?php echo e(Helper::formatNumber($response->likes()->count()), false); ?></strong></small>
                </a>

                <span class="text-muted mr-2 <?php if(auth()->guard()->check()): ?> <?php if( ! isset($inPostDetail)): ?> toggleComments <?php endif; ?> <?php endif; ?>">
				<i class="far fa-comment"></i> <small
                        class="font-weight-bold totalComments"><?php echo e(Helper::formatNumber($response->comments()->count()), false); ?></small>
			</span>

                <?php if(auth()->guard()->check()): ?>
                    <?php if(auth()->user()->id != $response->user()->id
                                && $checkUserSubscription && $response->price == 0.00

                                || auth()->user()->id != $response->user()->id
                                && $checkUserSubscription
                                && $checkUserSubscription->free == 'yes'
                                && $response->price != 0.00
                                && $checkPayPerView

                                || auth()->user()->id != $response->user()->id
                                && $response->price != 0.00
                                && $checkPayPerView

                                || auth()->user()->id != $response->user()->id
                                && $response->locked == 'no'): ?>
                        <a href="javascript:void(0);" data-toggle="modal" title="<?php echo e(trans('general.tip'), false); ?>"
                           data-target="#tipForm" class="text-muted text-decoration-none"
                           <?php if(auth()->guard()->check()): ?> data-id="<?php echo e($response->id, false); ?>"
                           data-cover="<?php echo e(Helper::getFile(config('path.cover').$response->user()->cover), false); ?>"
                           data-avatar="<?php echo e(Helper::getFile(config('path.avatar').$response->user()->avatar), false); ?>"
                           data-name="<?php echo e($response->user()->hide_name == 'yes' ? $response->user()->username : $response->user()->name, false); ?>"
                           data-userid="<?php echo e($response->user()->id, false); ?>" <?php endif; ?>>
                            <i class="fa fa-donate"></i> <h6 class="d-inline"><?php echo app('translator')->get('general.tip'); ?></h6>
                        </a>
                    <?php endif; ?>
                <?php endif; ?>

                <a href="javascript:void(0);" <?php if(auth()->guard()->guest()): ?> data-toggle="modal" data-target="#loginFormModal"
                   <?php endif; ?> class="<?php if($bookmarkActive): ?> text-primary <?php else: ?> text-muted <?php endif; ?> float-right <?php echo e($buttonBookmark, false); ?>"
                   <?php if(auth()->guard()->check()): ?> data-id="<?php echo e($response->id, false); ?>" <?php endif; ?>>
                    <i class="<?php if($bookmarkActive): ?>fas <?php else: ?> far <?php endif; ?> fa-bookmark"></i>
                </a>
            </h4>

            <?php if(auth()->guard()->check()): ?>

                <div class="container-comments <?php if( ! isset($inPostDetail)): ?> display-none <?php endif; ?>">

                    <div class="container-media">
                        <?php if($response->comments()->count() != 0): ?>

                            <?php
                                $comments = $response->comments()->take($settings->number_comments_show)->orderBy('id', 'DESC')->get();
                                $data = [];

                                if ($comments->count()) {
                                    $data['reverse'] = collect($comments->values())->reverse();
                                } else {
                                    $data['reverse'] = $comments;
                                }

                                $dataComments = $data['reverse'];
                                  $counter = ($response->comments()->count() - $settings->number_comments_show);
                            ?>

                            <?php if(auth()->user()->id == $response->user()->id

                                || $response->locked == 'yes'
                                && $checkUserSubscription
                                && $response->price == 0.00

                                || $response->locked == 'yes'
                                && $checkUserSubscription
                                && $checkUserSubscription->free == 'yes'
                                && $response->price != 0.00
                                && $checkPayPerView

                                || $response->locked == 'yes'
                                && $response->price != 0.00
                                && $checkPayPerView

                                || auth()->user()->role == 'admin'
                                && auth()->user()->permission == 'all'
                                || $response->locked == 'no'): ?>

                                <?php echo $__env->make('includes.comments', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                            <?php endif; ?>

                        <?php endif; ?>
                    </div><!-- container-media -->

                    <?php if(auth()->user()->id == $response->user()->id

                        || $response->locked == 'yes'
                        && $checkUserSubscription
                        && $response->price == 0.00

                        || $response->locked == 'yes'
                        && $checkUserSubscription
                        && $checkUserSubscription->free == 'yes'
                        && $response->price != 0.00
                        && $checkPayPerView

                        || $response->locked == 'yes'
                        && $response->price != 0.00
                        && $checkPayPerView

                        || auth()->user()->role == 'admin'
                        && auth()->user()->permission == 'all'
                        || $response->locked == 'no'): ?>

                        <hr/>

                        <div class="alert alert-danger alert-small dangerAlertComments display-none">
                            <ul class="list-unstyled m-0 showErrorsComments"></ul>
                        </div><!-- Alert -->

                        <div class="media position-relative">
                            <div class="blocked display-none"></div>
                            <span href="#" class="float-left">
				<img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>"
                     class="rounded-circle mr-1 avatarUser" width="40">
			</span>
                            <div class="media-body">
                                <form action="<?php echo e(url('comment/store'), false); ?>" method="post" class="comments-form">
                                    <?php echo csrf_field(); ?>
                                    <input type="hidden" name="update_id" value="<?php echo e($response->id, false); ?>"/>
                                    <input type="text" name="comment" class="form-control comments border-0"
                                           autocomplete="off" placeholder="<?php echo e(trans('general.write_comment'), false); ?>">
                            </div>
                            </form>
                        </div>
                    <?php endif; ?>

                </div><!-- container-comments -->

            <?php endif; ?>
        </div><!-- card-footer -->
    </div><!-- card -->
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<div class="card mb-3 pb-4 loadMoreSpin d-none">
    <div class="card-body">
        <div class="media">
		<span class="rounded-circle mr-3">
			<span class="item-loading position-relative loading-avatar"></span>
		</span>
            <div class="media-body">
                <h5 class="mb-0 item-loading position-relative loading-name"></h5>
                <small class="text-muted item-loading position-relative loading-time"></small>
            </div>
        </div>
    </div>
    <div class="card-body pt-0 pb-3">
        <p class="mb-1 item-loading position-relative loading-text-1"></p>
        <p class="mb-1 item-loading position-relative loading-text-2"></p>
        <p class="mb-0 item-loading position-relative loading-text-3"></p>
    </div>
</div>

<?php
    if (isset($ajaxRequest)) {
        $totalPosts = $total;
    } else {
        $totalPosts = $updates->total();
    }
?>

<?php if($totalPosts > $settings->number_posts_show && $counterPosts >= 1): ?>
    <button rel="next" class="btn btn-primary w-100 text-center loadPaginator d-none" id="paginator">
        <?php echo e(trans('general.loadmore'), false); ?>

    </button>
<?php endif; ?>

<?php /**PATH /home/allajwno/public_html/backup/resources/views/includes/updates.blade.php ENDPATH**/ ?>