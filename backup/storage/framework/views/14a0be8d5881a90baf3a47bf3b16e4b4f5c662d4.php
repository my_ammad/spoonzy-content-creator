<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale()), false); ?>">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token(), false); ?>">
    <meta name="description"
          content="<?php echo $__env->yieldContent('description_custom'); ?><?php if(!Request::route()->named('seo') && !Request::route()->named('profile')): ?><?php echo e(trans('seo.description'), false); ?><?php endif; ?>">
    <meta name="keywords" content="<?php echo $__env->yieldContent('keywords_custom'); ?><?php echo e(trans('seo.keywords'), false); ?>"/>
    <meta name="theme-color"
          content="<?php echo e(auth()->check() && auth()->user()->dark_mode == 'on' ? '#303030' : $settings->color_default, false); ?>">
    <title><?php echo e(Auth::check() && User::notificationsCount() ? '('.User::notificationsCount().') ' : '', false); ?><?php $__env->startSection('title'); ?><?php echo $__env->yieldSection(); ?> <?php if( isset( $settings->title ) ): ?><?php echo e($settings->title, false); ?><?php endif; ?></title>
    <!-- Favicon -->
    <link href="<?php echo e(url('public/img', $settings->favicon), false); ?>" rel="icon">

    <?php echo $__env->make('includes.css_general', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <script src="<?php echo e(asset('public/js/jquery.min.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/naim/js/axios.min.js'), false); ?>"></script>
    <?php if(auth()->guard()->check()): ?>
        <?php if(request()->route()->named('profile') || request()->route()->named('home')): ?>
            <script>window.PostAuthor = " <?php echo e(@$user->id, false); ?>";</script>
            <script src="<?php echo e(asset('public/naim/js/post_inspection.js'), false); ?>?var=2"></script>
            <script src="<?php echo e(asset('public/naim/js/screenshot_detection.js'), false); ?>?var=2"></script>
        <?php endif; ?>
    <?php endif; ?>

    <?php $config = (new \LaravelPWA\Services\ManifestService)->generate(); echo $__env->make( 'laravelpwa::meta' , ['config' => $config])->render(); ?>

    <?php echo $__env->yieldContent('css'); ?>

    <?php if($settings->google_analytics != ''): ?>
        <?php echo $settings->google_analytics; ?>

    <?php endif; ?>
    
    <style>
        
        .btn-outline-light,.btn-primary,.btn-outline-primary {
  
    background: #fc1d4a;
    border-color:#fd9562;
}.btn-outline-light:hover,.btn-primary:hover,.btn-outline-primary:hover {
  
    background: #fd9562!important;
    border-color:#fd9562;
}
.btn-outline-primary {
  
   color:#fff;
}
.range::-webkit-slider-thumb {
  -webkit-appearance: none;
    background-color: red;

}
    </style>
</head>

<body>


<div id="mobileMenuOverlay" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
     aria-expanded="false"></div>

<?php if(auth()->guard()->check()): ?>
    <?php if( ! request()->is('messages/*')): ?>
        <?php echo $__env->make('includes.menu-mobile', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php endif; ?>
<?php endif; ?>

<?php if($settings->alert_adult == 'on'): ?>
    <div class="modal fade" tabindex="-1" id="alertAdult">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-4">
                    <p><?php echo e(__('general.alert_content_adult'), false); ?></p>
                </div>
                <div class="modal-footer border-0 pt-0">
                    <a href="https://google.com" class="btn e-none p-0 mr-3"><?php echo e(trans('general.leave'), false); ?></a>
                    <button type="button" class="btn btn-primary"
                            id="btnAlertAdult"><?php echo e(trans('general.i_am_age'), false); ?></button>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<div class="popout popout-error font-default"></div>

<?php if(Auth::guest() && request()->path() == '/' && $settings->home_style == 0
    || Auth::guest() && request()->path() != '/' && $settings->home_style == 0
    || Auth::guest() && request()->path() != '/' && $settings->home_style == 1
    || Auth::check()
    ): ?>
    <?php echo $__env->make('includes.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php endif; ?>

<main <?php if(request()->is('messages/*')): ?> class="h-100" <?php endif; ?> role="main">
    <?php echo $__env->yieldContent('content'); ?>

    <?php if(Auth::guest() && ! request()->route()->named('profile')
          || Auth::check()
          && request()->path() != '/'
          && ! request()->is('my/bookmarks')
          && ! request()->is('my/purchases')
          && ! request()->route()->named('profile')
          && ! request()->is('messages/*')
          ): ?>

        <?php if(Auth::guest() && request()->path() == '/' && $settings->home_style == 0 && ! request()->is('offline')
              || Auth::guest() && request()->path() != '/' && $settings->home_style == 0 && ! request()->is('offline')
              || Auth::guest() && request()->path() != '/' && $settings->home_style == 1 && ! request()->is('offline')
              || Auth::check()
                ): ?>

            <?php if(Auth::guest() && $settings->who_can_see_content == 'users'): ?>
                <div class="text-center py-3 px-3">
                    <?php echo $__env->make('includes.footer-tiny', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            <?php else: ?>
                <?php echo $__env->make('includes.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endif; ?>

        <?php endif; ?>

    <?php endif; ?>

    <?php if(auth()->guard()->guest()): ?>

        <?php if(! request()->is('/')
            && ! request()->is('login')
            && ! request()->is('signup')
            && ! request()->is('password/reset')
            && ! request()->is('password/reset/*')
            && ! request()->is('contact')
            ): ?>
            <div class="modal fade" id="loginFormModal" tabindex="-1" role="dialog" aria-labelledby="modal-form"
                 aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-sm modal-login" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-0">
                            <div class="card-body px-lg-5 py-lg-5 position-relative">

                                <h6 class="modal-title text-center mb-3"><?php echo e(__('general.login_continue'), false); ?></h6>

                                <?php if($settings->facebook_login == 'on' || $settings->google_login == 'on' || $settings->twitter_login == 'on'): ?>
                                    <div class="mb-2 w-100">

                                        <?php if($settings->facebook_login == 'on'): ?>
                                            <a href="<?php echo e(url('oauth/facebook'), false); ?>"
                                               class="btn btn-facebook auth-form-btn flex-grow mb-2 w-100">
                                                <i class="fab fa-facebook mr-2"></i> <?php echo e(__('auth.login_with'), false); ?>

                                                Facebook
                                            </a>
                                        <?php endif; ?>

                                        <?php if($settings->twitter_login == 'on'): ?>
                                            <a href="<?php echo e(url('oauth/twitter'), false); ?>"
                                               class="btn btn-twitter auth-form-btn mb-2 w-100">
                                                <i class="fab fa-twitter mr-2"></i> <?php echo e(__('auth.login_with'), false); ?> Twitter
                                            </a>
                                        <?php endif; ?>

                                        <?php if($settings->google_login == 'on'): ?>
                                            <a href="<?php echo e(url('oauth/google'), false); ?>"
                                               class="btn btn-google auth-form-btn flex-grow w-100">
                                                <img src="<?php echo e(url('public/img/google.svg'), false); ?>" class="mr-2" width="18"
                                                     height="18"> <?php echo e(__('auth.login_with'), false); ?> Google
                                            </a>
                                        <?php endif; ?>
                                    </div>

                                    <small
                                        class="btn-block text-center my-3 text-uppercase or"><?php echo e(__('general.or'), false); ?></small>

                                <?php endif; ?>

                                <form method="POST" action="<?php echo e(route('login'), false); ?>" data-url-login="<?php echo e(route('login'), false); ?>"
                                      data-url-register="<?php echo e(route('register'), false); ?>" id="formLoginRegister"
                                      enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>

                                    <?php if(request()->route()->named('profile')): ?>
                                        <input type="hidden" name="isProfile" value="<?php echo e($user->username, false); ?>">
                                    <?php endif; ?>

                                    <input type="hidden" name="isModal" id="isModal" value="true">

                                    <?php if($settings->captcha == 'on'): ?>
                                        <?php echo app('captcha')->render(); ?>
                                    <?php endif; ?>

                                    <div class="form-group mb-3 display-none" id="full_name">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-user-circle"></i></span>
                                            </div>
                                            <input class="form-control" value="<?php echo e(old('name'), false); ?>"
                                                   placeholder="<?php echo e(trans('auth.full_name'), false); ?>" name="name" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group mb-3 display-none" id="email">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input class="form-control" value="<?php echo e(old('email'), false); ?>"
                                                   placeholder="<?php echo e(trans('auth.email'), false); ?>" name="email" type="text">
                                        </div>
                                    </div>

                                    <div class="form-group mb-3" id="username_email">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                            </div>
                                            <input class="form-control" value="<?php echo e(old('username_email'), false); ?>"
                                                   placeholder="<?php echo e(trans('auth.username_or_email'), false); ?>"
                                                   name="username_email" type="text">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group input-group-alternative" id="showHidePassword">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fa fa-key"></i></span>
                                            </div>
                                            <input name="password" type="password" class="form-control"
                                                   placeholder="<?php echo e(trans('auth.password'), false); ?>">
                                            <div class="input-group-append">
                                                <span class="input-group-text c-pointer"><i class="fa fa-eye-slash"></i></span>
                                            </div>
                                        </div>
                                        <small class="form-text text-muted">
                                            <a href="<?php echo e(url('password/reset'), false); ?>" id="forgotPassword">
                                                <?php echo e(trans('auth.forgot_password'), false); ?>

                                            </a>
                                        </small>
                                    </div>

                                    <div class="custom-control custom-control-alternative custom-checkbox"
                                         id="remember">
                                        <input class="custom-control-input" id=" customCheckLogin" type="checkbox"
                                               name="remember" <?php echo e(old('remember') ? 'checked' : '', false); ?>>
                                        <label class="custom-control-label" for=" customCheckLogin">
                                            <span><?php echo e(trans('auth.remember_me'), false); ?></span>
                                        </label>
                                    </div>

                                    <div class="custom-control custom-control-alternative custom-checkbox display-none"
                                         id="agree_gdpr">
                                        <input class="custom-control-input" id="customCheckRegister" type="checkbox"
                                               name="agree_gdpr">
                                        <label class="custom-control-label" for="customCheckRegister">
                        <span><?php echo e(trans('admin.i_agree_gdpr'), false); ?>

                          <a href="<?php echo e($settings->link_privacy, false); ?>" target="_blank"><?php echo e(trans('admin.privacy_policy'), false); ?></a>
                        </span>
                                        </label>
                                    </div>

                                    <div class="alert alert-danger display-none mb-0 mt-3" id="errorLogin">
                                        <ul class="list-unstyled m-0" id="showErrorsLogin"></ul>
                                    </div>

                                    <div class="alert alert-success display-none mb-0 mt-3" id="checkAccount"></div>

                                    <div class="text-center">
                                        <button type="submit" id="btnLoginRegister" class="btn btn-primary mt-4 w-100">
                                            <i></i> <?php echo e(trans('auth.login'), false); ?></button>

                                        <div class="w-100 mt-2">
                                            <button type="button" class="btn e-none p-0"
                                                    data-dismiss="modal"><?php echo e(__('admin.cancel'), false); ?></button>
                                        </div>
                                    </div>
                                </form>

                                <?php if($settings->captcha == 'on'): ?>
                                    <small class="btn-block text-center mt-3"><?php echo e(trans('auth.protected_recaptcha'), false); ?> <a
                                            href="https://policies.google.com/privacy"
                                            target="_blank"><?php echo e(trans('general.privacy'), false); ?></a> - <a
                                            href="https://policies.google.com/terms"
                                            target="_blank"><?php echo e(trans('general.terms'), false); ?></a></small>
                                <?php endif; ?>

                                <?php if($settings->registration_active == '1'): ?>
                                    <div class="row mt-3">
                                        <div class="col-12 text-center">
                                            <a href="javascript:void(0);" id="toggleLogin"
                                               data-not-account="<?php echo e(trans('auth.not_have_account'), false); ?>"
                                               data-already-account="<?php echo e(trans('auth.already_have_an_account'), false); ?>"
                                               data-text-login="<?php echo e(trans('auth.login'), false); ?>"
                                               data-text-register="<?php echo e(trans('auth.sign_up'), false); ?>">
                                                <strong><?php echo e(trans('auth.not_have_account'), false); ?></strong>
                                            </a>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div><!-- ./ card-body -->
                        </div>
                    </div>
                </div>
            </div>
            </div><!-- End Modal -->
        <?php endif; ?>
    <?php endif; ?>

    <?php if(auth()->guard()->check()): ?>
        <div class="modal fade" id="tipForm" tabindex="-1" role="dialog" aria-labelledby="modal-form"
             aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="card bg-white shadow border-0">
                            <div class="card-header pb-2 border-0 position-relative"
                                 style="height: 100px; background: <?php echo e($settings->color_default, false); ?> <?php if(auth()->user()->cover != ''): ?>  url('<?php echo e(Helper::getFile(config('path.cover').auth()->user()->cover), false); ?>') <?php endif; ?> no-repeat center center; background-size: cover;">

                            </div>
                            <div class="card-body px-lg-5 py-lg-5 position-relative">

                                <div class="text-muted text-center mb-3 position-relative modal-offset">
                                    <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>"
                                         width="100" class="avatar-modal rounded-circle mb-1">
                                    <h6>
                                        <?php echo e(trans('general.send_tip'), false); ?> <span class="userNameTip"></span>
                                    </h6>
                                </div>

                                <form method="post" action="<?php echo e(url('send/tip'), false); ?>" id="formSendTip">

                                    <input type="hidden" name="id" class="userIdInput" value="<?php echo e(auth()->user()->id, false); ?>"/>

                                    <?php if(request()->is('messages/*')): ?>
                                        <input type="hidden" name="isMessage" value="1"/>
                                    <?php endif; ?>

                                    <input type="hidden" id="cardholder-name" value="<?php echo e(auth()->user()->name, false); ?>"/>
                                    <input type="hidden" id="cardholder-email" value="<?php echo e(auth()->user()->email, false); ?>"/>
                                    <input type="number" min="<?php echo e($settings->min_donation_amount, false); ?>" autocomplete="off"
                                           id="onlyNumber" class="form-control mb-3" name="amount"
                                           placeholder="<?php echo e(trans('general.tip_amount'), false); ?>">

                                    <?php echo csrf_field(); ?>

                                    <?php $__currentLoopData = PaymentGateways::where('enabled', '1')->whereSubscription('yes')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php

                                            if ($payment->type == 'card' ) {
                                              $paymentName = '<i class="far fa-credit-card mr-1"></i> '.trans('general.debit_credit_card') .' <small class="w-100 d-block">'.__('general.powered_by').' '.$payment->name.'</small>';
                                            } else if ($payment->id == 1) {
                                              $paymentName = '<img src="'.url('public/img/payments', auth()->user()->dark_mode == 'off' ? $payment->logo : 'paypal-white.png').'" width="70"/> <small class="w-100 d-block">'.trans('general.redirected_to_paypal_website').'</small>';
                                            } else {
                                              $paymentName = '<img src="'.url('public/img/payments', $payment->logo).'" width="70"/>';
                                            }

                                            $allPayments = PaymentGateways::where('enabled', '1')->whereSubscription('yes')->get();

                                        ?>
                                        <?php if($payment->name == 'Stripe'): ?>
                                        <small class="text-danger">$5 is minimum amount for tip.</small>
                                        <?php endif; ?>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway_tip" value="<?php echo e($payment->id, false); ?>"
                                                   id="tip_radio<?php echo e($payment->id, false); ?>"
                                                   <?php if($allPayments->count() == 1 && auth()->user()->wallet == 0.00): ?> checked
                                                   <?php endif; ?> class="custom-control-input" type="radio">
                                            <label class="custom-control-label" for="tip_radio<?php echo e($payment->id, false); ?>">
                                                <span><strong><?php echo $paymentName; ?></strong></span>
                                            </label>
                                        </div>

                                        <?php if($payment->name == 'Stripe'): ?>
                                            <div id="stripeContainerTip"
                                                 class="<?php if($allPayments->count() != 1): ?> display-none <?php endif; ?>">
                                                <div id="card-element" class="margin-bottom-10">
                                                    <!-- A Stripe Element will be inserted here. -->
                                                </div>
                                                <!-- Used to display form errors. -->
                                                <div id="card-errors" class="alert alert-danger display-none"
                                                     role="alert"></div>
                                            </div>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php if($settings->disable_wallet == 'on' && auth()->user()->wallet != 0.00 || $settings->disable_wallet == 'off'): ?>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway_tip" <?php if(Auth::user()->wallet == 0): ?> disabled
                                                   <?php endif; ?> value="wallet" id="tip_radio0" class="custom-control-input"
                                                   type="radio">
                                            <label class="custom-control-label" for="tip_radio0">
                      <span>
                        <strong>
                        <i class="fas fa-wallet mr-1"></i> <?php echo e(__('general.wallet'), false); ?>

                        <span class="w-100 d-block font-weight-light">
                          <?php echo e(__('general.available_balance'), false); ?>: <span
                                class="font-weight-bold mr-1 balanceWallet"><?php echo e(Helper::amountFormatDecimal(Auth::user()->wallet), false); ?></span>

                          <?php if(Auth::user()->wallet == 0): ?>
                                <a href="<?php echo e(url('my/wallet'), false); ?>" class="link-border"><?php echo e(__('general.recharge'), false); ?></a>
                            <?php endif; ?>
                        </span>
                      </strong>
                      </span>
                                            </label>
                                        </div>
                                    <?php endif; ?>

                                    <div class="alert alert-danger display-none" id="errorTip">
                                        <ul class="list-unstyled m-0" id="showErrorsTip"></ul>
                                    </div>

                                    <div class="text-center">
                                        <button type="button" class="btn e-none mt-4"
                                                data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                                        <button type="submit" id="tipBtn" class="btn btn-primary mt-4 tipBtn">
                                            <i></i> <?php echo e(trans('general.pay'), false); ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Tip -->

        <!-- Start Modal payPerViewForm -->
        <div class="modal fade" id="payPerViewForm" tabindex="-1" role="dialog" aria-labelledby="modal-form"
             aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="card bg-white shadow border-0">

                            <div class="card-body px-lg-5 py-lg-5 position-relative">

                                <div class="mb-3">
                                    <i class="feather icon-unlock mr-1"></i> <?php echo e(trans('general.unlock_content'), false); ?>

                                </div>

                                <form method="post" action="<?php echo e(url('send/ppv'), false); ?>" id="formSendPPV">

                                    <input type="hidden" name="id" class="mediaIdInput" value="0"/>
                                    <input type="hidden" name="amount" class="priceInput" value="0"/>

                                    <?php if(request()->is('messages/*')): ?>
                                        <input type="hidden" name="isMessage" value="1"/>
                                    <?php endif; ?>

                                    <input type="hidden" id="cardholder-name-PPV" value="<?php echo e(auth()->user()->name, false); ?>"/>
                                    <input type="hidden" id="cardholder-email-PPV" value="<?php echo e(auth()->user()->email, false); ?>"/>
                                    <?php echo csrf_field(); ?>

                                    <?php $__currentLoopData = PaymentGateways::where('enabled', '1')->whereSubscription('yes')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php

                                            if ($payment->type == 'card' ) {
                                              $paymentName = '<i class="far fa-credit-card mr-1"></i> '.trans('general.debit_credit_card') .' <small class="w-100 d-block">'.__('general.powered_by').' '.$payment->name.'</small>';
                                            } else if ($payment->id == 1) {
                                              $paymentName = '<img src="'.url('public/img/payments', auth()->user()->dark_mode == 'off' ? $payment->logo : 'paypal-white.png').'" width="70"/> <small class="w-100 d-block">'.trans('general.redirected_to_paypal_website').'</small>';
                                            } else {
                                              $paymentName = '<img src="'.url('public/img/payments', $payment->logo).'" width="70"/>';
                                            }

                                            $allPayments = PaymentGateways::where('enabled', '1')->whereSubscription('yes')->get();

                                        ?>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway_ppv" value="<?php echo e($payment->id, false); ?>"
                                                   id="ppv_radio<?php echo e($payment->id, false); ?>"
                                                   <?php if($allPayments->count() == 1 && auth()->user()->wallet == 0.00): ?> checked
                                                   <?php endif; ?> class="custom-control-input" type="radio">
                                            <label class="custom-control-label" for="ppv_radio<?php echo e($payment->id, false); ?>">
                                                <span><strong><?php echo $paymentName; ?></strong></span>
                                            </label>
                                        </div>

                                        <?php if($payment->name == 'Stripe'): ?>
                                            <div id="stripeContainerPPV"
                                                 class="<?php if($allPayments->count() != 1): ?> display-none <?php endif; ?>">
                                                <div id="card-elementPPV" class="margin-bottom-10">
                                                    <!-- A Stripe Element will be inserted here. -->
                                                </div>
                                                <!-- Used to display form errors. -->
                                                <div id="card-errorsPPV" class="alert alert-danger display-none"
                                                     role="alert"></div>
                                            </div>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php if($settings->disable_wallet == 'on' && auth()->user()->wallet != 0.00 || $settings->disable_wallet == 'off'): ?>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway_ppv" <?php if(Auth::user()->wallet == 0): ?> disabled
                                                   <?php endif; ?> value="wallet" id="ppv_radio0" class="custom-control-input"
                                                   type="radio">
                                            <label class="custom-control-label" for="ppv_radio0">
                      <span>
                        <strong>
                        <i class="fas fa-wallet mr-1"></i> <?php echo e(__('general.wallet'), false); ?>

                        <span class="w-100 d-block font-weight-light">
                          <?php echo e(__('general.available_balance'), false); ?>: <span
                                class="font-weight-bold mr-1 balanceWallet"><?php echo e(Helper::amountFormatDecimal(Auth::user()->wallet), false); ?></span>

                          <?php if(Auth::user()->wallet == 0): ?>
                                <a href="<?php echo e(url('my/wallet'), false); ?>" class="link-border"><?php echo e(__('general.recharge'), false); ?></a>
                            <?php endif; ?>
                        </span>
                      </strong>
                      </span>
                                            </label>
                                        </div>
                                    <?php endif; ?>

                                    <div class="alert alert-danger display-none" id="errorPPV">
                                        <ul class="list-unstyled m-0" id="showErrorsPPV"></ul>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" id="ppvBtn" class="btn btn-primary mt-4 ppvBtn">
                                            <i></i> <?php echo e(trans('general.pay'), false); ?> <span class="pricePPV"></span>
                                            <small><?php echo e($settings->currency_code, false); ?></small></button>

                                        <div class="w-100 mt-2">
                                            <button type="button" class="btn e-none p-0"
                                                    data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal payPerViewForm -->
    <?php endif; ?>
</main>

<?php echo $__env->make('includes.javascript_general', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->yieldContent('javascript'); ?>
<?php if(auth()->guard()->check()): ?>
    <div id="bodyContainer"></div>
<?php endif; ?>
</body>
</html><?php /**PATH /home/allajwno/public_html/backup/resources/views/layouts/app.blade.php ENDPATH**/ ?>