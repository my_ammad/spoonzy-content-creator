<?php $__env->startSection('title'); ?> <?php echo e(trans('general.payment_card'), false); ?> -<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 pt-5 pb-4">
          <h2 class="mb-0 font-montserrat"><i class="feather icon-credit-card mr-2"></i> <?php echo e(trans('Add Bank info'), false); ?></h2>
          
        </div>
      </div>
      <div class="row">

        <div class="col-md-8 mx-auto mb-lg-0">

          <div class="bg-white rounded-lg shadow-sm p-5">

            <div class="alert alert-success display-none" id="success">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>

                <?php echo e(trans('general.payment_card_success'), false); ?>

            </div>
            <div>
                <form action="<?php echo e(url('settings/payments/wise'), false); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label>ACH Routing number</label>
                        <input class="form-control" required name="abartn">
                    </div>
                    <div class="form-group">
                        <label>Bank account number</label>
                        <input class="form-control" name="ac_no" required>
                    </div>
                    <div class="form-group">
                        <label>Acount Type</label>
                        <select class="form-control" name="ac_type">
                            <option value="">Please Select</option>
                            <option>CHECKING</option>
                            <option>SAVINGS</option>
                        </select>
                    </div>
                    <h5>Address</h5>
                    <div class="row">    
                    <div class="form-group col-md-6">
                        <label>Country</label>
                        <select class="form-control" name="country">
                            <option value="">Please Select</option>
                            <option value="US">United State</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>City</label>
                        <input class="form-control" name="city" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Post code</label>
                        <input class="form-control" name="post_code" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <input class="form-control" name="address" required>
                    </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-sm">Submit</button>
                    </div>
                </form>
            </div>

        </div><!-- end col-md-8 -->

      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(asset('public/js/add-payment-card.js'), false); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/wiseForm.blade.php ENDPATH**/ ?>