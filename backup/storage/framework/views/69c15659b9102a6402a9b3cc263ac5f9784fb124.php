<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/morris/morris.css'), false); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('public/plugins/jvectormap/jquery-jvectormap-1.2.2.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo e(trans('admin.dashboard'), false); ?> v<?php echo e($settings->version, false); ?>

          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo e(url('panel/admin'), false); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('admin.home'), false); ?></a></li>
            <li class="active"><?php echo e(trans('admin.dashboard'), false); ?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

        	<div class="row">

        <div class="col-lg-3">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo e($total_subscriptions, false); ?></h3>
                  <p><?php echo e(trans('admin.subscriptions'), false); ?></p>
                </div>
                <div class="icon">
                  <i class="iconmoon icon-Dollar"></i>
                </div>
								<a href="<?php echo e(url('panel/admin/subscriptions'), false); ?>" class="small-box-footer"><?php echo e(trans('general.view_more'), false); ?> <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

        <div class="col-lg-3">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo e(Helper::amountFormatDecimal($total_raised_funds), false); ?></h3>
                  <p><?php echo e(trans('admin.earnings_net'), false); ?> (<?php echo e(__('users.admin'), false); ?>)</p>
                </div>
                <div class="icon">
                  <i class="iconmoon icon-Bag"></i>
                </div>
								<span class="small-box-footer"><?php echo e(trans('admin.earnings_net'), false); ?></span>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-3">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo e(number_format(User::count()), false); ?></h3>
                  <p><?php echo e(trans('general.members'), false); ?></p>
                </div>
                <div class="icon">
                  <i class="iconmoon icon-Users"></i>
                </div>
								<a href="<?php echo e(url('panel/admin/members'), false); ?>" class="small-box-footer"><?php echo e(trans('general.view_more'), false); ?> <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

						<div class="col-lg-3">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo e(Helper::formatNumber($total_posts), false); ?></h3>
                  <p><?php echo e(trans('general.posts'), false); ?></p>
                </div>
                <div class="icon">
                  <i class="fa fa-user-edit"></i>
                </div>
								<a href="<?php echo e(url('panel/admin/posts'), false); ?>" class="small-box-footer"><?php echo e(trans('general.view_more'), false); ?> <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

          </div>

    <div class="row">

      <section class="col-md-12">
        <div class="box">
          <div class="box-header with-border text-center">
          <h3 class="box-title"><i class="fa fa-bar-chart-o"></i> <?php echo e(trans('general.statistics_of_the_month'), false); ?></h3>
        </div>
        <div class="chart">
          <!-- Sales Chart Canvas -->
          <canvas id="salesChart" style="height: 280px;"></canvas>
        </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-4 col-xs-12">
                <div class="description-block border-right">
                  <h2 class="<?php echo e($stat_revenue_today > 0 ? 'text-green' : 'text-red', false); ?>"><?php echo e(Helper::amountFormatDecimal($stat_revenue_today), false); ?></h2>
                  <span class="description-text text-black"><?php echo e(trans('general.revenue_today'), false); ?></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-4 col-xs-12">
                <div class="description-block border-right">
                  <h2 class="<?php echo e($stat_revenue_week > 0 ? 'text-green' : 'text-red', false); ?>"><?php echo e(Helper::amountFormatDecimal($stat_revenue_week), false); ?></h2>
                  <span class="description-text text-black"><?php echo e(trans('general.revenue_week'), false); ?></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-4 col-xs-12">
                <div class="description-block">
                  <h2 class="<?php echo e($stat_revenue_month > 0 ? 'text-green' : 'text-red', false); ?>"><?php echo e(Helper::amountFormatDecimal($stat_revenue_month), false); ?></h2>
                  <span class="description-text text-black"><?php echo e(trans('general.revenue_month'), false); ?></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div><!-- /.box-footer -->
        </div><!-- /.box -->
      </section>

			<section class="col-md-7">
			  <div class="nav-tabs-custom">
			    <ul class="nav nav-tabs pull-right ui-sortable-handle">
			        <li class="pull-left header"><i class="ion ion-cash"></i> <?php echo e(trans('admin.subscriptions_last_30_days'), false); ?></li>
			    </ul>
			    <div class="tab-content">
			        <div class="tab-pane active">
			          <div class="chart" id="chart1"></div>
			        </div>
			    </div>
			</div>
		  </section>

			<section class="col-md-5">
		  	<!-- Map box -->
              <div class="box box-solid bg-purple-gradient">
                <div class="box-header">

                  <i class="fa fa-map-marker-alt"></i>
                  <h3 class="box-title">
                    <?php echo e(trans('admin.user_countries'), false); ?>

                  </h3>
                </div>
                <div class="box-body">
                  <div id="world-map" class="world-map"></div>
                </div><!-- /.box-body-->
              </div>
              <!-- /.box -->
		  </section>

        </div><!-- ./row -->

        <div class="row">

					<div class="col-md-6">
						<div class="box">
							 <div class="box-header with-border">
								 <h3 class="box-title"><?php echo e(trans('admin.recent_subscriptions'), false); ?></h3>
								 <div class="box-tools pull-right">
								 </div>
							 </div><!-- /.box-header -->

							 <?php if($subscriptions->count() != 0): ?>
							 <div class="box-body">

								 <ul class="products-list product-list-in-box">

								<?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

									 <li class="item">
										 <div class="product-img">
											 <img src="<?php echo e(Helper::getFile(config('path.avatar').$subscription->user()->avatar), false); ?>" class="img-circle h-auto" onerror="onerror" />
										 </div>
										 <div class="product-info">
											 <span class="product-title">
                         <?php if( ! isset($subscription->user()->username)): ?>
                           <em class="text-muted"><?php echo e(trans('general.no_available'), false); ?></em>
                       <?php else: ?>
                         <a href="<?php echo e(url($subscription->user()->username), false); ?>" target="_blank"><?php echo e($subscription->user()->name, false); ?></a>
                       <?php endif; ?>

                          <?php echo e(trans('general.subscribed_to'), false); ?>


                          <?php if( ! isset($subscription->subscribed()->username)): ?>
                            <em class="text-muted"><?php echo e(trans('general.no_available'), false); ?></em>
                        <?php else: ?>
                          <a href="<?php echo e(url($subscription->subscribed()->username), false); ?>" target="_blank"><?php echo e($subscription->subscribed()->name, false); ?></a>
                        <?php endif; ?>
												 </span>
											 <span class="product-description">
												 <?php echo e(Helper::formatDate($subscription->created_at), false); ?>

											 </span>
										 </div>
									 </li><!-- /.item -->
									 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								 </ul>
							 </div><!-- /.box-body -->

							 <div class="box-footer text-center">
								 <a href="<?php echo e(url('panel/admin/subscriptions'), false); ?>" class="uppercase"><?php echo e(trans('general.view_all'), false); ?></a>
							 </div><!-- /.box-footer -->

							 <?php else: ?>
								<div class="box-body">
								 <h5><?php echo e(trans('admin.no_result'), false); ?></h5>
									</div><!-- /.box-body -->
							 <?php endif; ?>
						 </div>
					 </div>

              <div class="col-md-6">
                <!-- USERS LIST -->
                <div class="box">
                  <div class="box-header with-border">
                    <h3 class="box-title"><?php echo e(trans('admin.latest_members'), false); ?></h3>
                    <div class="box-tools pull-right">
                    </div>
                  </div><!-- /.box-header -->

                  <div class="box-body">
                    <ul class="products-list product-list-in-box">
                      <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php
												switch (  $user->status ) {
												  case 'active':
													  $user_color_status = 'success';
													  $user_txt_status = trans('general.active');
													  break;

												case 'pending':
													  $user_color_status = 'info';
													  $user_txt_status = trans('general.pending');
													  break;

												case 'suspended':
													  $user_color_status = 'warning';
													  $user_txt_status = trans('admin.suspended');
													  break;

											  }
												 ?>

											<li class="item">
	                      <div class="product-img">
	                        <img src="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>" class="img-circle h-auto" onerror="onerror" />
	                      </div>
	                      <div class="product-info">
	                        <a href="<?php echo e(url($user->username), false); ?>" target="_blank" class="product-title"><?php if($user->name !='' ): ?> <?php echo e($user->name, false); ?> <?php else: ?> <?php echo e($user->username, false); ?> <?php endif; ?>
	                        	<span class="label label-<?php echo e($user_color_status, false); ?> pull-right"><?php echo e($user_txt_status, false); ?></span>
	                        	</a>
	                        <span class="product-description">
	                          <?php echo e('@'.$user->username, false); ?> / <?php echo e(App\Helper::formatDate($user->date), false); ?>

	                        </span>
	                      </div>
	                    </li><!-- /.item -->
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul><!-- /.users-list -->
                  </div><!-- /.box-body -->

                  <div class="box-footer text-center">
                    <a href="<?php echo e(url('panel/admin/members'), false); ?>" class="uppercase"><?php echo e(trans('admin.view_all_members'), false); ?></a>
                  </div><!-- /.box-footer -->
                </div><!--/.box -->
              </div>
              </div><!-- ./row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
	<!-- Morris -->
	<script src="<?php echo e(asset('public/plugins/morris/raphael-min.js'), false); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('public/plugins/morris/morris.min.js'), false); ?>" type="text/javascript"></script>

	<!-- knob -->
	<script src="<?php echo e(asset('public/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'), false); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('public/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'), false); ?>" type="text/javascript"></script>
	<script src="<?php echo e(asset('public/plugins/knob/jquery.knob.js'), false); ?>" type="text/javascript"></script>
  <script src="<?php echo e(asset('public/js/Chart.min.js'), false); ?>"></script>
  <?php echo $__env->make('admin.charts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/dashboard.blade.php ENDPATH**/ ?>