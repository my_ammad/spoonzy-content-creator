<?php $__env->startSection('title'); ?> <?php echo e(trans('general.subscription'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="feather icon-refresh-cw mr-2"></i> <?php echo e(trans('general.subscription'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('general.info_subscription'), false); ?></p>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          <?php if(session('status')): ?>
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    <?php echo e(session('status'), false); ?>

                  </div>
                <?php endif; ?>

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php if(auth()->user()->verified_id == 'no' && $settings->requests_verify_account == 'on'): ?>
    <div class="alert alert-danger mb-3">
             <ul class="list-unstyled m-0">
               <li><i class="fa fa-exclamation-triangle"></i> <?php echo e(trans('general.verified_account_info'), false); ?> <a href="<?php echo e(url('settings/verify/account'), false); ?>" class="text-white link-border"><?php echo e(trans('general.verify_account'), false); ?></a></li>
             </ul>
           </div>
           <?php endif; ?>

          <form method="POST" action="<?php echo e(url('settings/subscription'), false); ?>">

            <?php echo csrf_field(); ?>

            <div class="form-group">
              <label><?php echo e(trans('users.subscription_price'), false); ?> <?php if(auth()->user()->free_subscription == 'no' && Auth::user()->verified_id == 'yes'): ?> <a href="javascript:void(0)" data-container="body" data-toggle="popover" data-placement="top" data-trigger="focus" class="link-border" data-content='<?php echo e(trans('general.user_gain', ['percentage' => (100 - $settings->fee_commission)]), false); ?>'><?php echo e(__('general.how_much_earn'), false); ?></a> <?php endif; ?></label>
              <div class="input-group mb-2">
              <div class="input-group-prepend">
                <span class="input-group-text"><?php echo e($settings->currency_symbol, false); ?></span>
              </div>
                  <input class="form-control form-control-lg isNumber" id="subscriptionPrice" <?php if(Auth::user()->verified_id == 'no' || Auth::user()->verified_id == 'reject' || auth()->user()->free_subscription == 'yes'): ?> disabled <?php endif; ?> name="price" placeholder="<?php echo e(trans('users.subscription_price'), false); ?>" value="<?php echo e($settings->currency_code == 'JPY' ? round(Auth::user()->price) : Auth::user()->price, false); ?>"  type="text">
              </div>
              <div class="text-muted btn-block mb-4">
                <div class="custom-control custom-switch">
                  <input type="checkbox" class="custom-control-input" <?php if(Auth::user()->verified_id == 'no' || Auth::user()->verified_id == 'reject'): ?> disabled <?php endif; ?> name="free_subscription" value="yes" <?php if(auth()->user()->free_subscription == 'yes'): ?> checked <?php endif; ?> id="customSwitch1">
                  <label class="custom-control-label switch" for="customSwitch1"><?php echo e(trans('general.free_subscription'), false); ?></label>
                </div>
              </div>
            </div><!-- End form-group -->

            <button class="btn btn-1 btn-success btn-block" <?php if(Auth::user()->verified_id == 'no' || Auth::user()->verified_id == 'reject'): ?> disabled <?php endif; ?> onClick="this.form.submit(); this.disabled=true; this.innerText='<?php echo e(trans('general.please_wait'), false); ?>';" type="submit"><?php echo e(trans('general.save_changes'), false); ?></button>

          </form>
        </div><!-- end col-md-6 -->
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/subscription.blade.php ENDPATH**/ ?>