

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('general.posts'), false); ?> (<?php echo e($data->total(), false); ?>)
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

		    <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		      <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"> <?php echo e(trans('general.posts'), false); ?></h3>
                </div><!-- /.box-header -->



                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	<?php if($data->count() !=  0): ?>
                   <tr>
                      <th class="active">ID</th>
                      <th class="active"><?php echo e(trans('general.type'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.description'), false); ?></th>
                      <th class="active"><?php echo e(trans('auth.username'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.date'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.actions'), false); ?></th>
                    </tr>

                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $update): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr>
                      <td><?php echo e($update->id, false); ?></td>
                      <td>
                        <?php if($update->image !== ''): ?>
                        <i class="fa fa-image"></i> <?php echo e(trans('general.image'), false); ?>


                      <?php elseif($update->video !== ''): ?>
                        <i class="fa fa-video"></i> <?php echo e(trans('general.video'), false); ?>


                      <?php elseif($update->music !== ''): ?>
                      <i class="fa fa-music"></i>  <?php echo e(trans('general.audio'), false); ?>


                    <?php elseif($update->file !== ''): ?>
                      <i class="far fa-file-archive"></i>  <?php echo e(trans('general.file'), false); ?>

                      <?php else: ?>

                      <i class="fa fa-font"></i>    <?php echo e(trans('admin.text'), false); ?>

                      <?php endif; ?>
                      </td>
                      <td><?php echo e(str_limit($update->description, 50, '...'), false); ?></td>
                      <td><a href="<?php echo e(url($update->user()->username), false); ?>" target="_blank"><?php echo e($update->user()->username, false); ?> <i class="fa fa-external-link-square-alt"></i></a></td>
                      <td><?php echo e(Helper::formatDate($update->date), false); ?></td>
                      <td>
                      	<a href="<?php echo e(url($update->user()->username, 'post').'/'.$update->id, false); ?>" target="_blank" class="btn btn-success btn-sm padding-btn">
                      		<?php echo e(trans('admin.view'), false); ?> <i class="fa fa-external-link-square-alt"></i>
                      	</a>

                       <?php echo Form::open([
                         'method' => 'POST',
                         'url' => "panel/admin/posts/delete/$update->id",
                         'class' => 'displayInline'
                       ]); ?>


                       <?php echo Form::button(trans('admin.delete'), ['class' => 'btn btn-danger btn-sm padding-btn actionDelete']); ?>

                       <?php echo Form::close(); ?>


                      		</td>

                    </tr><!-- /.TR -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <hr />
                    	<h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>
                    <?php endif; ?>

                  </tbody>

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php if($data->hasPages()): ?>
                <?php echo e($data->links(), false); ?>

               <?php endif; ?>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/posts.blade.php ENDPATH**/ ?>