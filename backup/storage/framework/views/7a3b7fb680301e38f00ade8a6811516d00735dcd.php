<!-- FOOTER -->
<div class="py-5 <?php if(Auth::check() && auth()->user()->dark_mode == 'off' || Auth::guest() ): ?> footer_background_color footer_text_color <?php else: ?> bg-white <?php endif; ?> <?php if(Auth::check() && auth()->user()->dark_mode == 'off' && $settings->footer_background_color == '#ffffff' || Auth::guest() && $settings->footer_background_color == '#ffffff' ): ?> border-top <?php endif; ?>">
<footer class="container">
  <div class="row">
    <div class="col-md-3">
      <a href="<?php echo e(url('/'), false); ?>">
        <?php if(Auth::check() && auth()->user()->dark_mode == 'on' ): ?>
          <img src="<?php echo e(url('public/img', $settings->logo), false); ?>" alt="<?php echo e($settings->title, false); ?>" class="max-w-125">
        <?php else: ?>
          <img src="<?php echo e(url('public/img', $settings->logo_2), false); ?>" alt="<?php echo e($settings->title, false); ?>" class="max-w-125">
      <?php endif; ?>
      </a>
      <?php if($settings->twitter != ''
          || $settings->facebook != ''
          || $settings->instagram != ''
          || $settings->pinterest != ''
          || $settings->youtube != ''
          || $settings->github != ''
          ): ?>
      <div class="w-100">
        <span class="w-100"><?php echo e(trans('general.keep_connect_with_us'), false); ?> <?php echo e(trans('general.follow_us_social'), false); ?></span>
        <ul class="list-inline list-social">
          <?php if($settings->twitter != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->twitter, false); ?>" target="_blank" class="ico-social"><i class="fab fa-twitter"></i></a></li>
        <?php endif; ?>

        <?php if($settings->facebook != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->facebook, false); ?>" target="_blank" class="ico-social"><i class="fab fa-facebook"></i></a></li>
          <?php endif; ?>

          <?php if($settings->instagram != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->instagram, false); ?>" target="_blank" class="ico-social"><i class="fab fa-instagram"></i></a></li>
        <?php endif; ?>

          <?php if($settings->pinterest != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->pinterest, false); ?>" target="_blank" class="ico-social"><i class="fab fa-pinterest"></i></a></li>
          <?php endif; ?>

          <?php if($settings->youtube != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->youtube, false); ?>" target="_blank" class="ico-social"><i class="fab fa-youtube"></i></a></li>
          <?php endif; ?>

          <?php if($settings->github != ''): ?>
          <li class="list-inline-item"><a href="<?php echo e($settings->github, false); ?>" target="_blank" class="ico-social"><i class="fab fa-github"></i></a></li>
          <?php endif; ?>
        </ul>
      </div>
    <?php endif; ?>
    </div>
    <div class="col-md-3">
      <h5><?php echo app('translator')->get('general.about'); ?></h5>
      <ul class="list-unstyled">
        <?php $__currentLoopData = Pages::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li><a class="link-footer" href="<?php echo e(url('/p', $page->slug), false); ?>">
          <?php echo e(Lang::has('pages.' . $page->slug) ? __('pages.' . $page->slug) : $page->title, false); ?>

        </a>
      </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <li><a class="link-footer" href="<?php echo e(url('contact'), false); ?>"><?php echo e(trans('general.contact'), false); ?></a></li>
        <li><a class="link-footer" href="<?php echo e(url('blog'), false); ?>"><?php echo e(trans('general.blog'), false); ?></a></li>
      </ul>
    </div>
    <?php if(Categories::count() != 0): ?>
    <div class="col-md-3">
      <h5><?php echo app('translator')->get('general.categories'); ?></h5>
      <ul class="list-unstyled">
        <?php $__currentLoopData = Categories::where('mode','on')->orderBy('name')->take(6)->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li><a class="link-footer" href="<?php echo e(url('category', $category->slug), false); ?>"><?php echo e(Lang::has('categories.' . $category->slug) ? __('categories.' . $category->slug) : $category->name, false); ?></a></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        <?php if(Categories::count() > 6): ?>
          <li><a class="link-footer" href="<?php echo e(url('creators'), false); ?>"><?php echo e(trans('general.explore'), false); ?> <i class="fa fa-long-arrow-alt-right"></i></a></li>
          <?php endif; ?>
      </ul>
    </div>
  <?php endif; ?>
    <div class="col-md-3">
      <h5><?php echo app('translator')->get('general.links'); ?></h5>
      <ul class="list-unstyled">
      <?php if(auth()->guard()->guest()): ?>
        <li><a class="link-footer" href="<?php echo e($settings->home_style == 0 ? url('login') : url('/'), false); ?>"><?php echo e(trans('auth.login'), false); ?></a></li><li>
          <?php if($settings->registration_active == '1'): ?>
        <li><a class="link-footer" href="<?php echo e($settings->home_style == 0 ? url('signup') : url('/'), false); ?>"><?php echo e(trans('auth.sign_up'), false); ?></a></li><li>
        <?php endif; ?>
        <?php else: ?>
          <li><a class="link-footer url-user" href="<?php echo e(url(Auth::User()->username), false); ?>"><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.my_page') : trans('users.my_profile'), false); ?></a></li><li>
          <li><a class="link-footer" href="<?php echo e(url('settings/page'), false); ?>"><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?></a></li><li>
          <li><a class="link-footer" href="<?php echo e(url('my/subscriptions'), false); ?>"><?php echo e(trans('users.my_subscriptions'), false); ?></a></li><li>
          <li><a class="link-footer" href="<?php echo e(url('logout'), false); ?>"><?php echo e(trans('users.logout'), false); ?></a></li><li>
      <?php endif; ?>

      <?php if(auth()->guard()->guest()): ?>
      <div class="btn-group dropup d-inline ">
        <li>
          <a class="link-footer dropdown-toggle text-decoration-none" href="javascript:;" data-toggle="dropdown">
            <i class="fa fa-globe mr-1"></i>
            <?php $__currentLoopData = Languages::orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <?php if( $languages->abbreviation == config('app.locale') ): ?> <?php echo e($languages->name, false); ?>  <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </a>

        <div class="dropdown-menu">
          <?php $__currentLoopData = Languages::orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <a <?php if($languages->abbreviation != config('app.locale')): ?> href="<?php echo e(url('lang', $languages->abbreviation), false); ?>" <?php endif; ?> class="dropdown-item dropdown-lang <?php if( $languages->abbreviation == config('app.locale') ): ?> active text-white <?php endif; ?>">
            <?php if($languages->abbreviation == config('app.locale')): ?> <i class="fa fa-check mr-1"></i> <?php endif; ?> <?php echo e($languages->name, false); ?>

            </a>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        </li>
      </div><!-- dropup -->
      <?php endif; ?>

      </ul>
    </div>
  </div>
</footer>
</div>

<footer class="py-3 <?php if(Auth::check() && auth()->user()->dark_mode == 'off' || Auth::guest() ): ?> footer_background_color <?php endif; ?> text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12 copyright">
        &copy; <?php echo e(date('Y'), false); ?> <?php echo e($settings->title, false); ?>

      </div>
    </div>
  </div>
</footer>
<?php /**PATH /home/allajwno/public_html/backup/resources/views/includes/footer.blade.php ENDPATH**/ ?>