<link href="<?php echo e(asset('public/css/core.min.css'), false); ?>?v=<?php echo e($settings->version, false); ?>" rel="stylesheet">
<link href="<?php echo e(asset('public/css/feather.css'), false); ?>" rel="stylesheet">
<link href="<?php echo e(asset('public/css/bootstrap-icons.css'), false); ?>" rel="stylesheet">
<link href="<?php echo e(asset('public/css/icomoon.css'), false); ?>" rel="stylesheet">
<?php if(Auth::check() && auth()->user()->dark_mode == 'on'): ?>
  <link href="<?php echo e(asset('public/css/bootstrap-dark.min.css'), false); ?>" rel="stylesheet">
<?php else: ?>
  <link href="<?php echo e(asset('public/css/bootstrap.min.css'), false); ?>" rel="stylesheet">
<?php endif; ?>
<link href="<?php echo e(asset('public/css/styles.css'), false); ?>?v=<?php echo e($settings->version, false); ?>" rel="stylesheet">
<link href="<?php echo e(asset('public/css/smartphoto.min.css'), false); ?>" rel="stylesheet">
<?php if(Auth::check() && request()->path() == '/' || request()->route()->named('profile') || request()->is('messages/*') || request()->is('my/bookmarks') || request()->is('my/purchases')): ?>
<link href="<?php echo e(asset('public/js/plyr/plyr.css'), false); ?>?v=<?php echo e($settings->version, false); ?>" rel="stylesheet" type="text/css" />
<?php endif; ?>
<script type="text/javascript">
// Global variables
  var URL_BASE = "<?php echo e(url('/'), false); ?>";
  var _title = '<?php $__env->startSection("title"); ?><?php echo $__env->yieldSection(); ?> <?php echo e(e($settings->title), false); ?>';
  var session_status = "<?php echo e(Auth::check() ? 'on' : 'off', false); ?>";
  var totalPosts = <?php if(isset($updates)): ?> <?php echo e($updates->total(), false); ?><?php else: ?> 0 <?php endif; ?>;
  var ReadMore = "<?php echo e(trans('general.view_all'), false); ?>";
  var copiedSuccess = "<?php echo e(trans('general.copied_success'), false); ?>";
  var copied = "<?php echo e(trans('general.copied'), false); ?>";
  var copy_link = "<?php echo e(trans('general.copy_link'), false); ?>";
  var loading = "<?php echo e(trans('general.loading'), false); ?>";
  var please_wait = "<?php echo e(trans('general.please_wait'), false); ?>";
  var error_occurred = "<?php echo e(trans('general.error'), false); ?>";
  var error_oops = "<?php echo e(trans('general.error_oops'), false); ?>";
  var error_reload_page = "<?php echo e(trans('general.error_reload_page'), false); ?>";
  var ok = "<?php echo e(trans('users.ok'), false); ?>";
  var user_count_carousel = <?php if(Auth::guest() && request()->path() == '/'): ?> <?php echo e($users->count(), false); ?><?php else: ?> 0 <?php endif; ?>;
  var no_results_found = "<?php echo e(trans('general.no_results_found'), false); ?>";
  var no_results = "<?php echo e(trans('general.no_results'), false); ?>";
  var is_profile = <?php echo e(request()->route()->named('profile') ? 'true' : 'false', false); ?>;
  var error_scrollelement = false;
  var captcha = <?php echo e($settings->captcha == 'on' ? 'true' : 'false', false); ?>;
  var alert_adult = <?php echo e($settings->alert_adult == 'on' ? 'true' : 'false', false); ?>;
  var error_internet_disconnected = "<?php echo e(trans('general.error_internet_disconnected'), false); ?>";
<?php if(auth()->guard()->check()): ?>
  var is_bookmarks = <?php echo e(request()->is('my/bookmarks') ? 'true' : 'false', false); ?>;
  var is_purchases = <?php echo e(request()->is('my/purchases') ? 'true' : 'false', false); ?>;
  var delete_confirm = "<?php echo e(trans('general.delete_confirm'), false); ?>";
  var confirm_delete_comment = "<?php echo e(trans('general.confirm_delete_comment'), false); ?>";
  var confirm_delete_update = "<?php echo e(trans('general.confirm_delete_update'), false); ?>";
  var yes_confirm = "<?php echo e(trans('general.yes_confirm'), false); ?>";
  var cancel_confirm = "<?php echo e(trans('general.cancel_confirm'), false); ?>";
  var formats_available = "<?php echo e(trans('general.formats_available'), false); ?>";
  var formats_available_images = "<?php echo e(trans('general.formats_available_images'), false); ?>";
  var formats_available_verification = "<?php echo e(trans('general.formats_available_verification'), false); ?>";
  var file_size_allowed = <?php echo e($settings->file_size_allowed * 1024, false); ?>;
  var max_size_id = "<?php echo e(trans('general.max_size_id').' '.Helper::formatBytes($settings->file_size_allowed * 1024), false); ?>";
  var max_size_id_lang = "<?php echo e(trans('general.max_size_id').' '.Helper::formatBytes($settings->file_size_allowed_verify_account * 1024), false); ?>";
  var file_size_allowed_verify_account = <?php echo e($settings->file_size_allowed_verify_account * 1024, false); ?>;
  var error_width_min = "<?php echo e(trans('general.width_min',['data' => 20]), false); ?>";
  var story_length = <?php echo e($settings->story_length, false); ?>;
  var payment_card_error = "<?php echo e(trans('general.payment_card_error'), false); ?>";
  var confirm_delete_message = "<?php echo e(trans('general.confirm_delete_message'), false); ?>";
  var confirm_delete_conversation = "<?php echo e(trans('general.confirm_delete_conversation'), false); ?>";
  var confirm_cancel_subscription = "<?php echo trans('general.confirm_cancel_subscription'); ?>";
  var yes_confirm_cancel = "<?php echo e(trans('general.yes_confirm_cancel'), false); ?>";
  var confirm_delete_notifications = "<?php echo e(trans('general.confirm_delete_notifications'), false); ?>";
  var confirm_delete_withdrawal = "<?php echo e(trans('general.confirm_delete_withdrawal'), false); ?>";
  var change_cover = "<?php echo e(trans('general.change_cover'), false); ?>";
  var pin_to_your_profile = "<?php echo e(trans('general.pin_to_your_profile'), false); ?>";
  var unpin_from_profile = "<?php echo e(trans('general.unpin_from_profile'), false); ?>";
  var post_pinned_success = "<?php echo e(trans('general.post_pinned_success'), false); ?>";
  var post_unpinned_success = "<?php echo e(trans('general.post_unpinned_success'), false); ?>";
  var stripeKey = "<?php echo e(PaymentGateways::where('id', 2)->where('enabled', '1')->first() ? env('STRIPE_KEY') : false, false); ?>";
  var thanks = "<?php echo e(trans('general.thanks'), false); ?>";
  var tip_sent_success = "<?php echo e(trans('general.tip_sent_success'), false); ?>";
  var error_payment_stripe_3d = "<?php echo e(trans('general.error_payment_stripe_3d'), false); ?>";
  var colorStripe = <?php echo auth()->user()->dark_mode == 'on' ? "'#dcdcdc'" : "'#32325d'"; ?>;
  var full_name_user = '<?php echo e(auth()->user()->name, false); ?>';
  var color_default = '<?php echo e($settings->color_default, false); ?>';
  var formats_available_upload_file = "<?php echo e(trans('general.formats_available_upload_file'), false); ?>";
  var cancel_subscription = "<?php echo e(trans('general.unsubscribe'), false); ?>";
  var your_subscribed = "<?php echo e(trans('general.your_subscribed'), false); ?>";
  var subscription_expire = "<?php echo e(trans('general.subscription_expire'), false); ?>";
  var formats_available_verification_form_w9 = "<?php echo e(trans('general.formats_available_verification_form_w9', ['formats' => 'PDF']), false); ?>";
  var payment_was_successful = "<?php echo e(trans('general.payment_was_successful'), false); ?>";
<?php endif; ?>
</script>

<style type="text/css">

<?php if($settings->custom_css): ?>
  <?php echo $settings->custom_css; ?>

<?php endif; ?>

<?php if(auth()->check() && auth()->user()->dark_mode == 'on' ): ?>
  body { color: #FFF; }
  .dd-menu-user:before { color: #222222; }
  .dropdown-item.balance:hover {background: #222 !important;color: #ffffff;}
  .blocked {background-color: transparent;}
  .btn-google, .btn-google:hover, .btn-google:active, .btn-google:focus {
  background: transparent;
  border-color: #ccc;
  color: #fff;
}

.img-user,
.avatar-modal,
.img-user-small { border-color: #303030; }
.actionDeleteNotify,
.actionDeleteNotify:hover { color: #FFF; }

.nav-profile a, .nav-profile li.active a:hover, .nav-profile li.active a:active, .nav-profile li.active a:focus,
.sm-btn-size, .verified {
  color: #fff;
}
.text-featured {color: #fff !important;}
.input-group-text {
  border-color: #222;
  background-color: #303030;
}
.datepicker.dropdown-menu {background-color: #303030 !important;}
.datepicker-dropdown.datepicker-orient-bottom:after {border-top: 6px solid #303030 !important;}
.datepicker-dropdown:after {border-bottom: 6px solid #303030 !important;}

.form-control:focus, .custom-select:focus {
  border-color: #222 !important;
}
.custom-select {
  background: #303030 url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='4' height='5' viewBox='0 0 4 5'%3e%3cpath fill='%23a5a5a5' d='M2 0L0 2h4zm0 5L0 3h4z'/%3e%3c/svg%3e")
  no-repeat right .75rem center/8px 10px;
  color: #fff;
}
.navbar-toggler,
.sweet-alert h2,
.sweet-alert p,
.ico-no-result {
  color: #FFF;
}
.sweet-alert { background-color: #2f2f2f;}
.content-locked {background: #444444;}

@media (max-width: 991px) {
.navbar .navbar-collapse {
  background: #222;
}
.navbar .navbar-collapse .navbar-nav .nav-item .nav-link:not(.btn) {
  color: #ffffff;
}

.navbar-collapse .navbar-toggler span {
  background: #fff;
}
}
.link-scroll a.nav-link:not(.btn) {
  color: #969696;
}
.btn-upload:hover {
background-color: #222222;
}
.btn-active-hover {
background-color: #222222 !important;
}
.modal-danger .modal-content {
background-color: #303030;
}
h3, .h3 {font-size: 1.75rem;}
h2, .h2 {font-size: 2rem;}
h4, .h4 {font-size: 1.5rem;}
h5, .h5 {font-size: 1.25rem;}

@keyframes  animate {
from {transition:none;}
to {background-color:#383838;transition: all 0.3s ease-out;}
}

.item-loading::before {
  background-color: #6b6b6b;
  content: ' ';
  display: block;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  animation-name: animate;
  animation-duration: 2s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  background-image: none;
  border-radius: 0;
}
.loading-avatar::before {
border-radius: 50%;
}
.loading-avatar {background-color: inherit;}
.plyr--audio .plyr__controls {background: #212121; color: #ffffff;}
.readmore-js-collapsed:after {background-image: linear-gradient(hsla(0,0%,100%,0),#303030 95%);}
.sweet-alert .sa-icon.sa-success .sa-fix {background-color: #2f2f2f;}
.sweet-alert .sa-icon.sa-success::after, .sweet-alert .sa-icon.sa-success::before {background: #2f2f2f;}
.page-item.disabled .page-link, .page-link {background-color: #222222;}
.nav-pills .nav-link {background-color: #303030; color: #ffffff;}
a.social-share i {color: #dedede!important;}

.StripeElement {background-color: #222222; border: 1px solid #222222;}
.StripeElement--focus {border-color: #525252;}
.bg-autocomplete {background-color: #222;}

<?php endif; ?>

.bg-gradient {
  background: url('<?php echo e(url('public/img', $settings->bg_gradient), false); ?>');
  background-size: cover;
}

a.social-share i {color: #797979; font-size: 32px;}
a:hover.social-share { text-decoration: none; }
.btn-whatsapp {color: #50b154 !important;}
.close-inherit {color: inherit !important;}
.btn-twitter { background-color: #1da1f2;  color:#fff !important;}

@media (max-width: 991px) {
  .navbar-user-mobile {
    font-size: 20px;
  }
}

.or {
display:flex;
justify-content:center;
align-items: center;
color:grey;
}

.or:after,
.or:before {
  content: "";
  display: block;
  background: #adb5bd;
  width: 50%;
  height:1px;
  margin: 0 10px;
}

.icon-navbar { font-size: 23px; vertical-align: bottom; <?php if(auth()->check() && auth()->user()->dark_mode == 'on'): ?> color: #FFF !important; <?php endif; ?> }

<?php echo e($settings->button_style == 'rounded' ? '.btn {border-radius: 50rem!important;}' : null, false); ?>


<?php if(auth()->check() && auth()->user()->dark_mode == 'off' || auth()->guest()): ?>
.navbar_background_color { background-color: <?php echo e($settings->navbar_background_color, false); ?> !important; }
.link-scroll a.nav-link:not(.btn), .navbar-toggler:not(.text-white) { color: <?php echo e($settings->navbar_text_color, false); ?> !important; }

@media (max-width: 991px) {
  .navbar .navbar-collapse, .dd-menu-user, .dropdown-item.balance:hover { background-color: <?php echo e($settings->navbar_background_color, false); ?> !important; color: <?php echo e($settings->navbar_text_color, false); ?> !important; }
  .dd-menu-user a, .dropdown-item:not(.dropdown-lang) { color: <?php echo e($settings->navbar_text_color, false); ?> }
  .navbar-collapse .navbar-toggler span { background-color: <?php echo e($settings->navbar_text_color, false); ?> !important; }
  .dropdown-divider { border-top-color: <?php echo e($settings->navbar_background_color, false); ?> !important;}
  }

.footer_background_color { background-color: <?php echo e($settings->footer_background_color, false); ?> !important; }
.footer_text_color, .link-footer:not(.footer-tiny) { color: <?php echo e($settings->footer_text_color, false); ?>; }
<?php endif; ?>

<?php if($settings->color_default <> ''): ?>

:root {
  --plyr-color-main: <?php echo e($settings->color_default, false); ?>;
}

.plyr--video.plyr--stopped .plyr__controls {display: none;}

@media (min-width: 767px) {
  .login-btn { padding-top: 12px !important;}
}

::selection{ background-color: <?php echo e($settings->color_default, false); ?>; color: white; }
::moz-selection{ background-color: <?php echo e($settings->color_default, false); ?>; color: white; }
::webkit-selection{ background-color: <?php echo e($settings->color_default, false); ?>; color: white; }

body a,
a:hover,
a:focus,
a.page-link,
.btn-outline-primary {
    color: <?php echo e($settings->color_default, false); ?>;
}
.text-primary {
    color: <?php echo e($settings->color_default, false); ?>!important;
}

a.text-primary.btnBookmark:hover, a.text-primary.btnBookmark:focus {
  color: <?php echo e($settings->color_default, false); ?>!important;
}

.btn-primary:not(:disabled):not(.disabled).active,
.btn-primary:not(:disabled):not(.disabled):active,
.show>.btn-primary.dropdown-toggle,
.btn-primary:hover,
.btn-primary:focus,
.btn-primary:active,
.btn-primary,
.btn-primary.disabled,
.btn-primary:disabled,
.custom-checkbox .custom-control-input:checked ~ .custom-control-label::before,
.page-item.active .page-link,
.page-link:hover,
.owl-theme .owl-dots .owl-dot span,
.owl-theme .owl-dots .owl-dot.active span,
.owl-theme .owl-dots .owl-dot:hover span
 {
    background-color: <?php echo e($settings->color_default, false); ?>;
    border-color: <?php echo e($settings->color_default, false); ?>;
}
.bg-primary,
.dropdown-item:focus,
.dropdown-item:hover,
.dropdown-item.active,
.dropdown-item:active,
.tooltip-inner,
.custom-range::-webkit-slider-thumb,
.custom-range::-webkit-slider-thumb:active {
    background-color: <?php echo e($settings->color_default, false); ?>!important;
}

.custom-range::-moz-range-thumb:active,
.custom-range::-ms-thumb:active {
  background-color: <?php echo e($settings->color_default, false); ?>!important;
}

.custom-checkbox .custom-control-input:indeterminate ~ .custom-control-label::before,
.custom-control-input:focus:not(:checked) ~ .custom-control-label::before,
.btn-outline-primary {
  border-color: <?php echo e($settings->color_default, false); ?>;
}
.custom-control-input:not(:disabled):active~.custom-control-label::before,
.custom-control-input:checked~.custom-control-label::before,
.btn-outline-primary:hover,
.btn-outline-primary:focus,
.btn-outline-primary:not(:disabled):not(.disabled):active,
.list-group-item.active {
    color: #fff;
    background-color: <?php echo e($settings->color_default, false); ?>;
    border-color: <?php echo e($settings->color_default, false); ?>;
}
.popover .arrow::before { border-top-color: rgba(0,0,0,.35) !important; }
.bs-tooltip-bottom .arrow::before {
  border-bottom-color: <?php echo e($settings->color_default, false); ?>!important;
}
.arrow::before {
  border-top-color: <?php echo e($settings->color_default, false); ?>!important;
}
.nav-profile li.active {
  border-bottom: 3px solid <?php echo e($settings->color_default, false); ?>!important;
}
.button-avatar-upload {left: 0;}
input[type='file'] {overflow: hidden;}
.badge-free { top: 10px; right: 10px; background: rgb(0 0 0 / 65%); color: #fff; font-size: 12px;}

.btn-facebook, .btn-twitter, .btn-google {position: relative;}
.btn-facebook i, .btn-twitter i  {
  position: absolute;
    left: 10px;
    bottom: 14px;
    width: 36px;
}

.btn-google img  {
  position: absolute;
    left: 18px;
    bottom: 12px;
    width: 18px;
}

.button-search {top: 0;}

@media (min-width: 768px) {
  .pace {display:none !important;}
}

@media (min-width: 992px) {
  .menuMobile {display:none !important;}
}

.pace{-webkit-pointer-events:none;pointer-events:none;-webkit-user-select:none;-moz-user-select:none;user-select:none}
.pace-inactive{display:none}
.pace .pace-progress{background:<?php echo e($settings->color_default, false); ?>;position:fixed;z-index:2000;top:0;right:100%;width:100%;height:3px}

.menuMobile {
  position: fixed;
  bottom: 0;
  left: 0;
  z-index: 1040;
  <?php if(auth()->check() && auth()->user()->dark_mode == 'off'): ?>
    background-color: <?php echo e($settings->navbar_background_color, false); ?> !important;
  <?php endif; ?>

}
.btn-mobile {border-radius: 25px;
  <?php if(auth()->check() && auth()->user()->dark_mode == 'off'): ?>
  color: <?php echo e($settings->navbar_text_color, false); ?> !important;
  <?php endif; ?>
}
.btn-mobile:hover {
    background-color: rgb(243 243 243 / 26%);
    text-decoration: none !important;
    -webkit-transition: all 200ms linear;
    -moz-transition: all 200ms linear;
    -o-transition: all 200ms linear;
    -ms-transition: all 200ms linear;
    transition: all 200ms linear;
}

@media (max-width: 991px) {
  .navbar .navbar-collapse {
    width: 300px !important;
    box-shadow: 5px 0px 8px #000;
  }

  .section-msg {padding: 0 !important;}

  #navbarUserHome { position: initial !important;}

  .notify {
    top: 5px !important;
    right: 5px !important;
  }

  <?php if(auth()->guard()->check()): ?>
  .margin-auto {
      margin: auto!important;
  }
  <?php endif; ?>
}
.sidebar-overlay #mobileMenuOverlay {
    position: fixed;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.8);
    z-index: 101;
    -webkit-transition: all .9s;
    -moz-transition: all .8s;
    -ms-transition: all .8s;
    -o-transition: all .8s;
    transition: all .8s;
    transition-delay: .35s;
    left: 0;
}
.noti_notifications, .noti_msg {display: none;}

.link-menu-mobile {border-radius: 6px;}
.link-menu-mobile:hover:not(.balance) {
  background: rgb(242 242 242 / 40%);
}
a.link-border {text-decoration: none;}
@media (max-width: 479px) {
  .card-updates {border-right: 0; border-left: 0; border-radius: 0;}
  .card-form-post {border-radius: 0;}
  .wrap-post {padding: 0 !important;}
}

@media (min-width: 576px) {
  .modal-login {
      max-width: 415px;
  }
}
.toggleComments { cursor: pointer;}
.blocked {left: 0; top: 0;}
.card-settings > .list-group-flush>.list-group-item {border-width: 0 0 0px !important;}
.btn-active-hover {background-color: #f3f3f3;}

/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
.container-msg {position: relative; overflow: auto; overflow-x: hidden; flex: 2; -webkit-box-flex: 2;}
.section-msg {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-flow: column;
    flex-flow: column;
    min-width: 0;
    width: 100%;
}
.container-media-msg {max-width: 100%;max-height: 100%;}
.container-media-img {max-width: 100%;}
.rounded-top-right-0 {border-top-right-radius: 0 !important;}
.rounded-top-left-0{border-top-left-radius: 0 !important;}
.custom-rounded {border-radius: 10px;}
<?php endif; ?>
</style>
<?php /**PATH /home/allajwno/public_html/backup/resources/views/includes/css_general.blade.php ENDPATH**/ ?>