

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/select2/select2.min.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('general.billing_information'), false); ?>


          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(session('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(session('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo e(trans('general.billing_information'), false); ?></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/billing'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                 <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.company'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->company, false); ?>" name="company" class="form-control" placeholder="<?php echo e(trans('general.company'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.select_your_country'), false); ?></label>
                      <div class="col-sm-10">
                        <select name="country" class="form-control custom-select select2">
                          <option value=""><?php echo e(trans('general.select_your_country'), false); ?></option>
                              <?php $__currentLoopData = Countries::orderBy('country_name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php if($settings->country == $country->country_name): ?> selected="selected" <?php endif; ?> value="<?php echo e($country->country_name, false); ?>"><?php echo e($country->country_name, false); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                   <div class="box-body">
                     <div class="form-group">
                       <label class="col-sm-2 control-label"><?php echo e(trans('general.address'), false); ?></label>
                       <div class="col-sm-10">
                         <input type="text" value="<?php echo e($settings->address, false); ?>" name="address" class="form-control" placeholder="<?php echo e(trans('general.address'), false); ?>">
                       </div>
                     </div>
                   </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label"><?php echo e(trans('general.city'), false); ?></label>
                        <div class="col-sm-10">
                          <input type="text" value="<?php echo e($settings->city, false); ?>" name="city" class="form-control" placeholder="<?php echo e(trans('general.city'), false); ?>">
                        </div>
                      </div>
                    </div><!-- /.box-body -->

                    <!-- Start Box Body -->
                     <div class="box-body">
                       <div class="form-group">
                         <label class="col-sm-2 control-label"><?php echo e(trans('general.zip'), false); ?></label>
                         <div class="col-sm-10">
                           <input type="text" value="<?php echo e($settings->zip, false); ?>" name="zip" class="form-control" placeholder="<?php echo e(trans('general.zip'), false); ?>">
                         </div>
                       </div>
                     </div><!-- /.box-body -->

                     <!-- Start Box Body -->
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label"><?php echo e(trans('general.vat'), false); ?></label>
                          <div class="col-sm-10">
                            <input type="text" value="<?php echo e($settings->vat, false); ?>" name="vat" class="form-control" placeholder="<?php echo e(trans('general.vat'), false); ?>">
                          </div>
                        </div>
                      </div><!-- /.box-body -->


                  <div class="box-footer">
                    <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>

        		</div><!-- /.row -->

        	</div><!-- /.content -->

          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/billing.blade.php ENDPATH**/ ?>