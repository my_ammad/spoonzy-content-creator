

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.payment_settings'), false); ?> <i class="fa fa-angle-right margin-separator"></i>
                Stripe
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url()->current(), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

          <hr />

          <!-- Start Box Body -->
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label"><?php echo e(trans('admin.fee'), false); ?></label>
               <div class="col-sm-10">
                 <input type="text" value="<?php echo e($data->fee, false); ?>" name="fee" class="form-control" placeholder="<?php echo e(trans('admin.fee'), false); ?>">
               </div>
             </div>
           </div><!-- /.box-body -->

           <!-- Start Box Body -->
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo e(trans('admin.fee_cents'), false); ?></label>
                <div class="col-sm-10">
                  <input type="text" value="<?php echo e($data->fee_cents, false); ?>" name="fee_cents" class="form-control" placeholder="<?php echo e(trans('admin.fee_cents'), false); ?>">
                </div>
              </div>
            </div><!-- /.box-body -->

            <!-- Start Box Body -->
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Stripe Publishable Key</label>
                <div class="col-sm-10">
                  <input type="text" value="<?php echo e($data->key, false); ?>" name="key" class="form-control">
                 <p class="help-block"><a href="https://dashboard.stripe.com/account/apikeys" target="_blank">https://dashboard.stripe.com/account/apikeys</a></p>
                </div>
              </div>
            </div><!-- /.box-body -->

            <!-- Start Box Body -->
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Stripe Secret Key</label>
                <div class="col-sm-10">
                  <input type="password" value="<?php echo e($data->key_secret, false); ?>" name="key_secret" class="form-control">
                 <p class="help-block"><a href="https://dashboard.stripe.com/account/apikeys" target="_blank">https://dashboard.stripe.com/account/apikeys</a></p>
                </div>
              </div>
            </div><!-- /.box-body -->

            <!-- Start Box Body -->
            <div class="box-body">
              <div class="form-group">
                <label class="col-sm-2 control-label">Stripe Webhook Secret</label>
                <div class="col-sm-10">
                  <input type="password" value="<?php echo e(env('STRIPE_WEBHOOK_SECRET'), false); ?>" name="webhook_secret" class="form-control">
                 <p class="help-block"><a href="https://dashboard.stripe.com/webhooks" target="_blank">https://dashboard.stripe.com/webhooks</a></p>
                </div>
              </div>
            </div><!-- /.box-body -->

               <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label"><?php echo e(trans('admin.status'), false); ?></label>
                   <div class="col-sm-10">
                     <div class="radio">
                     <label class="padding-zero">
                       <input type="radio" value="1" name="enabled" <?php if( $data->enabled == 1 ): ?> checked="checked" <?php endif; ?> checked>
                       <?php echo e(trans('admin.active'), false); ?>

                     </label>
                   </div>
                   <div class="radio">
                     <label class="padding-zero">
                       <input type="radio" value="0" name="enabled" <?php if( $data->enabled == 0 ): ?> checked="checked" <?php endif; ?>>
                       <?php echo e(trans('admin.disabled'), false); ?>

                     </label>
                   </div>
                   </div>
                 </div>
               </div><!-- /.box-body -->

               <div class="box-footer">
                 <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
               </div><!-- /.box-footer -->
               </form>

              </div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/stripe-settings.blade.php ENDPATH**/ ?>