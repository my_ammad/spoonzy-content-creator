<?php $__env->startSection('title'); ?> <?php echo e(trans('general.privacy_security'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="bi bi-shield-check mr-2"></i> <?php echo e(trans('general.privacy_security'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('general.desc_privacy'), false); ?></p>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          <?php if(session('status')): ?>
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    <?php echo e(session('status'), false); ?>

                  </div>
                <?php endif; ?>

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

          <?php if($sessions): ?>
          <h5><?php echo e(__('general.login_sessions'), false); ?></h5>
              <div class="card mb-4">
                <div class="card-body">
                  <small class="w-100 d-block"><strong><?php echo e(__('general.last_login_record'), false); ?></strong></small>
                  <p class="card-text"><?php echo e($sessions->user_agent, false); ?></p>
                  <p>
                    <span>IP: <?php echo e($sessions->ip_address, false); ?>


            <span class="w-100 d-block mt-2">
              <?php if($current_session_id == $sessions->id): ?>
                <button type="button" :disabled="true" class="btn btn-sm btn-primary e-none"><?php echo e(__('general.this_device'), false); ?></button>
                <?php else: ?>
                  <form method="POST" action="<?php echo e(url('logout/session', $sessions->id), false); ?>">
                    <?php echo csrf_field(); ?>
                    <button type="submit" class="btn btn-sm btn-danger"><i class="feather icon-trash-2"></i> <?php echo e(__('general.delete'), false); ?></button>
                  </form>
                <?php endif; ?>
            </span>
                  </p>
                </div>
              </div>
          <?php endif; ?>

          <?php if(auth()->user()->verified_id == 'yes'): ?>

            <h5><?php echo e(__('general.privacy'), false); ?></h5>

            <form method="POST" action="<?php echo e(url('privacy/security'), false); ?>">

              <?php echo csrf_field(); ?>

              <div class="form-group">
                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_profile" value="yes" <?php if(auth()->user()->hide_profile == 'yes'): ?> checked <?php endif; ?> id="customSwitch1">
                    <label class="custom-control-label switch" for="customSwitch1"><?php echo e(__('general.hide_profile'), false); ?> <?php echo e(__('general.info_hide_profile'), false); ?></label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_last_seen" value="yes" <?php if(auth()->user()->hide_last_seen == 'yes'): ?> checked <?php endif; ?> id="customSwitch2">
                    <label class="custom-control-label switch" for="customSwitch2"><?php echo e(__('general.hide_last_seen'), false); ?></label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_count_subscribers" value="yes" <?php if(auth()->user()->hide_count_subscribers == 'yes'): ?> checked <?php endif; ?> id="customSwitch3">
                    <label class="custom-control-label switch" for="customSwitch3"><?php echo e(__('general.hide_count_subscribers'), false); ?></label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="hide_my_country" value="yes" <?php if(auth()->user()->hide_my_country == 'yes'): ?> checked <?php endif; ?> id="customSwitch4">
                    <label class="custom-control-label switch" for="customSwitch4"><?php echo e(__('general.hide_my_country'), false); ?></label>
                  </div>
                </div>

                <div class="btn-block mb-4">
                  <div class="custom-control custom-switch custom-switch-lg">
                    <input type="checkbox" class="custom-control-input" name="show_my_birthdate" value="yes" <?php if(auth()->user()->show_my_birthdate == 'yes'): ?> checked <?php endif; ?> id="customSwitch5">
                    <label class="custom-control-label switch" for="customSwitch5"><?php echo e(__('general.show_my_birthdate'), false); ?></label>
                  </div>
                </div>

              </div><!-- End form-group -->

              <button class="btn btn-1 btn-success btn-block" onClick="this.form.submit(); this.disabled=true; this.innerText='<?php echo e(__('general.please_wait'), false); ?>';" type="submit"><?php echo e(__('general.save_changes'), false); ?></button>

            </form>
          <?php endif; ?>

        </div><!-- end col-md-6 -->
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/users/privacy_security.blade.php ENDPATH**/ ?>