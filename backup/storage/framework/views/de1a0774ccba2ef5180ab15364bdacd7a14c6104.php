

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('admin.subscriptions'), false); ?> (<?php echo e($data->total(), false); ?>)
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">
        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                  		<?php echo e(trans('admin.subscriptions'), false); ?>

                  	</h3>
                </div><!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	<?php if($data->total() !=  0 && $data->count() != 0): ?>
                   <tr>
                      <th class="active">ID</th>
                      <th class="active"><?php echo e(trans('general.user'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.subscriber'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.date'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.status'), false); ?></th>
                    </tr><!-- /.TR -->

                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($subscription->id, false); ?></td>
                      <td>
                        <?php if( ! isset($subscription->subscribed()->username)): ?>
                          <?php echo e(trans('general.no_available'), false); ?>

                        <?php else: ?>
                        <a href="<?php echo e(url($subscription->subscribed()->username), false); ?>" target="_blank">
                          <?php echo e($subscription->subscribed()->name, false); ?> <i class="fa fa-external-link-square"></i>
                        </a>
                      <?php endif; ?>
                      </td>
                      <td>
                        <?php if( ! isset($subscription->user()->username)): ?>
                          <?php echo e(trans('general.no_available'), false); ?>

                        <?php else: ?>
                        <a href="<?php echo e(url($subscription->user()->username), false); ?>" target="_blank">
                          <?php echo e($subscription->user()->name, false); ?>

                        </a>
                        <?php endif; ?>
                      </td>
                      <td><?php echo e(Helper::formatDate($subscription->created_at), false); ?></td>
                      <td>
                        <?php if($subscription->stripe_id == ''
                          && strtotime($subscription->ends_at) > strtotime(now()->format('Y-m-d H:i:s'))
                          && $subscription->cancelled == 'no'
                            || $subscription->stripe_id != '' && $subscription->stripe_status == 'active'
                            || $subscription->stripe_id == '' && $subscription->free == 'yes'
                          ): ?>
                          <span class="label label-success"><?php echo e(trans('general.active'), false); ?></span>
                        <?php elseif($subscription->stripe_id != '' && $subscription->stripe_status == 'incomplete'): ?>
                          <span class="label label-warning"><?php echo e(trans('general.incomplete'), false); ?></span>
                        <?php else: ?>
                          <span class="label label-danger"><?php echo e(trans('general.cancelled'), false); ?></span>
                        <?php endif; ?>
                      </td>
                    </tr><!-- /.TR -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <hr />
                    	<h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>

                    <?php endif; ?>

                  </tbody>

                  </table>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php if($data->lastPage() > 1): ?>
             <?php echo e($data->links(), false); ?>

             <?php endif; ?>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/subscriptions.blade.php ENDPATH**/ ?>