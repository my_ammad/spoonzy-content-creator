<div class="card border-0 bg-transparent">
  <div class="card-body p-0">
    <small class="text-muted">&copy; <?php echo e(date('Y'), false); ?> <?php echo e($settings->title, false); ?></small>
    <ul class="list-inline mb-0 small">
      <?php $__currentLoopData = Pages::all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li class="list-inline-item"><a class="link-footer footer-tiny" href="<?php echo e(url('/p', $page->slug), false); ?>">
        <?php echo e(Lang::has('pages.' . $page->slug) ? __('pages.' . $page->slug) : $page->title, false); ?>

      </a>
      </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <li class="list-inline-item"><a class="link-footer footer-tiny" href="<?php echo e(url('contact'), false); ?>"><?php echo e(trans('general.contact'), false); ?></a></li>
      <li class="list-inline-item"><a class="link-footer footer-tiny" href="https://www.instagram.com/alladmirers/">Instagram</a></li>

    <?php if(auth()->guard()->guest()): ?>
    <div class="btn-group dropup d-inline">
      <li class="list-inline-item">
        <a class="link-footer dropdown-toggle text-decoration-none footer-tiny" href="javascript:;" data-toggle="dropdown">
          <i class="fa fa-globe mr-1"></i>
          <?php $__currentLoopData = Languages::orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if( $languages->abbreviation == config('app.locale') ): ?> <?php echo e($languages->name, false); ?>  <?php endif; ?>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </a>

      <div class="dropdown-menu">
        <?php $__currentLoopData = Languages::orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <a <?php if($languages->abbreviation != config('app.locale')): ?> href="<?php echo e(url('lang', $languages->abbreviation), false); ?>" <?php endif; ?> class="dropdown-item <?php if( $languages->abbreviation == config('app.locale') ): ?> active text-white <?php endif; ?>">
          <?php if($languages->abbreviation == config('app.locale')): ?> <i class="fa fa-check mr-1"></i> <?php endif; ?> <?php echo e($languages->name, false); ?>

          </a>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      </li>
    </div><!-- dropup -->
    <?php endif; ?>

    </ul>
  </div>
</div>
<?php /**PATH /home/allajwno/public_html/resources/views/includes/footer-tiny.blade.php ENDPATH**/ ?>