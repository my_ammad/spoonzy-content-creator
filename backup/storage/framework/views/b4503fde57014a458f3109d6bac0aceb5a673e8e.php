

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
           <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('general.withdrawals'), false); ?> #<?php echo e($data->id, false); ?>

          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">

              	<div class="box-body">
              		<dl class="dl-horizontal">

					  <!-- start -->
					  <dt>ID</dt>
					  <dd><?php echo e($data->id, false); ?></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans('general.user'), false); ?></dt>
					  <dd>
              <?php if( ! isset($data->user()->username)): ?>
                <?php echo e(trans('general.no_available'), false); ?>

              <?php else: ?>
              <a href="<?php echo e(url($data->user()->username), false); ?>" target="_blank"><?php echo e($data->user()->name, false); ?></a>
            <?php endif; ?>
            </dd>
					  <!-- ./end -->

					<?php if( $data->gateway == 'PayPal' ): ?>
					  <!-- start -->
					  <dt><?php echo e(trans('admin.paypal_account'), false); ?></dt>
					  <dd><?php echo e($data->account, false); ?></dd>
					  <!-- ./end -->

					  <?php else: ?>
					   <!-- start -->
					  <dt><?php echo e(trans('general.bank_details'), false); ?></dt>
					  <dd>
					    <?php 
					    $uid = $data->user()->id;
					    $wise = App\Models\Wise::where('user_id', $uid)->first();
					    //echo $uid;
					    ?>
					    <div class="d-inline">
                          <div>Acount Type: <b><?php echo e($wise->ac_type, false); ?></b></div>
                          <div>ACH Routing number: <br><?php echo e($wise->routing_no, false); ?></div>
                          <div>Bank account number: <br><?php echo e($wise->ac_no, false); ?></div>
                        </div>
					  </dd>
					  <!-- ./end -->
                        
					  <?php endif; ?>

					  <!-- start -->
					  <dt><?php echo e(trans('admin.amount'), false); ?></dt>
					  <dd><strong class="text-success"><?php echo e(Helper::amountFormatDecimal($data->amount), false); ?></strong></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans('general.payment_gateway'), false); ?></dt>
					  <dd><?php echo e($data->gateway, false); ?></dd>
					  <!-- ./end -->


					  <!-- start -->
					  <dt><?php echo e(trans('admin.date'), false); ?></dt>
					  <dd><?php echo e(date($settings->date_format, strtotime($data->date)), false); ?></dd>
					  <!-- ./end -->

					  <!-- start -->
					  <dt><?php echo e(trans('admin.status'), false); ?></dt>
					  <dd>
					  	<?php if( $data->status == 'paid' ): ?>
                      	<span class="label label-success"><?php echo e(trans('general.paid'), false); ?></span>
                      	<?php else: ?>
                      	<span class="label label-warning"><?php echo e(trans('general.pending_to_pay'), false); ?></span>
                      	<?php endif; ?>
					  </dd>
					  <!-- ./end -->

					<?php if( $data->status == 'paid' ): ?>
					  <!-- start -->
					  <dt><?php echo e(trans('general.date_paid'), false); ?></dt>
					  <dd>
					  	<?php echo e(date('d M, y', strtotime($data->date_paid)), false); ?>

					  </dd>
					  <!-- ./end -->
					  <?php endif; ?>
            
					</dl>
              	</div><!-- box body -->

              	<div class="box-footer">
                  	 <a href="<?php echo e(url('panel/admin/withdrawals'), false); ?>" class="btn btn-default"><?php echo e(trans('auth.back'), false); ?></a>

                  <?php if( $data->status == 'pending' ): ?>
                  <?php if($data->gateway == 'Bank'): ?> 
                  <a href='<?php echo e(url("panel/admin/withdrawal_wise/$data->id"), false); ?>' class="btn btn-success pull-right myicon-right"><?php echo e(trans('Wise Confirm'), false); ?></a>
                  <?php else: ?>
                <?php echo Form::open([
			            'method' => 'POST',
			            'url' => "panel/admin/withdrawals/paid/$data->id",
			            'class' => 'displayInline'
				        ]); ?>


	            	<?php echo Form::submit(trans('general.mark_paid'), ['class' => 'btn btn-success pull-right myicon-right']); ?>

	        	<?php echo Form::close(); ?>

                  <?php endif; ?>    
	        	<?php endif; ?>
            </div><!-- /.box-footer -->
        </div><!-- box -->
      </div><!-- col -->
   </div><!-- row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/withdrawal-view.blade.php ENDPATH**/ ?>