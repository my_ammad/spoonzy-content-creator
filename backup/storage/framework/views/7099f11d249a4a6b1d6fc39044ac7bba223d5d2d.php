<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="_token" content="<?php echo csrf_token(); ?>"/>
    <title><?php echo e(trans('admin.admin'), false); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link href="<?php echo e(asset('public/bootstrap/css/bootstrap.min.css'), false); ?>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo e(asset('public/css/fontawesome.min.css'), false); ?>" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="<?php echo e(asset('public/admin/css/app.css'), false); ?>" rel="stylesheet" type="text/css" />
    <!-- IcoMoon CSS -->
    <link href="<?php echo e(asset('public/css/icomoon.css'), false); ?>" rel="stylesheet">
     <!-- Theme style -->
    <link href="<?php echo e(asset('public/admin/css/AdminLTE.min.css'), false); ?>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo e(asset('public/admin/css/skins/skin-black.min.css'), false); ?>" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="<?php echo e(url('public/img', $settings->favicon), false); ?>" />

    <link href='//fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>

    <link href="<?php echo e(asset('public/plugins/sweetalert/sweetalert.css'), false); ?>" rel="stylesheet" type="text/css" />

    <?php echo $__env->yieldContent('css'); ?>

  <script type="text/javascript">
    var URL_BASE = "<?php echo e(url('/'), false); ?>";
    var url_file_upload = "<?php echo e(route('upload.image', ['_token' => csrf_token()]), false); ?>";
    var delete_confirm = "<?php echo e(trans('general.delete_confirm'), false); ?>";
    var yes_confirm = "<?php echo e(trans('general.yes_confirm'), false); ?>";
    var yes = "<?php echo e(trans('general.yes'), false); ?>";
    var cancel_confirm = "<?php echo e(trans('general.cancel_confirm'), false); ?>";
    var timezone = "<?php echo e(env('TIMEZONE'), false); ?>";
    var add_tag = "<?php echo e(trans("general.add_tag"), false); ?>";
    var choose_image = '<?php echo e(trans('general.choose_image'), false); ?>';
    var formats_available = "<?php echo e(trans('general.formats_available'), false); ?>";
    var cancel_payment = "<?php echo trans('general.confirm_cancel_payment'); ?>";
    var yes_cancel_payment = "<?php echo e(trans('general.yes_cancel_payment'), false); ?>";
    var approve_confirm_verification = "<?php echo e(trans('admin.approve_confirm_verification'), false); ?>";
    var yes_confirm_approve_verification = "<?php echo e(trans('admin.yes_confirm_approve_verification'), false); ?>";
    var yes_confirm_verification = "<?php echo e(trans('admin.yes_confirm_verification'), false); ?>";
    var delete_confirm_verification = "<?php echo e(trans('admin.delete_confirm_verification'), false); ?>";
    var login_as_user_warning = "<?php echo e(trans('general.login_as_user_warning'), false); ?>";
  </script>

  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="skin-black sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo e(url('panel/admin'), false); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b><i class="fas fa-bolt"></i></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b><i class="fas fa-bolt"></i> <?php echo e(trans('admin.admin'), false); ?></b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <i class="fa fa-bars"></i>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

            	<li>
            		<a href="<?php echo e(url('/'), false); ?>"><i class="glyphicon glyphicon-home myicon-right"></i> <?php echo e(trans('admin.view_site'), false); ?></a>
            	</li>

              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>" class="user-image" alt="User Image" />
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo e(Auth::user()->name, false); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>" class="img-circle" alt="User Image" />
                    <p>
                      <small><?php echo e(Auth::user()->name, false); ?></small>
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo e(url('settings/page'), false); ?>" class="btn btn-default btn-flat"><?php echo e(trans('general.edit_my_page'), false); ?></a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo e(url('logout'), false); ?>" class="btn btn-default btn-flat"><?php echo e(trans('users.logout'), false); ?></a>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p class="text-overflow"><?php echo e(Auth::user()->name, false); ?></p>
              <small class="btn-block text-overflow"><a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> <?php echo e(trans('general.online'), false); ?></a></small>
            </div>
          </div>


          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">

            <li class="header"><?php echo e(trans('admin.main_menu'), false); ?></li>

            <!-- Links -->
            <li <?php if(Request::is('panel/admin')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin'), false); ?>"><i class="iconmoon icon-Speedometter myicon-right"></i> <span><?php echo e(trans('admin.dashboard'), false); ?></span></a>
            </li><!-- ./Links -->

           <!-- Links -->
            <li class="treeview <?php if( Request::is('panel/admin/settings') || Request::is('panel/admin/settings/limits') ): ?> active <?php endif; ?>">
            	<a href="<?php echo e(url('panel/admin/settings'), false); ?>"><i class="fa fa-cogs"></i> <span><?php echo e(trans('admin.general_settings'), false); ?></span> <i class="fa fa-angle-left pull-right"></i></a>

           		<ul class="treeview-menu">
                <li <?php if(Request::is('panel/admin/settings')): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('panel/admin/settings'), false); ?>"><i class="fas fa fa-angle-right"></i> <?php echo e(trans('admin.general'), false); ?></a></li>
                <li <?php if(Request::is('panel/admin/settings/limits')): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('panel/admin/settings/limits'), false); ?>"><i class="fas fa fa-angle-right"></i> <?php echo e(trans('admin.limits'), false); ?></a></li>
              </ul>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/post_inspection')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/post_inspection'), false); ?>"><i class="fa fa-info-circle"></i> <span>Post Inspection</span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/maintenance/mode')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/maintenance/mode'), false); ?>"><i class="fa fa-paint-roller"></i> <span><?php echo e(trans('admin.maintenance_mode'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/billing')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/billing'), false); ?>"><i class="fa fa-file-invoice"></i> <span><?php echo e(trans('general.billing_information'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/settings/email')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/settings/email'), false); ?>"><i class="fa fa-at"></i> <span><?php echo e(trans('admin.email_settings'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/storage')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/storage'), false); ?>"><i class="fa fa-database"></i> <span><?php echo e(trans('admin.storage'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/theme')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/theme'), false); ?>"><i class="fa fa-paint-brush"></i> <span><?php echo e(trans('admin.theme'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/custom-css-js')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/custom-css-js'), false); ?>"><i class="fa fa-code"></i> <span><?php echo e(trans('general.custom_css_js'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/posts')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/posts'), false); ?>"><i class="fa fa-user-edit"></i> <span><?php echo e(trans('general.posts'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li class="<?php if( Request::is('panel/admin/subscriptions') ): ?> active <?php endif; ?>">
            	<a href="<?php echo e(url('panel/admin/subscriptions'), false); ?>"><i class="fa fa-dollar-sign"></i> <span><?php echo e(trans('admin.subscriptions'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li class="<?php if( Request::is('panel/admin/transactions') ): ?> active <?php endif; ?>">
            	<a href="<?php echo e(url('panel/admin/transactions'), false); ?>"><i class="fa fa-file-invoice-dollar"></i> <span><?php echo e(trans('admin.transactions'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
           <li <?php if(Request::is('panel/admin/deposits')): ?> class="active" <?php endif; ?>>
             <a href="<?php echo e(url('panel/admin/deposits'), false); ?>"><i class="fa fa-money-bill-alt"></i>
               <span><?php echo e(trans('general.deposits'), false); ?>

                 <?php if(App\Models\Deposits::where('status','pending')->count() != 0): ?> <span class="label label-warning label-admin"><?php echo e(App\Models\Deposits::where('status','pending')->count(), false); ?></span>  <?php endif; ?>
               </span>
             </a>
           </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/members')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/members'), false); ?>"><i class="glyphicon glyphicon-user"></i> <span><?php echo e(trans('admin.members'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
           <li <?php if(Request::is('panel/admin/languages')): ?> class="active" <?php endif; ?>>
             <a href="<?php echo e(url('panel/admin/languages'), false); ?>"><i class="fa fa-language"></i> <span><?php echo e(trans('admin.languages'), false); ?></span></a>
           </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/categories')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/categories'), false); ?>"><i class="fa fa-list-ul"></i> <span><?php echo e(trans('admin.categories'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/reports')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/reports'), false); ?>"><i class="glyphicon glyphicon-ban-circle"></i> <span>
                <?php echo e(trans('admin.reports'), false); ?>

                <?php if( Reports::count() <> 0 ): ?> <span class="label label-danger label-admin"><?php echo e(Reports::count(), false); ?></span> <?php endif; ?>
              </span>
            </a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/withdrawals')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/withdrawals'), false); ?>"><i class="fa fa-university"></i> <span>
                <?php echo e(trans('general.withdrawals'), false); ?> <?php if(Withdrawals::where('status','pending')->count() != 0): ?> <span class="label label-warning label-admin"><?php echo e(Withdrawals::where('status','pending')->count(), false); ?></span>  <?php endif; ?>
              </span>
            </a>
            </li><!-- ./Links -->



            <!-- Links -->
            <li <?php if(Request::is('panel/admin/verification/members')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/verification/members'), false); ?>"><i class="far fa-check-circle myicon-right"></i>
                <span><?php echo e(trans('admin.verification_requests'), false); ?> <?php if(VerificationRequests::where('status','pending')->count() != 0): ?> <span class="label label-warning label-admin"><?php echo e(VerificationRequests::where('status','pending')->count(), false); ?></span>  <?php endif; ?>
                </span>
              </a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/pages')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/pages'), false); ?>"><i class="glyphicon glyphicon-file"></i> <span><?php echo e(trans('admin.pages'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/blog')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/blog'), false); ?>"><i class="fa fa-pencil-alt"></i> <span><?php echo e(trans('general.blog'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li class="treeview <?php if(Request::is('panel/admin/payments') || Request::is('panel/admin/payments/*')): ?> active <?php endif; ?>">
            	<a href="<?php echo e(url('panel/admin/payments'), false); ?>"><i class="fa fa-credit-card"></i> <span><?php echo e(trans('admin.payment_settings'), false); ?></span> <i class="fa fa-angle-left pull-right"></i></a>

           		<ul class="treeview-menu">
                <li <?php if(Request::is('panel/admin/payments')): ?> class="active" <?php endif; ?>><a href="<?php echo e(url('panel/admin/payments'), false); ?>"><i class="fas fa fa-angle-right"></i> <?php echo e(trans('admin.general'), false); ?></a></li>

                  <?php
                  foreach (PaymentGateways::all() as $key) {
                    ?>
                    <li <?php if(Request::is('panel/admin/payments/'.$key->id)): ?> class="active" <?php endif; ?>>
                      <a href="<?php echo e(url('panel/admin/payments/'.$key->id), false); ?>"><i class="fas fa fa-angle-right"></i> <?php echo e($key->type == 'bank' ? trans('general.bank_transfer') : $key->name, false); ?></a>
                    </li>
                <?php
                  }
                ?>
              </ul>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/profiles-social')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/profiles-social'), false); ?>"><i class="fa fa-share-alt"></i> <span><?php echo e(trans('admin.profiles_social'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/social-login')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/social-login'), false); ?>"><i class="fab fa-facebook myicon-right"></i> <span><?php echo e(trans('admin.social_login'), false); ?></span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/google')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/google'), false); ?>"><i class="fab fa-google myicon-right"></i> <span>Google</span></a>
            </li><!-- ./Links -->

            <!-- Links -->
            <li <?php if(Request::is('panel/admin/pwa')): ?> class="active" <?php endif; ?>>
            	<a href="<?php echo e(url('panel/admin/pwa'), false); ?>"><i class="fa fa-mobile-alt myicon-right"></i> <span>PWA</span></a>
            </li><!-- ./Links -->

          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <?php echo $__env->yieldContent('content'); ?>

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- Default to the left -->
       &copy; <strong><?php echo e($settings->title, false); ?></strong> - <?php echo date('Y'); ?>
      </footer>

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <script src="<?php echo e(asset('public/plugins/jQuery/jQuery-2.1.4.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/bootstrap/js/bootstrap.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/plugins/fastclick/fastclick.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/admin/js/app.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/plugins/sweetalert/sweetalert.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/plugins/iCheck/icheck.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/js/ckeditor/ckeditor.js'), false); ?>"></script>
    <script src="<?php echo e(asset('public/plugins/select2/select2.full.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/plugins/tagsinput/jquery.tagsinput.min.js'), false); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/plugins/colorpicker/bootstrap-colorpicker.min.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('public/admin/js/functions.js'), false); ?>?v=<?php echo e($settings->version, false); ?>" type="text/javascript"></script>

    <?php echo $__env->yieldContent('javascript'); ?>

    <?php if(session('success_update')): ?>
      <script type="text/javascript">
          swal({
            title: "<?php echo e(session('success_update'), false); ?>",
            type: "success",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        </script>
    	 <?php endif; ?>

		 <?php if(session('info_message_demo')): ?>
       <script type="text/javascript">
    		swal({
    			title: "<?php echo e(trans('general.error_oops'), false); ?>",
    			text: "Disabled on demo",
    			type: "info",
    			confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
    			});
          </script>
   		 <?php endif; ?>
  </body>
</html>
<?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/layout.blade.php ENDPATH**/ ?>