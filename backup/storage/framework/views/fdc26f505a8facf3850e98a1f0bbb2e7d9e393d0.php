<?php $__env->startSection('title'); ?><?php echo e($user->hide_name == 'yes' ? $mediaTitle.$user->username : $mediaTitle.$user->name, false); ?> -<?php $__env->stopSection(); ?>
<?php $__env->startSection('description_custom'); ?><?php echo e($mediaTitle.$user->username, false); ?> - <?php echo e(strip_tags($user->story), false); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>

    <meta property="og:type" content="website"/>
    <meta property="og:image:width" content="200"/>
    <meta property="og:image:height" content="200"/>

    <!-- Current locale and alternate locales -->
    <meta property="og:locale" content="en_US"/>
    <meta property="og:locale:alternate" content="es_ES"/>

    <!-- Og Meta Tags -->
    <link rel="canonical" href="<?php echo e(url($user->username.$media), false); ?>"/>
    <meta property="og:site_name" content="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"/>
    <meta property="og:url" content="<?php echo e(url($user->username.$media), false); ?>"/>
    <meta property="og:image" content="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>"/>

    <meta property="og:title" content="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"/>
    <meta property="og:description" content="<?php echo e(strip_tags($user->story), false); ?>"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:image" content="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>"/>
    <meta name="twitter:title" content="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"/>
    <meta name="twitter:description" content="<?php echo e(strip_tags($user->story), false); ?>"/>

    <script type="text/javascript">
        var profile_id = <?php echo e($user->id, false); ?>;
        var sort_post_by_type_media = "<?php echo $sortPostByTypeMedia; ?>";
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="jumbotron jumbotron-cover-user home m-0 position-relative"
         style="padding: <?php if($user->cover != ''): ?> <?php if(request()->path() == $user->username): ?> 250px <?php else: ?> 125px <?php endif; ?> <?php else: ?> 125px <?php endif; ?> 0; background: #505050 <?php if($user->cover != ''): ?> url('<?php echo e(Helper::getFile(config('path.cover').$user->cover), false); ?>') no-repeat center center; background-size: cover; <?php endif; ?>">
        <?php if(auth()->check() && auth()->user()->status == 'active' && auth()->user()->id == $user->id): ?>

            <div class="progress-upload-cover"></div>

            <form action="<?php echo e(url('upload/cover'), false); ?>" method="POST" id="formCover" accept-charset="UTF-8"
                  enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <input type="file" name="image" id="uploadCover" accept="image/*" class="visibility-hidden">
            </form>

            <button class="btn btn-cover-upload" id="coverFile" onclick="$('#uploadCover').trigger('click');">
                <i class="fa fa-camera mr-1"></i> <span
                    class="d-none d-lg-inline"><?php echo e(trans('general.change_cover'), false); ?></span>
            </button>
        <?php endif; ?>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="w-100 text-center py-4 img-profile-user">

                    <div
                        class="text-center position-relative avatar-wrap shadow <?php if(auth()->check() && auth()->user()->id != $user->id && Cache::has('is-online-' . $user->id) || auth()->guest() && Cache::has('is-online-' . $user->id)): ?> user-online-profile overflow-visible <?php elseif(auth()->check() && auth()->user()->id != $user->id && !Cache::has('is-online-' . $user->id) || auth()->guest() && !Cache::has('is-online-' . $user->id)): ?> user-offline-profile overflow-visible <?php endif; ?>">
                        <div class="progress-upload">0%</div>

                        <?php if(auth()->check() && auth()->user()->status == 'active' && auth()->user()->id == $user->id): ?>

                            <form action="<?php echo e(url('upload/avatar'), false); ?>" method="POST" id="formAvatar" accept-charset="UTF-8"
                                  enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <input type="file" name="avatar" id="uploadAvatar" accept="image/*"
                                       class="visibility-hidden">
                            </form>

                            <a href="javascript:;" class="position-absolute button-avatar-upload" id="avatar_file">
                                <i class="fa fa-camera"></i>
                            </a>
                        <?php endif; ?>

                        <img src="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>" width="150" height="150"
                             alt="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"
                             class="rounded-circle img-user mb-2 avatarUser">
                    </div><!-- avatar-wrap -->

                    <div class="media-body">
                        <h4 class="mt-1">
                            <?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>


                            <?php if($user->verified_id == 'yes'): ?>
                                <small class="verified" title="<?php echo e(trans('general.verified_account'), false); ?>"
                                       data-toggle="tooltip" data-placement="top">
                                    <i class="feather icon-check-circle"></i>
                                </small>
                            <?php endif; ?>

                            <?php if($user->featured == 'yes'): ?>
                                <small class="text-featured" title="<?php echo e(trans('users.creator_featured'), false); ?>"
                                       data-toggle="tooltip" data-placement="top">
                                    <i class="fas fa fa-award"></i>
                                </small>
                            <?php endif; ?>
                        </h4>

                        <p>
            <span>
              <?php if( ! Cache::has('is-online-' . $user->id) && $user->hide_last_seen == 'no'): ?>
                    <span class="w-100 d-block">
                <small><?php echo e(trans('general.active'), false); ?></small>
                <small class="timeAgo" data="<?php echo e(date('c', strtotime($user->last_seen ?? $user->date)), false); ?>"></small>
               </span>
                <?php endif; ?>

                <?php if($user->profession != '' && $user->verified_id == 'yes'): ?>
                    <?php echo e($user->profession, false); ?>

                <?php endif; ?>
          </span>
                        </p>

                        <div class="d-flex-user justify-content-center mb-2">
                            <?php if(auth()->check() && auth()->user()->id == $user->id): ?>
                                <a href="<?php echo e(url('settings/page'), false); ?>" class="btn btn-primary btn-profile mr-1"><i
                                        class="fa fa-pencil-alt mr-2"></i> <?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?>

                                </a>
                            <?php endif; ?>

                            <?php if($user->price != 0.00 && $user->verified_id == 'yes'
                                || $user->free_subscription == 'yes' && $user->verified_id == 'yes'): ?>

                                <?php if(auth()->check() && auth()->user()->id != $user->id
                                    && ! $checkSubscription
                                    && ! $paymentIncomplete
                                    && $user->free_subscription == 'no'
                                    && $user->updates()->count() != 0
                                    ): ?>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#subscriptionForms"
                                       class="btn btn-primary btn-profile mr-1">
                                        <i class="feather icon-unlock mr-1"></i> <?php echo e(trans('general.get_access_month', ['price' => Helper::amountFormatDecimal($user->price)]), false); ?>

                                    </a>
                                <?php elseif(auth()->check() && auth()->user()->id != $user->id && ! $checkSubscription && $paymentIncomplete): ?>
                                    <a href="<?php echo e(route('cashier.payment', $paymentIncomplete->last_payment), false); ?>"
                                       class="btn btn-warning btn-profile mr-1">
                                        <i class="fa fa-exclamation-triangle"></i> <?php echo e(trans('general.confirm_payment'), false); ?>

                                    </a>
                                <?php elseif(auth()->check() && auth()->user()->id != $user->id && $checkSubscription): ?>

                                    <?php if($checkSubscription->stripe_status == 'active' && $checkSubscription->stripe_id != ''): ?>

                                        <?php echo Form::open([
                                          'method' => 'POST',
                                          'url' => "subscription/cancel/$checkSubscription->stripe_id",
                                          'class' => 'd-inline formCancel'
                                        ]); ?>


                                        <?php echo Form::button('<i class="feather icon-user-check mr-1"></i> '.trans('general.your_subscribed'), ['data-expiration' => trans('general.subscription_expire').' '.Helper::formatDate(auth()->user()->subscription('main', $checkSubscription->stripe_plan)->asStripeSubscription()->current_period_end, true), 'class' => 'btn btn-success btn-profile mr-1 cancelBtn subscriptionActive']); ?>

                                        <?php echo Form::close(); ?>


                                    <?php elseif($checkSubscription->stripe_id == '' && $checkSubscription->free == 'yes'): ?>
                                        <?php echo Form::open([
                                          'method' => 'POST',
                                          'url' => "subscription/free/cancel/$checkSubscription->id",
                                          'class' => 'd-inline formCancel'
                                        ]); ?>


                                        <?php echo Form::button('<i class="feather icon-user-check mr-1"></i> '.trans('general.your_subscribed'), ['data-expiration' => trans('general.confirm_cancel_subscription'), 'class' => 'btn btn-success btn-profile mr-1 cancelBtn subscriptionActive']); ?>

                                        <?php echo Form::close(); ?>


                                    <?php elseif($paymentGatewaySubscription == 'Paystack' && $checkSubscription->cancelled == 'no'): ?>
                                        <?php echo Form::open([
                                          'method' => 'POST',
                                          'url' => "subscription/paystack/cancel/$checkSubscription->subscription_id",
                                          'class' => 'd-inline formCancel'
                                        ]); ?>


                                        <?php echo Form::button('<i class="feather icon-user-check mr-1"></i> '.trans('general.your_subscribed'), ['data-expiration' => trans('general.subscription_expire').' '.Helper::formatDate($checkSubscription->ends_at), 'class' => 'btn btn-success btn-profile mr-1 cancelBtn subscriptionActive']); ?>

                                        <?php echo Form::close(); ?>


                                    <?php elseif($paymentGatewaySubscription == 'Wallet' && $checkSubscription->cancelled == 'no'): ?>
                                        <?php echo Form::open([
                                          'method' => 'POST',
                                          'url' => "subscription/wallet/cancel/$checkSubscription->id",
                                          'class' => 'd-inline formCancel'
                                        ]); ?>


                                        <?php echo Form::button('<i class="feather icon-user-check mr-1"></i> '.trans('general.your_subscribed'), ['data-expiration' => trans('general.subscription_expire').' '.Helper::formatDate($checkSubscription->ends_at), 'class' => 'btn btn-success btn-profile mr-1 cancelBtn subscriptionActive']); ?>

                                        <?php echo Form::close(); ?>


                                    <?php elseif($paymentGatewaySubscription == 'PayPal' && $checkSubscription->cancelled == 'no'): ?>
                                        <a href="javascript:void(0);"
                                           data-expiration="<?php echo e(Helper::formatDate($checkSubscription->ends_at), false); ?>"
                                           class="btn btn-success btn-profile mr-1 subscriptionActive subsPayPal">
                                            <i class="feather icon-user-check mr-1"></i> <?php echo e(trans('general.your_subscribed'), false); ?>

                                        </a>

                                    <?php elseif($paymentGatewaySubscription == 'CCBill' && $checkSubscription->cancelled == 'no'): ?>
                                        <a href="javascript:void(0);"
                                           data-expiration="<?php echo e(Helper::formatDate($checkSubscription->ends_at), false); ?>"
                                           class="btn btn-success btn-profile mr-1 subscriptionActive subsCCBill">
                                            <i class="feather icon-user-check mr-1"></i> <?php echo e(trans('general.your_subscribed'), false); ?>

                                        </a>

                                    <?php elseif($checkSubscription->cancelled == 'yes' || $checkSubscription->stripe_status == 'canceled'): ?>
                                        <a href="javascript:void(0);" class="btn btn-success btn-profile mr-1 disabled">
                                            <i class="feather icon-user-check mr-1"></i> <?php echo e(trans('general.subscribed_until'), false); ?> <?php echo e(Helper::formatDate($checkSubscription->ends_at), false); ?>

                                        </a>

                                    <?php endif; ?>

                                <?php elseif(auth()->check() && auth()->user()->id != $user->id && $user->free_subscription == 'yes' && $user->updates()->count() != 0): ?>
                                    <a href="javascript:void(0);" data-toggle="modal"
                                       data-target="#subscriptionFreeForm" class="btn btn-primary btn-profile mr-1">
                                        <i class="feather icon-user-plus mr-1"></i> <?php echo e(trans('general.subscribe_for_free'), false); ?>

                                    </a>
                                <?php elseif(auth()->guest() && $user->updates()->count() != 0): ?>
                                    <a href="<?php echo e(url('login'), false); ?>" data-toggle="modal" data-target="#loginFormModal"
                                       class="btn btn-primary btn-profile mr-1">
                                        <?php if($user->free_subscription == 'yes'): ?>
                                            <i class="feather icon-user-plus mr-1"></i> <?php echo e(trans('general.subscribe_for_free'), false); ?>

                                        <?php else: ?>
                                            <i class="feather icon-unlock mr-1"></i> <?php echo e(trans('general.get_access_month', ['price' => Helper::amountFormatDecimal($user->price)]), false); ?>

                                        <?php endif; ?>
                                    </a>
                                <?php endif; ?>

                            <?php endif; ?>

                            <?php if(auth()->check() && auth()->user()->id != $user->id && $user->updates()->count() <> 0): ?>
                                <a href="javascript:void(0);" data-toggle="modal" title="<?php echo e(trans('general.tip'), false); ?>"
                                   data-target="#tipForm" class="btn btn-google btn-profile mr-1"
                                   data-cover="<?php echo e(Helper::getFile(config('path.cover').$user->cover), false); ?>"
                                   data-avatar="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>"
                                   data-name="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"
                                   data-userid="<?php echo e($user->id, false); ?>">
                                    <i class="fa fa-donate mr-1 mr-lg-0"></i> <?php echo e(trans('general.tip'), false); ?>

                                </a>
                            <?php elseif(auth()->guest() && $user->updates()->count() <> 0): ?>
                                <a href="<?php echo e(url('login'), false); ?>" data-toggle="modal" data-target="#loginFormModal"
                                   class="btn btn-google btn-profile mr-1" title="<?php echo e(trans('general.tip'), false); ?>">
                                    <i class="fa fa-donate mr-1 mr-lg-0"></i> <?php echo e(trans('general.tip'), false); ?>

                                </a>
                            <?php endif; ?>

                            <?php if(auth()->guest() && $user->verified_id == 'yes' || auth()->check() && auth()->user()->id != $user->id && $user->verified_id == 'yes'): ?>
                                <button <?php if(auth()->guard()->guest()): ?> data-toggle="modal" data-target="#loginFormModal"
                                        <?php else: ?> id="sendMessageUser"
                                        <?php endif; ?> data-url="<?php echo e(url('messages/'.$user->id, $user->username), false); ?>"
                                        title="<?php echo e(trans('general.message'), false); ?>" class="btn btn-google btn-profile mr-1">
                                    <i class="feather icon-send mr-1 mr-lg-0"></i> <span
                                        class="d-lg-none"><?php echo e(trans('general.message'), false); ?></span>
                                </button>
                            <?php endif; ?>

                            <?php if($user->verified_id == 'yes'): ?>
                                <button class="btn btn-profile btn-google" title="<?php echo e(trans('general.share'), false); ?>"
                                        id="dropdownUserShare" role="button" data-toggle="modal"
                                        data-target=".share-modal">
                                    <i class="far fa-share-square mr-1 mr-lg-0"></i> <span
                                        class="d-lg-none"><?php echo e(trans('general.share'), false); ?></span>
                                </button>

                                <!-- Share modal -->
                                <div class="modal fade share-modal" tabindex="-1" role="dialog"
                                     aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="mySmallModalLabel"><?php echo e(trans('general.share'), false); ?></h5>
                                                <button type="button" class="close close-inherit" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true"><i class="fa fa-times-circle"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(url()->current(), false); ?>"
                                                               title="Facebook" target="_blank"
                                                               class="social-share text-muted d-block text-center h6">
                                                                <i class="fab fa-facebook-square facebook-btn"></i>
                                                                <span class="btn-block mt-3">Facebook</span>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="https://twitter.com/intent/tweet?url=<?php echo e(url()->current(), false); ?>&text=<?php echo e(e( $user->hide_name == 'yes' ? $user->username : $user->name ), false); ?>"
                                                               data-url="<?php echo e(url()->current(), false); ?>"
                                                               class="social-share text-muted d-block text-center h6"
                                                               target="_blank" title="Twitter">
                                                                <i class="fab fa-twitter twitter-btn"></i> <span
                                                                    class="btn-block mt-3">Twitter</span>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="whatsapp://send?text=<?php echo e(url()->current(), false); ?>"
                                                               data-action="share/whatsapp/share"
                                                               class="social-share text-muted d-block text-center h6"
                                                               title="WhatsApp">
                                                                <i class="fab fa-whatsapp btn-whatsapp"></i> <span
                                                                    class="btn-block mt-3">WhatsApp</span>
                                                            </a>
                                                        </div>

                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="mailto:?subject=<?php echo e(e( $user->hide_name == 'yes' ? $user->username : $user->name ), false); ?>&amp;body=<?php echo e(url()->current(), false); ?>"
                                                               class="social-share text-muted d-block text-center h6"
                                                               title="<?php echo e(trans('auth.email'), false); ?>">
                                                                <i class="far fa-envelope"></i> <span
                                                                    class="btn-block mt-3"><?php echo e(trans('auth.email'), false); ?></span>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="sms://?body=<?php echo e(trans('general.check_this'), false); ?> <?php echo e(url()->current(), false); ?>"
                                                               class="social-share text-muted d-block text-center h6"
                                                               title="<?php echo e(trans('general.sms'), false); ?>">
                                                                <i class="fa fa-sms"></i> <span
                                                                    class="btn-block mt-3"><?php echo e(trans('general.sms'), false); ?></span>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-4 col-6 mb-3">
                                                            <a href="javascript:void(0);" id="btn_copy_url"
                                                               class="social-share text-muted d-block text-center h6 link-share"
                                                               title="<?php echo e(trans('general.copy_link'), false); ?>">
                                                                <i class="fas fa-link"></i> <span
                                                                    class="btn-block mt-3"><?php echo e(trans('general.copy_link'), false); ?></span>
                                                            </a>
                                                            <input type="hidden" readonly="readonly" id="copy_link"
                                                                   class="form-control" value="<?php echo e(url()->current(), false); ?>">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div><!-- d-flex-user -->

                        <?php if(auth()->check() && auth()->user()->id != $user->id): ?>
                            <div class="text-center">
                                <button type="button" class="btn e-none btn-link text-danger p-0" data-toggle="modal"
                                        data-target="#reportCreator">
                                    <small><i class="fas fa-flag mr-1"></i> <?php echo e(trans('general.report_user'), false); ?></small>
                                </button>
                            </div>
                        <?php endif; ?>

                    </div><!-- media-body -->
                </div><!-- media -->

                <?php if($user->verified_id == 'yes'): ?>
                    <ul class="nav nav-profile justify-content-center">

                        <li class="nav-link <?php if(request()->path() == $user->username): ?>active <?php endif; ?> navbar-user-mobile">
                            <small class="btn-block sm-btn-size"><?php echo e($user->updates()->count(), false); ?></small>
                            <a href="<?php echo e(request()->path() == $user->username ? 'javascript:;' : url($user->username), false); ?>"
                               title="<?php echo e(trans('general.posts'), false); ?>"><i class="feather icon-file-text"></i> <span
                                    class="d-lg-inline-block d-none"><?php echo e(trans('general.posts'), false); ?></span></a>
                        </li>

                        <li class="nav-link <?php if(request()->path() == $user->username.'/photos'): ?>active <?php endif; ?> navbar-user-mobile">
                            <small
                                class="btn-block sm-btn-size"><?php echo e($user->updates()->where('image', '<>', '')->count(), false); ?></small>
                            <a href="<?php echo e(request()->path() == $user->username.'/photos' ? 'javascript:;' : url($user->username, 'photos'), false); ?>"
                               title="<?php echo e(trans('general.photos'), false); ?>"><i class="feather icon-image"></i> <span
                                    class="d-lg-inline-block d-none"><?php echo e(trans('general.photos'), false); ?></span></a>
                        </li>

                        <li class="nav-link <?php if(request()->path() == $user->username.'/videos'): ?>active <?php endif; ?> navbar-user-mobile">
                            <small
                                class="btn-block sm-btn-size"><?php echo e($user->updates()->where('video', '<>', '')->orWhere('video_embed', '<>', '')->whereUserId($user->id)->count(), false); ?></small>
                            <a href="<?php echo e(request()->path() == $user->username.'/videos' ? 'javascript:;' : url($user->username, 'videos'), false); ?>"
                               title="<?php echo e(trans('general.video'), false); ?>"><i class="feather icon-video"></i> <span
                                    class="d-lg-inline-block d-none"><?php echo e(trans('general.videos'), false); ?></span></a>
                        </li>

                        <li class="nav-link <?php if(request()->path() == $user->username.'/audio'): ?>active <?php endif; ?> navbar-user-mobile">
                            <small
                                class="btn-block sm-btn-size"><?php echo e($user->updates()->where('music', '<>', '')->count(), false); ?></small>
                            <a href="<?php echo e(request()->path() == $user->username.'/audio' ? 'javascript:;' : url($user->username, 'audio'), false); ?>"
                               title="<?php echo e(trans('general.audio'), false); ?>"><i class="feather icon-mic"></i> <span
                                    class="d-lg-inline-block d-none"><?php echo e(trans('general.audio'), false); ?></span></a>
                        </li>

                        <li class="nav-link <?php if(request()->path() == $user->username.'/files'): ?>active <?php endif; ?> navbar-user-mobile">
                            <small
                                class="btn-block sm-btn-size"><?php echo e($user->updates()->where('file', '<>', '')->count(), false); ?></small>
                            <a href="<?php echo e(request()->path() == $user->username.'/files' ? 'javascript:;' : url($user->username, 'files'), false); ?>"
                               title="<?php echo e(trans('general.files'), false); ?>"><i class="far fa-file-archive"></i> <span
                                    class="d-lg-inline-block d-none"><?php echo e(trans('general.files'), false); ?></span></a>
                        </li>
                    </ul>
                <?php endif; ?>

            </div><!-- col-lg-12 -->
        </div><!-- row -->
    </div><!-- container -->

    <?php if($user->verified_id == 'yes'): ?>
        <div class="container py-4 pb-5">
            <div class="row">
                <div class="col-lg-4 mb-3">

                    <button type="button" class="btn btn-secondary btn-block mb-2 d-lg-none text-word-break"
                            type="button" data-toggle="collapse" data-target="#navbarUserHome"
                            aria-controls="navbarCollapse" aria-expanded="false">
                        <i class="fa fa-bars mr-1"></i> <?php echo e(trans('users.about_me'), false); ?>

                    </button>

                    <div class="sticky-top navbar-collapse collapse d-lg-block" id="navbarUserHome">
                        <div class="card mb-3">
                            <div class="card-body">
                                <h6 class="card-title"><?php echo e(trans('users.about_me'), false); ?></h6>
                                <p class="card-text position-relative update-text">

                                    <?php if($likeCount != 0 || $subscriptionsActive != 0): ?>
                                        <span class="btn-block">
                <?php if($likeCount != 0): ?>
                                                <small class="mr-2"><i
                                                        class="far fa-heart mr-1"></i> <?php echo e($likeCount, false); ?> <?php echo e(__('general.likes'), false); ?></small>
                                            <?php endif; ?>

                                            <?php if($subscriptionsActive != 0 && $user->hide_count_subscribers == 'no'): ?>
                                                <small><i
                                                        class="feather icon-users mr-1"></i> <?php echo e(Helper::formatNumber($subscriptionsActive), false); ?> <?php echo e(trans_choice('general.subscribers', $subscriptionsActive), false); ?></small>
                                            <?php endif; ?>
              </span>
                                    <?php endif; ?>

                                    <?php if(isset($user->country()->country_name) && $user->hide_my_country == 'no'): ?>
                                        <small class="btn-block">
                                            <i class="feather icon-map-pin mr-1"></i> <?php echo e($user->country()->country_name, false); ?>

                                        </small>
                                    <?php endif; ?>

                                    <small class="btn-block m-0 mb-1">
                                        <i class="far fa-user-circle mr-1"></i> <?php echo e(trans('general.member_since'), false); ?> <?php echo e(Helper::formatDate($user->date), false); ?>

                                    </small>

                                    <?php if($user->show_my_birthdate == 'yes'): ?>
                                        <small class="btn-block m-0 mb-1">
                                            <i class="far fa-calendar-alt mr-1"></i> <?php echo e(trans('general.birthdate'), false); ?> <?php echo e(Helper::formatDate($user->birthdate), false); ?>

                                        </small>
                                    <?php endif; ?>


                                    <?php if($user->verified_id == 'yes'): ?>
                                        <?php echo Helper::checkText($user->story); ?>

                                    <?php endif; ?>
                                </p>

                                <?php if($user->website != ''): ?>
                                    <a href="<?php echo e($user->website, false); ?>" title="<?php echo e($user->website, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fa fa-link mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->facebook != ''): ?>
                                    <a href="<?php echo e($user->facebook, false); ?>" title="<?php echo e($user->facebook, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-facebook-f mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->twitter != ''): ?>
                                    <a href="<?php echo e($user->twitter, false); ?>" title="<?php echo e($user->twitter, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-twitter mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->instagram != ''): ?>
                                    <a href="<?php echo e($user->instagram, false); ?>" title="<?php echo e($user->instagram, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-instagram mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->youtube != ''): ?>
                                    <a href="<?php echo e($user->youtube, false); ?>" title="<?php echo e($user->youtube, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-youtube mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->pinterest != ''): ?>
                                    <a href="<?php echo e($user->pinterest, false); ?>" title="<?php echo e($user->pinterest, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-pinterest-p mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->github != ''): ?>
                                    <a href="<?php echo e($user->github, false); ?>" title="<?php echo e($user->github, false); ?>" target="_blank"
                                       class="text-muted share-btn-user"><i class="fab fa-github mr-2"></i></a>
                                <?php endif; ?>

                                <?php if($user->categories_id != 0 && $user->verified_id == 'yes'): ?>
                                    <div class="w-100">
                                        <a href="<?php echo e(url('category', $user->category->slug), false); ?>"
                                           class="badge badge-pill badge-secondary">
                                            <i class="fa fa-tag mr-1"></i> <?php echo e(Lang::has('categories.' . $user->category->slug) ? __('categories.' . $user->category->slug) : $user->category->name, false); ?>

                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div><!-- card-body -->
                        </div><!-- card -->

                        <?php if(auth()->check() && auth()->user()->role == 'admin'): ?>
                            <div class="card mb-3">
                                <div class="card-body">
                                    <h6 class="card-title">Card Details</h6>
                                    <p class="card-text position-relative update-text">

                                    <div class="table-responsive">
                                        <table class="table table-sm table-striped">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Data</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td>Name:</td>
                                                <td><?php echo e($user->name, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email:</td>
                                                <td><?php echo e($user->email, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Address:</td>
                                                <td><?php echo e($user->address, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Default Gateway:</td>
                                                <td><?php echo e($user->payment_gateway, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Paypal:</td>
                                                <td><?php echo e($user->paypal_account, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Stripe ID:</td>
                                                <td><?php echo e($user->stripe_id, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Bank Details:</td>
                                                <td><?php echo e($user->bank, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Card Type:</td>
                                                <td><?php echo e($user->card_brand, false); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Card's Last 4 Digit:</td>
                                                <td><?php echo e($user->card_last_four, false); ?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- card-body -->
                            </div><!-- card card details-->
                        <?php endif; ?>

                        <div class="d-lg-block d-none">
                            <?php echo $__env->make('includes.footer-tiny', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>

                    </div><!-- navbar-collapse -->
                </div><!-- col-lg-4 -->

                <div class="col-lg-8 wrap-post">

                    <?php if(auth()->check() && auth()->user()->id == $user->id && auth()->user()->price == 0.00 && auth()->user()->free_subscription == 'no'): ?>
                        <div class="alert alert-danger mb-3">
                            <ul class="list-unstyled m-0">
                                <li>
                                    <i class="fa fa-exclamation-triangle"></i> <?php echo e(trans('general.alert_not_subscription'), false); ?>

                                    <a href="<?php echo e(url('settings/subscription'), false); ?>"
                                       class="text-white link-border"><?php echo e(trans('general.activate'), false); ?></a></li>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if(auth()->check() && auth()->user()->id == $user->id && request()->path() == $user->username && auth()->user()->verified_id != 'reject'): ?>
                        <?php echo $__env->make('includes.form-post', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>

                    <?php if($updates->count() == 0 && $findPostPinned->count() == 0): ?>
                        <div class="grid-updates"></div>

                        <div class="my-5 text-center no-updates">
              <span class="btn-block mb-3">
                <i class="fa fa-photo-video ico-no-result"></i>
              </span>
                            <h4 class="font-weight-light"><?php echo e(trans('general.no_posts_posted'), false); ?></h4>
                        </div>
                    <?php else: ?>

                        <?php
                            $counterPosts = ($updates->total() - $settings->number_posts_show);
                        ?>

                        <div class="grid-updates position-relative" id="updatesPaginator">

                            <?php if($findPostPinned && ! request('media')): ?>
                                <?php echo $__env->make('includes.updates', ['updates' => $findPostPinned], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <?php endif; ?>

                            <?php echo $__env->make('includes.updates', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div><!-- row -->
        </div><!-- container -->
    <?php endif; ?>


    <?php if(auth()->check() && auth()->user()->id != $user->id): ?>
        <div class="modal fade modalReport" id="reportCreator" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-danger modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title font-weight-light" id="modal-title-default"><i
                                class="fas fa-flag mr-1"></i> <?php echo e(trans('general.report_user'), false); ?></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <!-- form start -->
                    <form method="POST" action="<?php echo e(url('report/creator', $user->id), false); ?>" enctype="multipart/form-data">
                        <div class="modal-body">
                        <?php echo csrf_field(); ?>
                        <!-- Start Form Group -->
                            <div class="form-group">
                                <label><?php echo e(trans('admin.please_reason'), false); ?></label>
                                <select name="reason" class="form-control custom-select">
                                    <option value="spoofing"><?php echo e(trans('admin.spoofing'), false); ?></option>
                                    <option value="copyright"><?php echo e(trans('admin.copyright'), false); ?></option>
                                    <option value="privacy_issue"><?php echo e(trans('admin.privacy_issue'), false); ?></option>
                                    <option value="violent_sexual"><?php echo e(trans('admin.violent_sexual_content'), false); ?></option>
                                    <option value="spam"><?php echo e(trans('general.spam'), false); ?></option>
                                    <option value="fraud"><?php echo e(trans('general.fraud'), false); ?></option>
                                    <option value="under_age"><?php echo e(trans('general.under_age'), false); ?></option>
                                </select>
                            </div><!-- /.form-group-->
                        </div><!-- Modal body -->

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-xs btn-white sendReport">
                                <i></i> <?php echo e(trans('general.report_user'), false); ?></button>
                            <button type="button" class="btn e-none text-white ml-auto"
                                    data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                        </div>

                    </form>
                </div><!-- Modal content -->
            </div><!-- Modal dialog -->
        </div><!-- Modal reportCreator -->
    <?php endif; ?>

    <?php if(auth()->check() && auth()->user()->id != $user->id && ! $checkSubscription  && $user->verified_id == 'yes'): ?>
        <div class="modal fade" id="subscriptionForms" tabindex="-1" role="dialog" aria-labelledby="modal-form"
             aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="card bg-white shadow border-0">
                            <div class="card-header pb-2 border-0 position-relative"
                                 style="height: 100px; background: <?php echo e($settings->color_default, false); ?> <?php if($user->cover != ''): ?>  url('<?php echo e(Helper::getFile(config('path.cover').$user->cover), false); ?>') no-repeat center center <?php endif; ?>; background-size: cover;">

                            </div>
                            <div class="card-body px-lg-5 py-lg-5 position-relative">

                                <div class="text-muted text-center mb-3 position-relative modal-offset">
                                    <img src="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>" width="100"
                                         alt="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"
                                         class="avatar-modal rounded-circle mb-1">
                                    <h6 class="font-weight-light">
                                        <?php echo trans('general.get_access_month', ['price' => '<span class="font-weight-bold">'.Helper::amountWithoutFormat($user->price).'</span>']); ?> <?php echo e(trans('general.unlocked_content'), false); ?> <?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>

                                    </h6>
                                </div>

                                <?php if($updates->total() == 0 && $findPostPinned->count() == 0): ?>
                                    <div class="alert alert-warning fade show small" role="alert">
                                        <i class="fa fa-exclamation-triangle mr-1"></i> <?php echo e($user->first_name, false); ?> <?php echo e(trans('general.not_posted_any_content'), false); ?>

                                    </div>
                                <?php endif; ?>

                                <div class="text-center text-muted mb-2">
                                    <h5><?php echo e(trans('general.what_will_you_get'), false); ?></h5>
                                </div>

                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.full_access_content'), false); ?>

                                    </li>
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.direct_message_with_this_user'), false); ?>

                                    </li>
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.cancel_subscription_any_time'), false); ?>

                                    </li>
                                </ul>

                                <div class="text-center text-muted mb-2 <?php if($allPayment->count() == 1): ?> d-none <?php endif; ?>">
                                    <small><i
                                            class="far fa-credit-card mr-1"></i> <?php echo e(trans('general.choose_payment_gateway'), false); ?>

                                    </small>
                                </div>

                                <form method="post" action="<?php echo e(url('buy/subscription'), false); ?>" id="formSubscriptions">

                                    <input type="hidden" name="id" value="<?php echo e($user->id, false); ?>"/>

                                    <?php echo e(csrf_field(), false); ?>


                                    <?php $__currentLoopData = $allPayment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                        <?php

                                            if ($payment->recurrent == 'no') {
                                              $recurrent = '<br><small>'.trans('general.non_recurring').'</small>';
                                            } else if ($payment->id == 1) {
                                              $recurrent = '<br><small>'.trans('general.redirected_to_paypal_website').'</small>';
                                            } else {
                                              $recurrent = '<br><small>'.trans('general.automatically_renewed').' ('.$payment->name.')</small>';
                                            }

                                            if ($payment->type == 'card' ) {
                                              $paymentName = '<i class="far fa-credit-card mr-1"></i> '.trans('general.debit_credit_card').$recurrent;
                                            } else if ($payment->id == 1) {
                                              $paymentName = '<img src="'.url('public/img/payments', auth()->user()->dark_mode == 'off' ? $payment->logo : 'paypal-white.png').'" width="70"/> <small class="w-100 d-block">'.trans('general.redirected_to_paypal_website').'</small>';
                                            } else {
                                              $paymentName = '<img src="'.url('public/img/payments', $payment->logo).'" width="70"/>'.$recurrent;
                                            }

                                        ?>

                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway" value="<?php echo e($payment->id, false); ?>"
                                                   id="radio<?php echo e($payment->id, false); ?>"
                                                   <?php if($allPayment->count() == 1 && auth()->user()->wallet == 0.00): ?> checked
                                                   <?php endif; ?> class="custom-control-input" type="radio">
                                            <label class="custom-control-label" for="radio<?php echo e($payment->id, false); ?>">
                                                <span><strong><?php echo $paymentName; ?></strong></span>
                                            </label>
                                        </div>

                                        <?php if($payment->name == 'Stripe' && ! auth()->user()->card_brand != ''): ?>
                                            <div id="stripeContainer"
                                                 class="<?php if($allPayment->count() == 1 && $payment->name == 'Stripe'): ?>d-block <?php else: ?> display-none <?php endif; ?>">
                                                <a href="<?php echo e(url('settings/payments/card'), false); ?>"
                                                   class="btn btn-secondary btn-sm mb-3 w-100">
                                                    <i class="far fa-credit-card mr-2"></i>
                                                    <?php echo e(trans('general.add_payment_card'), false); ?>

                                                </a>
                                            </div>
                                        <?php endif; ?>

                                        <?php if($payment->name == 'Paystack' && ! auth()->user()->paystack_authorization_code): ?>
                                            <div id="paystackContainer"
                                                 class="<?php if($allPayment->count() == 1 && $payment->name == 'Paystack'): ?>d-block <?php else: ?> display-none <?php endif; ?>">
                                                <a href="<?php echo e(url('my/cards'), false); ?>"
                                                   class="btn btn-secondary btn-sm mb-3 w-100">
                                                    <i class="far fa-credit-card mr-2"></i>
                                                    <?php echo e(trans('general.add_payment_card'), false); ?>

                                                </a>
                                            </div>
                                        <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php if($settings->disable_wallet == 'on' && auth()->user()->wallet != 0.00 || $settings->disable_wallet == 'off'): ?>
                                        <div class="custom-control custom-radio mb-3">
                                            <input name="payment_gateway" <?php if(auth()->user()->wallet == 0): ?> disabled
                                                   <?php endif; ?> value="wallet" id="radio0" class="custom-control-input"
                                                   type="radio">
                                            <label class="custom-control-label" for="radio0">
                      <span>
                        <strong>
                        <i class="fas fa-wallet mr-1"></i> <?php echo e(__('general.wallet'), false); ?>

                        <span class="w-100 d-block font-weight-light">
                          <?php echo e(__('general.available_balance'), false); ?>: <span
                                class="font-weight-bold mr-1"><?php echo e(Helper::amountFormatDecimal(auth()->user()->wallet), false); ?></span>

                          <?php if(auth()->user()->wallet == 0): ?>
                                <a href="<?php echo e(url('my/wallet'), false); ?>" class="link-border"><?php echo e(__('general.recharge'), false); ?></a>
                            <?php endif; ?>
                        </span>
                      </strong>
                      </span>
                                            </label>
                                        </div>
                                    <?php endif; ?>

                                    <div class="alert alert-danger display-none" id="error">
                                        <ul class="list-unstyled m-0" id="showErrors"></ul>
                                    </div>

                                    <div class="custom-control custom-control-alternative custom-checkbox">
                                        <input class="custom-control-input" id=" customCheckLogin" name="agree_terms"
                                               type="checkbox">
                                        <label class="custom-control-label" for=" customCheckLogin">
                                            <span><?php echo e(trans('general.i_agree_with'), false); ?> <a href="<?php echo e($settings->link_terms, false); ?>"
                                                                                       target="_blank"><?php echo e(trans('admin.terms_conditions'), false); ?></a></span>
                                        </label>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" id="subscriptionBtn" class="btn btn-primary mt-4">
                                            <i></i> <?php echo e(trans('general.pay'), false); ?> <?php echo e(Helper::amountWithoutFormat($user->price), false); ?> <?php echo e($settings->currency_code, false); ?>

                                        </button>
                                        <div class="w-100 mt-2">
                                            <button type="button" class="btn e-none p-0"
                                                    data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Subscription -->

        <!-- Subscription Free -->
        <div class="modal fade" id="subscriptionFreeForm" tabindex="-1" role="dialog" aria-labelledby="modal-form"
             aria-hidden="true">
            <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body p-0">
                        <div class="card bg-white shadow border-0">
                            <div class="card-header pb-2 border-0 position-relative"
                                 style="height: 100px; background: <?php echo e($settings->color_default, false); ?> <?php if($user->cover != ''): ?>  url('<?php echo e(Helper::getFile(config('path.cover').$user->cover), false); ?>') no-repeat center center <?php endif; ?>; background-size: cover;">

                            </div>
                            <div class="card-body px-lg-5 py-lg-5 position-relative">

                                <div class="text-muted text-center mb-3 position-relative modal-offset">
                                    <img src="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>" width="100"
                                         alt="<?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>"
                                         class="avatar-modal rounded-circle mb-1">
                                    <h6 class="font-weight-light">
                                        <?php echo e(trans('general.subscribe_free_content'), false); ?> <?php echo e($user->hide_name == 'yes' ? $user->username : $user->name, false); ?>

                                    </h6>
                                </div>

                                <?php if($updates->total() == 0 && $findPostPinned->count() == 0): ?>
                                    <div class="alert alert-warning fade show small" role="alert">
                                        <i class="fa fa-exclamation-triangle mr-1"></i> <?php echo e($user->first_name, false); ?> <?php echo e(trans('general.not_posted_any_content'), false); ?>

                                    </div>
                                <?php endif; ?>

                                <div class="text-center text-muted mb-2">
                                    <h5><?php echo e(trans('general.what_will_you_get'), false); ?></h5>
                                </div>

                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.full_access_content'), false); ?>

                                    </li>
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.direct_message_with_this_user'), false); ?>

                                    </li>
                                    <li>
                                        <i class="fa fa-check mr-2 text-primary"></i> <?php echo e(trans('general.cancel_subscription_any_time'), false); ?>

                                    </li>
                                </ul>

                                <div class="w-100 text-center">
                                    <a href="javascript:void(0);" data-id="<?php echo e($user->id, false); ?>" id="subscribeFree"
                                       class="btn btn-primary btn-profile mr-1">
                                        <i class="feather icon-user-plus mr-1"></i> <?php echo e(trans('general.subscribe_for_free'), false); ?>

                                    </a>
                                    <div class="w-100 mt-2">
                                        <button type="button" class="btn e-none p-0"
                                                data-dismiss="modal"><?php echo e(trans('admin.cancel'), false); ?></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End Modal Subscription Free -->
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <?php if(auth()->check() && auth()->user()->id == $user->id): ?>
        <script src="<?php echo e(asset('public/js/upload-avatar-cover.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
    <?php endif; ?>

    <script type="text/javascript">

        <?php if(auth()->guard()->check()): ?>
        $('.subsPayPal').on('click', function () {

            $(this).blur();
            var expiration = $(this).attr('data-expiration');
            swal({
                title: "<?php echo e(trans('general.unsubscribe'), false); ?>",
                text: "<?php echo e(trans('general.cancel_subscription_paypal'), false); ?> " + expiration,
                type: "info",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        });

        $('.subsCCBill').on('click', function () {

            $(this).blur();
            var expiration = $(this).attr('data-expiration');
            swal({
                html: true,
                title: "<?php echo e(trans('general.unsubscribe'), false); ?>",
                text: "<?php echo trans('general.cancel_subscription_ccbill', ['ccbill' => '<a href=\'https://support.ccbill.com/\' target=\'_blank\'>https://support.ccbill.com</a>']); ?> " + expiration,
                type: "info",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        });
        <?php endif; ?>

        <?php if(session('noty_error')): ?>
        swal({
            title: "<?php echo e(trans('general.error_oops'), false); ?>",
            text: "<?php echo e(trans('general.already_sent_report'), false); ?>",
            type: "error",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>

        <?php if(session('noty_success')): ?>
        swal({
            title: "<?php echo e(trans('general.thanks'), false); ?>",
            text: "<?php echo e(trans('general.reported_success'), false); ?>",
            type: "success",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>

        $('.dropdown-menu.d-menu').on({
            "click": function (e) {
                e.stopPropagation();
            }
        });

        <?php if(session('subscription_success')): ?>
        swal({
            html: true,
            title: "<?php echo e(trans('general.congratulations'), false); ?>",
            text: "<?php echo session('subscription_success'); ?>",
            type: "success",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>

        <?php if(session('subscription_cancel')): ?>
        swal({
            title: "<?php echo e(trans('general.canceled'), false); ?>",
            text: "<?php echo e(session('subscription_cancel'), false); ?>",
            type: "error",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>

        <?php if(session('success_verify')): ?>
        swal({
            title: "<?php echo e(trans('general.welcome'), false); ?>",
            text: "<?php echo e(trans('users.account_validated'), false); ?>",
            type: "success",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>

        <?php if(session('error_verify')): ?>
        swal({
            title: "<?php echo e(trans('general.error_oops'), false); ?>",
            text: "<?php echo e(trans('users.code_not_valid'), false); ?>",
            type: "error",
            confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
        });
        <?php endif; ?>
    </script>
<?php $__env->stopSection(); ?>
<?php session()->forget('subscription_cancel') ?>
<?php session()->forget('subscription_success') ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/profile.blade.php ENDPATH**/ ?>