

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('admin.verification_requests'), false); ?> (<?php echo e($data->count(), false); ?>)
          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

		    <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">

                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	<?php if($data->count() !=  0): ?>
                   <tr>
                      <th class="active">ID</th>
                      <th class="active"><?php echo e(trans('admin.user'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.address'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.city'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.country'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.zip'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.image'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.form_w9'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.date'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.actions'), false); ?></th>
                    </tr>

                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $verify): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($verify->id, false); ?></td>
                      <td>
                        <?php if( ! isset($verify->user()->username)): ?>
                          <?php echo e(trans('general.no_available'), false); ?>

                        <?php else: ?>
                        <a href="<?php echo e(url($verify->user()->username), false); ?>" target="_blank"><?php echo e($verify->user()->name, false); ?>

                          <i class="fa fa-external-link-square-alt"></i>
                        </a>
                      <?php endif; ?>
                      </td>
                      <td><?php echo e($verify->address, false); ?></td>
                      <td><?php echo e($verify->city, false); ?></td>
                      <td><?php echo e($verify->user()->country()->country_name, false); ?></td>
                      <td><?php echo e($verify->zip, false); ?></td>
                      <td><a href="<?php echo e(Helper::getFile(config('path.verification').$verify->image), false); ?>" target="_blank"><?php echo e(trans('admin.see_document_id'), false); ?> <i class="fa fa-external-link-square-alt"></i></a></td>
                      <td>
                        <?php if($verify->form_w9): ?>
                          <a href="<?php echo e(Helper::getFile(config('path.verification').$verify->form_w9), false); ?>" target="_blank">
                            <?php echo e(trans('general.form_w9'), false); ?> <i class="fa fa-external-link-square-alt"></i>
                          </a>
                        <?php else: ?>
                          <em><?php echo e(__('general.not_applicable'), false); ?></em>
                        <?php endif; ?>

                      </td>
                      <td><?php echo e(Helper::formatDate($verify->created_at), false); ?></td>
                    <td>

                  <?php if($verify->status == 'pending'): ?>

                  <?php if(isset($verify->user()->username)): ?>
                      <?php echo Form::open([
                      'method' => 'POST',
                      'url' => url('panel/admin/verification/members/approve', $verify->id).'/'.$verify->user_id,
                      'class' => 'displayInline'
                    ]); ?>

                   <?php echo Form::submit(trans('admin.approve'), ['class' => 'btn btn-success btn-sm padding-btn actionApprove']); ?>

                 <?php endif; ?>

                     <?php echo Form::close(); ?>


                        <?php echo Form::open([
      			            'method' => 'POST',
      			            'url' => url('panel/admin/verification/members/delete', $verify->id).'/'.$verify->user_id,
      			            'class' => 'displayInline'
				              ]); ?>

	                   <?php echo Form::submit(trans('admin.reject'), ['class' => 'btn btn-danger btn-sm padding-btn actionDeleteVerification']); ?>


	        	           <?php echo Form::close(); ?>


                     <?php else: ?>
                       <span class="label label-success"><?php echo e(trans('admin.approved'), false); ?></span>
                     <?php endif; ?>
                     </td>

                    </tr><!-- /.TR -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    	<h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>
                    <?php endif; ?>

                  </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/verification.blade.php ENDPATH**/ ?>