<header>
    <nav
        class="navbar navbar-expand-lg navbar-inverse fixed-top p-nav <?php if(auth()->guest() && request()->path() == '/'): ?> scroll <?php else: ?> p-3 <?php if(request()->is('messages/*')): ?> d-none d-lg-block shadow-sm <?php else: ?> shadow-custom <?php endif; ?> <?php echo e(auth()->check() && auth()->user()->dark_mode == 'on' ? 'bg-white' : 'navbar_background_color', false); ?> link-scroll <?php endif; ?>">
        <div class="container-fluid d-flex">
            <a class="navbar-brand margin-auto" href="<?php echo e(url('/'), false); ?>">
                <?php if(auth()->check() && auth()->user()->dark_mode == 'on' ): ?>
                    <img src="<?php echo e(url('public/img', $settings->logo), false); ?>" data-logo="<?php echo e($settings->logo, false); ?>"
                         data-logo-2="<?php echo e($settings->logo_2, false); ?>" alt="<?php echo e($settings->title, false); ?>"
                         class="logo align-bottom max-w-100"/>
                <?php else: ?>
                    <img
                        src="<?php echo e(url('public/img', auth()->guest() && request()->path() == '/' ? $settings->logo : $settings->logo_2), false); ?>"
                        data-logo="<?php echo e($settings->logo, false); ?>" data-logo-2="<?php echo e($settings->logo_2, false); ?>" alt="<?php echo e($settings->title, false); ?>"
                        class="logo align-bottom max-w-100"/>
                <?php endif; ?>
            </a>

            <?php if(auth()->guard()->guest()): ?>
                <button class="navbar-toggler <?php if(auth()->guest() && request()->path() == '/'): ?> text-white <?php endif; ?>"
                        type="button" data-toggle="collapse" data-target="#navbarCollapse"
                        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
            <?php endif; ?>

            <div class="collapse navbar-collapse navbar-mobile" id="navbarCollapse">

                <div class="d-lg-none text-right pr-2 mb-2">
                    <button type="button" class="navbar-toggler close-menu-mobile" data-toggle="collapse"
                            data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false">
                        <span></span>
                        <span></span>
                    </button>
                </div>

                <?php if(auth()->guest() && $settings->who_can_see_content == 'all' || auth()->check()): ?>
                    <ul class="navbar-nav mr-auto">
                        <form class="form-inline my-lg-0 position-relative" method="get" action="<?php echo e(url('creators'), false); ?>">
                            <input id="searchCreatorNavbar"
                                   class="form-control input-search <?php if(auth()->guest() && request()->path() == '/'): ?> border-0 <?php endif; ?>"
                                   type="text" required name="q" autocomplete="off" minlength="3"
                                   placeholder="<?php echo e(trans('general.find_user'), false); ?>" aria-label="Search">
                            <button class="btn btn-outline-success my-sm-0 button-search e-none" type="submit"><i
                                    class="fa fa-search"></i></button>

                            <div class="dropdown-menu dd-menu-user position-absolute" style="width: 95%; top: 48px;"
                                 id="dropdownCreators">

                                <button type="button" class="d-none" id="triggerBtn" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false"></button>

                                <div class="w-100 text-center display-none py-2" id="spinnerSearch">
                                    <span class="spinner-border spinner-border-sm align-middle text-primary"></span>
                                </div>

                                <div id="containerCreators"></div>

                                <div id="viewAll" class="display-none">
                                    <a class="dropdown-item border-top py-2 text-center"
                                       href="#"><?php echo e(__('general.view_all'), false); ?></a>
                                </div>
                            </div><!-- dropdown-menu -->
                        </form>

                        <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(url('creators'), false); ?>"><?php echo e(trans('general.explore'), false); ?></a>
                            </li>
                        <?php endif; ?>

                    </ul>
                <?php endif; ?>

                <ul class="navbar-nav ml-auto">
                    <?php if(auth()->guard()->guest()): ?>
                        <li class="nav-item mr-1">
                            <a <?php if(auth()->guest() && request()->route()->named('profile')): ?> data-toggle="modal"
                               data-target="#loginFormModal"
                               <?php endif; ?> class="nav-link login-btn <?php if($settings->registration_active == '0'): ?>  btn btn-main btn-primary pr-3 pl-3 <?php endif; ?>"
                               href="<?php echo e($settings->home_style == 0 ? url('login') : url('/'), false); ?>"><?php echo e(trans('auth.login'), false); ?></a>
                        </li>

                        <?php if($settings->registration_active == '1'): ?>
                            <li class="nav-item">
                                <a <?php if(auth()->guest() && request()->route()->named('profile')): ?> data-toggle="modal"
                                   data-target="#loginFormModal"
                                   <?php endif; ?> class="nav-link btn btn-main btn-primary pr-3 pl-3"
                                   href="<?php echo e($settings->home_style == 0 ? url('signup') : url('/'), false); ?>"><?php echo e(trans('general.getting_started'), false); ?>

                                    <small class="pl-1"><i class="fa fa-long-arrow-alt-right"></i></small></a>
                            </li>
                        <?php endif; ?>

                    <?php else: ?>

                    <!-- ============ Menu Mobile -->

                        <?php if(auth()->user()->role == 'admin'): ?>
                            <li class="nav-item dropdown d-lg-none mt-2">
                                <a href="<?php echo e(url('panel/admin'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="bi bi-speedometer2 mr-2"></i>
                                        <span class="d-lg-none"><?php echo e(trans('admin.admin'), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item dropdown d-lg-none <?php if(auth()->user()->role != 'admin'): ?> mt-2 <?php endif; ?>">
                            <a href="<?php echo e(url(auth()->user()->username), false); ?>"
                               class="nav-link px-2 link-menu-mobile py-1 url-user">
                                <div>
                                    <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>"
                                         alt="User" class="rounded-circle avatarUser mr-1" width="18" height="18">
                                    <span
                                        class="d-lg-none"><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.my_page') : trans('users.my_profile'), false); ?></span>
                                </div>
                            </a>
                        </li>

                        <?php if(auth()->user()->verified_id == 'yes'): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a class="nav-link px-2 link-menu-mobile py-1 balance">
                                    <div>
                                        <i class="iconmoon icon-Dollar mr-2"></i>
                                        <span
                                            class="d-lg-none balance"><?php echo e(trans('general.balance'), false); ?>: <?php echo e(Helper::amountFormatDecimal(auth()->user()->balance), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if($settings->disable_wallet == 'on' && auth()->user()->wallet != 0.00 || $settings->disable_wallet == 'off'): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a <?php if($settings->disable_wallet == 'off'): ?> href="<?php echo e(url('my/wallet'), false); ?>"
                                   <?php endif; ?> class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="iconmoon icon-Wallet mr-2"></i>
                                        <?php echo e(trans('general.wallet'), false); ?> <span
                                            class="balanceWallet"><?php echo e(Helper::amountFormatDecimal(auth()->user()->wallet), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if(auth()->user()->verified_id == 'yes'): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a href="<?php echo e(url('dashboard'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="bi bi-speedometer2 mr-2"></i>
                                        <span class="d-lg-none"><?php echo e(trans('admin.dashboard'), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item dropdown d-lg-none">
                            <a href="<?php echo e(url('my/payments'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                <div>
                                    <i class="bi bi-receipt mr-2"></i>
                                    <span class="d-lg-none"><?php echo e(trans('general.payments'), false); ?></span>
                                </div>
                            </a>
                        </li>

                        <?php if(Helper::showSectionMyCards()): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a href="<?php echo e(url('my/cards'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="feather icon-credit-card mr-2"></i>
                                        <span class="d-lg-none"><?php echo e(trans('general.my_cards'), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <?php if(auth()->user()->verified_id == 'yes'): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a href="<?php echo e(url('my/subscribers'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="feather icon-users mr-2"></i>
                                        <span class="d-lg-none"><?php echo e(trans('users.my_subscribers'), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item dropdown d-lg-none">
                            <a href="<?php echo e(url('my/subscriptions'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                <div>
                                    <i class="feather icon-user-check mr-2"></i>
                                    <span class="d-lg-none"><?php echo e(trans('users.my_subscriptions'), false); ?></span>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item dropdown d-lg-none">
                            <a href="<?php echo e(url('my/bookmarks'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                <div>
                                    <i class="feather icon-bookmark mr-2"></i>
                                    <span class="d-lg-none"><?php echo e(trans('general.bookmarks'), false); ?></span>
                                </div>
                            </a>
                        </li>

                        <?php if(auth()->user()->verified_id == 'no' && auth()->user()->verified_id != 'reject'): ?>
                            <li class="nav-item dropdown d-lg-none">
                                <a href="<?php echo e(url('settings/verify/account'), false); ?>"
                                   class="nav-link px-2 link-menu-mobile py-1">
                                    <div>
                                        <i class="feather icon-star mr-2"></i>
                                        <span class="d-lg-none"><?php echo e(trans('general.become_creator'), false); ?></span>
                                    </div>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item dropdown d-lg-none">
                            <a href="<?php echo e(auth()->user()->dark_mode == 'off' ? url('mode/dark') : url('mode/light'), false); ?>"
                               class="nav-link px-2 link-menu-mobile py-1">
                                <div>
                                    <i class="feather icon-<?php echo e(auth()->user()->dark_mode == 'off' ? 'moon' : 'sun', false); ?> mr-2"></i>
                                    <span
                                        class="d-lg-none"><?php echo e(auth()->user()->dark_mode == 'off' ? trans('general.dark_mode') : trans('general.light_mode'), false); ?></span>
                                </div>
                            </a>
                        </li>

                        <li class="nav-item dropdown d-lg-none mb-2">
                            <a href="<?php echo e(url('logout'), false); ?>" class="nav-link px-2 link-menu-mobile py-1">
                                <div>
                                    <i class="feather icon-log-out mr-2"></i>
                                    <span class="d-lg-none"><?php echo e(trans('auth.logout'), false); ?></span>
                                </div>
                            </a>
                        </li>
                        <!-- =========== End Menu Mobile -->


                        <li class="nav-item dropdown d-lg-block d-none">
                            <a class="nav-link px-2" href="<?php echo e(url('/'), false); ?>" title="<?php echo e(trans('admin.home'), false); ?>">
                                <i class="feather icon-home icon-navbar"></i>
                                <span class="d-lg-none align-middle ml-1"><?php echo e(trans('admin.home'), false); ?></span>
                            </a>
                        </li>

                        <?php if(auth()->user()->role == 'admin'): ?>
                            <li class="nav-item dropdown d-lg-block d-none">
                                <a class="nav-link px-2" href="<?php echo e(url('creators'), false); ?>"
                                   title="<?php echo e(trans('general.explore'), false); ?>">
                                    <i class="far	fa-compass icon-navbar"></i>
                                    <span class="d-lg-none align-middle ml-1"><?php echo e(trans('general.explore'), false); ?></span>
                                </a>
                            </li>
                        <?php endif; ?>

                        <li class="nav-item dropdown d-lg-block d-none">
                            <a href="<?php echo e(url('messages'), false); ?>" class="nav-link px-2" title="<?php echo e(trans('general.messages'), false); ?>">

						<span class="noti_msg notify <?php if(auth()->user()->messagesInbox() != 0): ?> d-block <?php endif; ?>">
							<?php echo e(auth()->user()->messagesInbox(), false); ?>

							</span>

                                <i class="feather icon-send icon-navbar"></i>
                                <span class="d-lg-none align-middle ml-1"><?php echo e(trans('general.messages'), false); ?></span>
                            </a>
                        </li>

                        <li class="nav-item dropdown d-lg-block d-none">
                            <a href="<?php echo e(url('notifications'), false); ?>" class="nav-link px-2"
                               title="<?php echo e(trans('general.notifications'), false); ?>">

						<span
                            class="noti_notifications notify <?php if(auth()->user()->notifications()->where('status', '0')->count()): ?> d-block <?php endif; ?>">
							<?php echo e(auth()->user()->notifications()->where('status', '0')->count(), false); ?>

							</span>

                                <i class="far fa-bell icon-navbar"></i>
                                <span class="d-lg-none align-middle ml-1"><?php echo e(trans('general.notifications'), false); ?></span>
                            </a>
                        </li>

                        <li class="nav-item dropdown d-lg-block d-none">
                            <a class="nav-link" href="#" id="nav-inner-success_dropdown_1" role="button"
                               data-toggle="dropdown">
                                <img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>" alt="User"
                                     class="rounded-circle avatarUser mr-1" width="24" height="24">
                                <span class="d-lg-none"><?php echo e(auth()->user()->first_name, false); ?></span>
                                <i class="feather icon-chevron-down m-0 align-middle"></i>
                            </a>
                            <div class="dropdown-menu mb-1 dropdown-menu-right dd-menu-user"
                                 aria-labelledby="nav-inner-success_dropdown_1">
                                <?php if(auth()->user()->role == 'admin'): ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('panel/admin'), false); ?>"><?php echo e(trans('admin.admin'), false); ?></a>
                                    <div class="dropdown-divider"></div>
                                <?php endif; ?>

                                <?php if(auth()->user()->verified_id == 'yes'): ?>
                                    <span class="dropdown-item dropdown-navbar balance">
							<?php echo e(trans('general.balance'), false); ?>: <?php echo e(Helper::amountFormatDecimal(auth()->user()->balance), false); ?>

						</span>
                                <?php endif; ?>

                                <?php if($settings->disable_wallet == 'on' && auth()->user()->wallet != 0.00 || $settings->disable_wallet == 'off'): ?>
                                    <?php if($settings->disable_wallet == 'off'): ?>
                                        <a class="dropdown-item dropdown-navbar" href="<?php echo e(url('my/wallet'), false); ?>">
                                            <?php echo e(trans('general.wallet'), false); ?>:
                                            <span
                                                class="balanceWallet"><?php echo e(Helper::amountFormatDecimal(auth()->user()->wallet), false); ?></span>
                                        </a>
                                    <?php else: ?>
                                        <span class="dropdown-item dropdown-navbar balance">
								<?php echo e(trans('general.wallet'), false); ?>:
								<span
                                    class="balanceWallet"><?php echo e(Helper::amountFormatDecimal(auth()->user()->wallet), false); ?></span>
							</span>
                                    <?php endif; ?>

                                <?php endif; ?>

                                <?php if($settings->disable_wallet == 'on' && auth()->user()->verified_id == 'yes'): ?>
                                    <div class="dropdown-divider"></div>
                                <?php endif; ?>

                                <a class="dropdown-item dropdown-navbar url-user"
                                   href="<?php echo e(url(auth()->User()->username), false); ?>"><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.my_page') : trans('users.my_profile'), false); ?></a>
                                <?php if(auth()->user()->verified_id == 'yes'): ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('dashboard'), false); ?>"><?php echo e(trans('admin.dashboard'), false); ?></a>
                                <?php endif; ?>
                                <a class="dropdown-item dropdown-navbar"
                                   href="<?php echo e(url('my/payments'), false); ?>"><?php echo e(trans('general.payments'), false); ?></a>
                                <?php if(Helper::showSectionMyCards()): ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('my/cards'), false); ?>"><?php echo e(trans('general.my_cards'), false); ?></a>
                                <?php endif; ?>
                                <?php if(auth()->user()->verified_id == 'yes'): ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('my/subscribers'), false); ?>"><?php echo e(trans('users.my_subscribers'), false); ?></a>
                                <?php endif; ?>
                                <a class="dropdown-item dropdown-navbar"
                                   href="<?php echo e(url('my/subscriptions'), false); ?>"><?php echo e(trans('users.my_subscriptions'), false); ?></a>
                                <a class="dropdown-item dropdown-navbar"
                                   href="<?php echo e(url('my/bookmarks'), false); ?>"><?php echo e(trans('general.bookmarks'), false); ?></a>

                                <?php if(auth()->user()->verified_id == 'no' && auth()->user()->verified_id != 'reject'): ?>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('settings/verify/account'), false); ?>"><?php echo e(trans('general.become_creator'), false); ?></a>
                                <?php endif; ?>

                                <div class="dropdown-divider"></div>

                                <?php if(auth()->user()->dark_mode == 'off'): ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('mode/dark'), false); ?>"><?php echo e(trans('general.dark_mode'), false); ?></a>
                                <?php else: ?>
                                    <a class="dropdown-item dropdown-navbar"
                                       href="<?php echo e(url('mode/light'), false); ?>"><?php echo e(trans('general.light_mode'), false); ?></a>
                                <?php endif; ?>

                                <div class="dropdown-divider dropdown-navbar"></div>
                                <a class="dropdown-item dropdown-navbar"
                                   href="<?php echo e(url('logout'), false); ?>"><?php echo e(trans('auth.logout'), false); ?></a>
                            </div>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link btn btn-main btn-primary pr-3 pl-3" href="<?php echo e(url('settings/page'), false); ?>">
                                <?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?>

                                <small class="pl-1"><i class="fa fa-long-arrow-alt-right"></i></small></a>
                        </li>

                    <?php endif; ?>

                </ul>
            </div>
        </div>
    </nav>
</header>
<?php /**PATH /home/allajwno/public_html/resources/views/includes/navbar.blade.php ENDPATH**/ ?>