

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('admin.members'), false); ?> (<?php echo e($data->total(), false); ?>)
          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('info_message')): ?>
		    <div class="alert alert-warning">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		      <i class="fa fa-warning margin-separator"></i>  <?php echo e(Session::get('info_message'), false); ?>

		    </div>
		<?php endif; ?>

		<?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i>  <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                  	<?php if( $data->count() != 0 && $data->currentPage() != 1 ): ?>
                  		<a href="<?php echo e(url('panel/admin/members'), false); ?>"><?php echo e(trans('admin.view_all_members'), false); ?></a>
                  	<?php else: ?>
                  		<?php echo e(trans('admin.members'), false); ?>

                  	<?php endif; ?>

                  	</h3>

                <div class="box-tools">
                 <?php if( $data->total() !=  0 ): ?>
                    <!-- form -->
                    <form role="search" autocomplete="off" action="<?php echo e(url('panel/admin/members'), false); ?>" method="get">
	                 <div class="input-group input-group-sm w-150">
	                  <input type="text" name="q" class="form-control pull-right" placeholder="<?php echo e(trans('general.search'), false); ?>">

	                  <div class="input-group-btn">
	                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
	                  </div>
	                </div>
                </form><!-- form -->
                <?php endif; ?>
              </div>

            </div><!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	<?php if( $data->total() !=  0 && $data->count() != 0 ): ?>
                   <tr>
                      <th class="active">ID</th>
                      <th class="active"><?php echo e(trans('auth.full_name'), false); ?></th>
                      <th class="active"><?php echo e(trans('auth.email'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.balance'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.wallet'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.posts'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.date'), false); ?></th>
                      <th class="active">IP</th>
                      <th class="active"><?php echo e(trans('admin.verified'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.status'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.actions'), false); ?></th>
                    </tr>

                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e($user->id, false); ?></td>
                      <td>
                        <a href="<?php echo e(url($user->username), false); ?>" target="_blank">
                        <img src="<?php echo e(Helper::getFile(config('path.avatar').$user->avatar), false); ?>" width="40" height="40" class="img-circle" />
                        <?php echo e($user->name, false); ?>

                        </a>
                      </td>
                      <td><?php echo e($user->email, false); ?></td>
                      <td><?php echo e(Helper::amountFormatDecimal($user->balance), false); ?></td>
                      <td><?php echo e(Helper::amountFormatDecimal($user->wallet), false); ?></td>
                      <td><?php echo e($user->updates()->count(), false); ?></td>
                      <td><?php echo e(Helper::formatDate($user->date), false); ?></td>
                      <td><?php echo e($user->ip ? $user->ip : trans('general.no_available'), false); ?></td>
                      <?php if( $user->verified_id == 'no' ) {
                       			$verified    = 'warning';
 								$_verified = trans('admin.pending');
              } elseif( $user->verified_id == 'yes' ) {
                       			$verified = 'success';
 								$_verified = trans('admin.verified');
                       		} else {
                       			$verified = 'danger';
 								$_verified = trans('admin.reject');
                       		}
                       		?>
                       <td><span class="label label-<?php echo e($verified, false); ?>"><?php echo e($_verified, false); ?></span></td>

                      <?php if( $user->status == 'pending' ) {
                       			$mode    = 'warning';
 								$_status = trans('admin.pending');
                       		} elseif( $user->status == 'active' ) {
                       			$mode = 'success';
 								$_status = trans('admin.active');
                       		} else {
                       			$mode = 'warning';
 								$_status = trans('admin.suspended');
                       		}
                       		?>
                       <td><span class="label label-<?php echo e($mode, false); ?>"><?php echo e($_status, false); ?></span></td>
                      <td>

                     <?php if( $user->id <> Auth::user()->id && $user->id <> 1 ): ?>

                   <a href="<?php echo e(route('user.edit', $user->id), false); ?>" class="btn btn-success btn-sm padding-btn">
                      		<?php echo e(trans('admin.edit'), false); ?>

                      	</a>

                   <?php echo Form::open([
			            'method' => 'DELETE',
			            'route' => ['user.destroy', $user->id],
			            'id' => 'form'.$user->id,
			            'class' => 'displayInline'
				        ]); ?>

	            	<?php echo Form::submit(trans('admin.delete'), ['data-url' => $user->id, 'class' => 'btn btn-danger btn-sm padding-btn actionDelete']); ?>

	        	<?php echo Form::close(); ?>


	       <?php else: ?>
	        ------------
                      		<?php endif; ?>

                      		</td>

                    </tr><!-- /.TR -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <hr />
                    	<h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>

                    	<?php if( isset( $query ) ): ?>
                    	<div class="col-md-12 text-center padding-bottom-15">
                    		<a href="<?php echo e(url('panel/admin/members'), false); ?>" class="btn btn-sm btn-danger"><?php echo e(trans('auth.back'), false); ?></a>
                    	</div>

                    	<?php endif; ?>
                    <?php endif; ?>

                  </tbody>

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php if($data->hasPages()): ?>
             <?php echo e($data->appends(['q' => $query])->links(), false); ?>

           <?php endif; ?>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/members.blade.php ENDPATH**/ ?>