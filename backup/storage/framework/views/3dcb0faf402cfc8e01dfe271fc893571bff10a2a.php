<?php $__env->startSection('title'); ?> <?php echo e(trans('general.verify_account'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="feather icon-check-circle mr-2"></i> <?php echo e(trans('general.verify_account'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(Auth::user()->verified_id != 'yes' ? trans('general.verified_account_desc') : trans('general.verified_account'), false); ?></p>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          <?php if(session('status')): ?>
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    <?php echo e(session('status'), false); ?>

                  </div>
                <?php endif; ?>

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <?php if($settings->requests_verify_account == 'on'
            && auth()->user()->verified_id != 'yes'
            && auth()->user()->verificationRequests() != 1
            && auth()->user()->verified_id != 'reject'): ?>

            <?php if(auth()->user()->countries_id != ''
                && auth()->user()->birthdate != ''
                && auth()->user()->cover != ''
                && auth()->user()->cover != $settings->cover_default
                && auth()->user()->avatar != $settings->avatar
              ): ?>

          <div class="alert alert-warning mr-1">
          <span class="alert-inner--text"><i class="fa fa-exclamation-triangle"></i> <?php echo e(trans('general.warning_verification_info'), false); ?></span>
        </div>

          <form method="POST" id="formVerify" action="<?php echo e(url('settings/verify/account'), false); ?>" accept-charset="UTF-8" enctype="multipart/form-data">

            <?php echo csrf_field(); ?>

          <div class="form-group">
            <div class="input-group mb-4">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-map-marked-alt"></i></span>
              </div>
              <input class="form-control" name="address" placeholder="<?php echo e(trans('general.address'), false); ?>" value="<?php echo e(old('address'), false); ?>" type="text">
            </div>
            </div>

            <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-map-pin"></i></span>
                  </div>
                  <input class="form-control" name="city" placeholder="<?php echo e(trans('general.city'), false); ?>" value="<?php echo e(old('city'), false); ?>" type="text">
                </div>
              </div>

              <div class="form-group">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-map-marker-alt"></i></span>
                    </div>
                    <input class="form-control" name="zip" placeholder="<?php echo e(trans('general.zip'), false); ?>" value="<?php echo e(old('zip'), false); ?>" type="text">
                  </div>
                </div>

                <?php if(auth()->user()->countries_id == 1): ?>
                  <div class="mb-3 text-center">
                    <span class="btn-block mb-2" id="previewImageFormW9"></span>

                      <input type="file" name="form_w9" id="fileVerifiyAccountFormW9" accept="application/pdf" class="visibility-hidden">
                      <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFileFormW9"><?php echo e(trans('general.upload_form_w9'), false); ?> (PDF) <?php echo e(trans('general.maximum'), false); ?>: <?php echo e(Helper::formatBytes($settings->file_size_allowed_verify_account * 1024), false); ?></button>

                    <small class="text-muted btn-block"><?php echo e(trans('general.form_w9_required'), false); ?></small>
                    <h6 class="btn-block text-center font-weight-bold">
                      <a href="https://www.irs.gov/pub/irs-pdf/fw9.pdf" target="_blank"><?php echo e(__('general.complete_form_W9_here'), false); ?> <i class="feather icon-external-link"></i></a>
                    </h6>
                  </div>

                <?php endif; ?>

                <div class="mb-3 text-center">
                  <span class="btn-block mb-2" id="previewImage"></span>

                    <input type="file" name="image" id="fileVerifiyAccount" accept="image/*,application/x-zip-compressed" class="visibility-hidden">
                    <button class="btn btn-1 btn-block btn-outline-primary mb-2 border-dashed" type="button" id="btnFilePhoto"><?php echo e(trans('general.upload_image'), false); ?> (JPG, PNG, GIF) <?php echo e(trans('general.or'), false); ?> ZIP - <?php echo e(trans('general.maximum'), false); ?>: <?php echo e(Helper::formatBytes($settings->file_size_allowed_verify_account * 1024), false); ?></button>

                  <small class="text-muted btn-block"><?php echo e(trans('general.info_verification_user'), false); ?></small>
                </div>

                <button class="btn btn-1 btn-success btn-block" id="sendData" type="submit"><?php echo e(trans('general.send_approval'), false); ?></button>
          </form>

        <?php else: ?>

          <div class="alert alert-danger">
          <span class="alert-inner--text"><i class="fa fa-exclamation-triangle mr-1"></i> <?php echo e(trans('general.complete_profile_alert'), false); ?></span>

          <ul class="list-unstyled">
            <br>

            <?php if(auth()->user()->avatar == $settings->avatar): ?>
              <li>
                <i class="far fa-times-circle"></i> <?php echo e(__('general.set_avatar'), false); ?> <a href="<?php echo e(url(auth()->user()->username), false); ?>" class="text-white link-border"><?php echo e(__('general.upload'), false); ?> <i class="feather icon-arrow-right"></i></a>
              </li>
            <?php endif; ?>

            <?php if(auth()->user()->cover == '' || auth()->user()->cover == $settings->cover_default): ?>
            <li>
              <i class="far fa-times-circle"></i> <?php echo e(__('general.set_cover'), false); ?> <a href="<?php echo e(url(auth()->user()->username), false); ?>" class="text-white link-border"><?php echo e(__('general.upload'), false); ?> <i class="feather icon-arrow-right"></i></a>
            </li>
          <?php endif; ?>

          <?php if(auth()->user()->countries_id == ''): ?>
            <li>
              <i class="far fa-times-circle"></i> <?php echo e(__('general.set_country'), false); ?> <a href="<?php echo e(url('settings/page'), false); ?>" class="text-white link-border"><?php echo e(__('admin.edit'), false); ?> <i class="feather icon-arrow-right"></i></a>
            </li>
            <?php endif; ?>

            <?php if(auth()->user()->birthdate == ''): ?>
            <li>
              <i class="far fa-times-circle"></i> <?php echo e(__('general.set_birthdate'), false); ?> <a href="<?php echo e(url('settings/page'), false); ?>" class="text-white link-border"><?php echo e(__('admin.edit'), false); ?> <i class="feather icon-arrow-right"></i></a>
            </li>
          <?php endif; ?>
          </ul>
        </div>

            <?php endif; ?>

        <?php elseif(auth()->user()->verificationRequests() == 1): ?>
          <div class="alert alert-primary alert-dismissible text-center fade show" role="alert">
            <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
          <span class="alert-inner--text"><?php echo e(trans('admin.pending_request_verify'), false); ?></span>
        </div>
      <?php elseif(auth()->user()->verified_id == 'reject'): ?>
        <div class="alert alert-danger alert-dismissible text-center fade show" role="alert">
          <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
        <span class="alert-inner--text"><?php echo e(trans('admin.rejected_request'), false); ?></span>
      </div>
    <?php elseif(auth()->user()->verified_id != 'yes' && $settings->requests_verify_account == 'off'): ?>
      <div class="alert alert-primary alert-dismissible text-center fade show" role="alert">
        <span class="alert-inner--icon mr-2"><i class="fa fa-info-circle"></i></span>
      <span class="alert-inner--text"><?php echo e(trans('general.info_receive_verification_requests'), false); ?></span>
    </div>

        <?php else: ?>
          <div class="alert alert-success alert-dismissible text-center fade show" role="alert">
            <span class="alert-inner--icon mr-2"><i class="feather icon-check-circle"></i></span>
          <span class="alert-inner--text"><?php echo e(trans('general.verified_account_success'), false); ?></span>
        </div>

        <?php endif; ?>

        </div><!-- end col-md-6 -->
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/users/verify_account.blade.php ENDPATH**/ ?>