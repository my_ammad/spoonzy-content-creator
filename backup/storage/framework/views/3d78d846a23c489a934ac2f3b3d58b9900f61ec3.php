<?php $__env->startSection('title'); ?> <?php echo e(trans('general.edit_my_page'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
  <link rel="stylesheet" href="<?php echo e(asset('public/plugins/datepicker/datepicker3.css'), false); ?>" rel="stylesheet" type="text/css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="bi bi-pencil mr-2"></i> <?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('users.settings_page_desc'), false); ?></p>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

          <?php if(session('status')): ?>
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>

                    <?php echo e(trans('admin.success_update'), false); ?>

                  </div>
                <?php endif; ?>

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

          <form method="POST" action="<?php echo e(url('settings/page'), false); ?>" id="formEditPage" accept-charset="UTF-8" enctype="multipart/form-data">

            <?php echo csrf_field(); ?>

            <input type="hidden" id="featured_content" name="featured_content" value="<?php echo e(auth()->user()->featured_content, false); ?>">

          <div class="form-group">
            <label><?php echo e(trans('auth.full_name'), false); ?> *</label>
            <div class="input-group mb-4">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-user"></i></span>
            </div>
                <input class="form-control" name="full_name" required placeholder="<?php echo e(trans('auth.full_name'), false); ?>" value="<?php echo e(auth()->user()->name, false); ?>"  type="text">
            </div>
          </div><!-- End form-group -->

          <div class="form-group">
            <label><?php echo e(trans('auth.username'), false); ?> *</label>
            <div class="input-group mb-2">
            <div class="input-group-prepend">
              <span class="input-group-text pr-0"><?php echo e(Helper::removeHTPP(url('/')), false); ?>/</span>
            </div>
                <input class="form-control" name="username" maxlength="25" required placeholder="<?php echo e(trans('auth.username'), false); ?>" value="<?php echo e(auth()->user()->username, false); ?>"  type="text">
            </div>
            <?php if(auth()->user()->verified_id == 'yes'): ?>
            <div class="text-muted btn-block">
              <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" name="hide_name" value="yes" <?php if(auth()->user()->hide_name == 'yes'): ?> checked <?php endif; ?> id="customSwitch1">
                <label class="custom-control-label switch" for="customSwitch1"><?php echo e(trans('general.hide_name'), false); ?></label>
              </div>
            </div>
          <?php endif; ?>
          </div><!-- End form-group -->

          <div class="form-group">
                <input class="form-control" placeholder="<?php echo e(trans('auth.email'), false); ?> *" <?php echo auth()->user()->id == 1 ? 'name="email"' : 'disabled'; ?> value="<?php echo e(auth()->user()->email, false); ?>" type="text">
            </div><!-- End form-group -->

          <div class="row form-group mb-0">
            <div class="col-md-6">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-user-tie"></i></span>
                  </div>
                  <input class="form-control" name="profession" placeholder="<?php echo e(trans('users.profession_ocupation'), false); ?>" value="<?php echo e(auth()->user()->profession, false); ?>" type="text">
                </div>
              </div><!-- ./col-md-6 -->

              <div class="col-md-6">
                <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fa fa-language"></i></span>
                </div>
                <select name="language" class="form-control custom-select">
                  <option <?php if(auth()->user()->language == ''): ?> selected="selected" <?php endif; ?> value="">(<?php echo e(trans('general.language'), false); ?>) <?php echo e(__('general.not_specified'), false); ?></option>
                  <?php $__currentLoopData = Languages::orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $languages): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option <?php if(auth()->user()->language == $languages->abbreviation): ?> selected="selected" <?php endif; ?> value="<?php echo e($languages->abbreviation, false); ?>"><?php echo e($languages->name, false); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </select>
                  </div>
                </div><!-- ./col-md-6 -->
            </div><!-- End Row Form Group -->

              <div class="row form-group mb-0">
                  <div class="col-md-6">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar-alt"></i></span>
                        </div>
                        <input class="form-control datepicker" name="birthdate" placeholder="<?php echo e(trans('general.birthdate'), false); ?> *"  value="<?php echo e(auth()->user()->birthdate, false); ?>" autocomplete="off" type="text">
                      </div>
                      <small class="form-text text-muted mb-4"><?php echo e(trans('general.valid_formats'), false); ?> <strong>01/31/2000</strong></small>
                    </div><!-- ./col-md-6 -->

                    <div class="col-md-6">
                      <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-venus-mars"></i></span>
                      </div>
                      <select required name="gender" class="form-control custom-select">
                        <option <?php if(auth()->user()->gender == '' ): ?> selected="selected" <?php endif; ?> value="">(<?php echo e(trans('general.gender'), false); ?>) <?php echo e(__('general.not_specified'), false); ?></option>
                        <?php $__currentLoopData = $genders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option <?php if(auth()->user()->gender == $gender): ?> selected="selected" <?php endif; ?> value="<?php echo e($gender, false); ?>"><?php echo e(__('general.'.$gender), false); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                      </div><!-- ./col-md-6 -->
                    </div><!-- End Row Form Group -->

        <?php if(auth()->user()->verified_id == 'yes'): ?>
        <div class="row form-group mb-0">
            <div class="col-md-6">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                  </div>
                  <input class="form-control" name="website" placeholder="<?php echo e(trans('users.website'), false); ?>"  value="<?php echo e(auth()->user()->website, false); ?>" type="text">
                </div>
              </div><!-- ./col-md-6 -->

              <div class="col-md-6" id="billing">
                <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-lightbulb"></i></span>
                </div>
                <select required name="categories_id" class="form-control custom-select" >
                      <?php $__currentLoopData = Categories::where('mode','on')->orderBy('name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option <?php if(auth()->user()->categories_id == $category->id ): ?> selected="selected" <?php endif; ?> value="<?php echo e($category->id, false); ?>"><?php echo e(Lang::has('categories.' . $category->slug) ? __('categories.' . $category->slug) : $category->name, false); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </select>
                      </div>
                </div><!-- ./col-md-6 -->
              </div><!-- End Row Form Group -->
            <?php endif; ?>

              <div class="row form-group mb-0">

                <div class="col-lg-12 py-2">
                  <small class="text-muted">-- <?php echo e(trans('general.billing_information'), false); ?></small>
                </div>

                <div class="col-lg-12">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-building"></i></span>
                      </div>
                      <input class="form-control" name="company" placeholder="<?php echo e(trans('general.company'), false); ?>"  value="<?php echo e(auth()->user()->company, false); ?>" type="text">
                    </div>
                  </div><!-- ./col-md-6 -->

                <div class="col-md-6">
                  <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-globe"></i></span>
                  </div>
                  <select name="countries_id" class="form-control custom-select">
                    <option value=""><?php echo e(trans('general.select_your_country'), false); ?> *</option>
                        <?php $__currentLoopData = Countries::orderBy('country_name')->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $country): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <option <?php if( auth()->user()->countries_id == $country->id ): ?> selected="selected" <?php endif; ?> value="<?php echo e($country->id, false); ?>"><?php echo e($country->country_name, false); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        </div>
                  </div><!-- ./col-md-6 -->

                  <div class="col-md-6">
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-map-pin"></i></span>
                        </div>
                        <input class="form-control" name="city" placeholder="<?php echo e(trans('general.city'), false); ?>"  value="<?php echo e(auth()->user()->city, false); ?>" type="text">
                      </div>
                    </div><!-- ./col-md-6 -->

                    <div class="col-md-6 <?php if(auth()->user()->verified_id == 'no'): ?> scrollError <?php endif; ?>">
                        <div class="input-group mb-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-map-marked-alt"></i></span>
                          </div>
                          <input class="form-control" name="address" placeholder="<?php echo e(trans('general.address'), false); ?>"  value="<?php echo e(auth()->user()->address, false); ?>" type="text">
                        </div>
                      </div><!-- ./col-md-6 -->

                      <div class="col-md-6">
                          <div class="input-group mb-4">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-map-marker-alt"></i></span>
                            </div>
                            <input class="form-control" name="zip" placeholder="<?php echo e(trans('general.zip'), false); ?>"  value="<?php echo e(auth()->user()->zip, false); ?>" type="text">
                          </div>
                        </div><!-- ./col-md-6 -->

              </div><!-- End Row Form Group -->

              <?php if(auth()->user()->verified_id == 'yes'): ?>
              <div class="row form-group mb-0">
                <div class="col-lg-12 py-2">
                  <small class="text-muted">-- <?php echo e(trans('admin.profiles_social'), false); ?></small>
                </div>

                  <div class="col-md-6">
                      <div class="input-group mb-4">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fab fa-facebook-f"></i></span>
                        </div>
                        <input class="form-control" name="facebook" placeholder="https://facebook.com/username"  value="<?php echo e(auth()->user()->facebook, false); ?>" type="text">
                      </div>
                    </div><!-- ./col-md-6 -->

                    <div class="col-md-6">
                        <div class="input-group mb-4">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fab fa-twitter"></i></span>
                          </div>
                          <input class="form-control" name="twitter" placeholder="https://twitter.com/username"  value="<?php echo e(auth()->user()->twitter, false); ?>" type="text">
                        </div>
                      </div><!-- ./col-md-6 -->
                    </div><!-- End Row Form Group -->

                    <div class="row form-group mb-0">
                        <div class="col-md-6">
                            <div class="input-group mb-4">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fab fa-instagram"></i></span>
                              </div>
                              <input class="form-control" name="instagram" placeholder="https://instagram.com/username"  value="<?php echo e(auth()->user()->instagram, false); ?>" type="text">
                            </div>
                          </div><!-- ./col-md-6 -->

                          <div class="col-md-6">
                              <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fab fa-youtube"></i></span>
                                </div>
                                <input class="form-control" name="youtube" placeholder="https://youtube.com/username"  value="<?php echo e(auth()->user()->youtube, false); ?>" type="text">
                              </div>
                            </div><!-- ./col-md-6 -->
                          </div><!-- End Row Form Group -->

                          <div class="row form-group mb-0">
                              <div class="col-md-6">
                                  <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text"><i class="fab fa-pinterest-p"></i></span>
                                    </div>
                                    <input class="form-control" name="pinterest" placeholder="https://pinterest.com/username"  value="<?php echo e(auth()->user()->pinterest, false); ?>" type="text">
                                  </div>
                                </div><!-- ./col-md-6 -->

                                <div class="col-md-6">
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fab fa-github"></i></span>
                                      </div>
                                      <input class="form-control" name="github" placeholder="https://github.com/username"  value="<?php echo e(auth()->user()->github, false); ?>" type="text">
                                    </div>
                                  </div><!-- ./col-md-6 -->
                                </div><!-- End Row Form Group -->

                          <div class="form-group">
                            <label class="w-100"><i class="fa fa-bullhorn text-muted"></i> <?php echo e(trans('users.your_story'), false); ?> *
                              <span id="the-count" class="float-right d-inline">
                                <span id="current"></span>
                                <span id="maximum">/ <?php echo e($settings->story_length, false); ?></span>
                              </span>
                            </label>
                            <textarea name="story" id="story" required rows="5" cols="40" class="form-control textareaAutoSize scrollError"><?php echo e(auth()->user()->story ? auth()->user()->story : old('story'), false); ?></textarea>

                          </div><!-- End Form Group -->
                        <?php endif; ?>

                          <!-- Alert -->
                          <div class="alert alert-danger my-3 display-none" id="errorUdpateEditPage">
                           <ul class="list-unstyled m-0" id="showErrorsUdpatePage"><li></li></ul>
                         </div><!-- Alert -->

                          <button class="btn btn-1 btn-success btn-block" data-msg-success="<?php echo e(trans('admin.success_update'), false); ?>" id="saveChangesEditPage" type="submit"><i></i> <?php echo e(trans('general.save_changes'), false); ?></button>

                      <?php if(auth()->user()->id != 1): ?>
                      <div class="text-center mt-3">
                        <a href="<?php echo e(url('account/delete'), false); ?>"><?php echo e(trans('general.delete_account'), false); ?></a>
                      </div>
                    <?php endif; ?>
                  </form>
                </div><!-- end col-md-6 -->
              </div>
            </div>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
  <script src="<?php echo e(asset('public/plugins/datepicker/bootstrap-datepicker.js'), false); ?>"></script>
  <?php if(config('app.locale') != 'en'): ?>
    <script src="<?php echo e(asset('public/plugins/datepicker/locales/bootstrap-datepicker.'.config('app.locale').'.js'), false); ?>"></script>
  <?php endif; ?>

<script type="text/javascript">

<?php if(auth()->user()->verified_id == 'yes'): ?>
$('#current').html($('#story').val().length);
<?php endif; ?>

$('.datepicker').datepicker({
    format: 'mm/dd/yyyy',
    language: '<?php echo e(config('app.locale'), false); ?>'
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/edit_my_page.blade.php ENDPATH**/ ?>