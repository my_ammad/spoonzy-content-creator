

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.general_settings'), false); ?>


            		<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.limits'), false); ?>

          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		        <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo e(trans('admin.limits'), false); ?></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/settings/limits'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">
                  <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group margin-zero">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.file_size_allowed'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="file_size_allowed" class="form-control">
                            <option <?php if( $settings->file_size_allowed == 1024 ): ?> selected="selected" <?php endif; ?> value="1024">1 MB</option>
            						  	<option <?php if( $settings->file_size_allowed == 2048 ): ?> selected="selected" <?php endif; ?> value="2048">2 MB</option>
            						  	<option <?php if( $settings->file_size_allowed == 3072 ): ?> selected="selected" <?php endif; ?> value="3072">3 MB</option>
            						  	<option <?php if( $settings->file_size_allowed == 4096 ): ?> selected="selected" <?php endif; ?> value="4096">4 MB</option>
            						  	<option <?php if( $settings->file_size_allowed == 5120 ): ?> selected="selected" <?php endif; ?> value="5120">5 MB</option>
            						  	<option <?php if( $settings->file_size_allowed == 10240 ): ?> selected="selected" <?php endif; ?> value="10240">10 MB</option>
                            <option <?php if( $settings->file_size_allowed == 15360 ): ?> selected="selected" <?php endif; ?> value="15360">15 MB</option>
                            <option <?php if( $settings->file_size_allowed == 20480 ): ?> selected="selected" <?php endif; ?> value="20480">20 MB</option>
                            <option <?php if( $settings->file_size_allowed == 25600 ): ?> selected="selected" <?php endif; ?> value="25600">25 MB</option>
                            <option <?php if( $settings->file_size_allowed == 30720 ): ?> selected="selected" <?php endif; ?> value="30720">30 MB</option>
                            <option <?php if( $settings->file_size_allowed == 40960 ): ?> selected="selected" <?php endif; ?> value="40960">40 MB</option>
                            <option <?php if( $settings->file_size_allowed == 51200 ): ?> selected="selected" <?php endif; ?> value="51200">50 MB</option>
                            <option <?php if( $settings->file_size_allowed == 102400 ): ?> selected="selected" <?php endif; ?> value="102400">100 MB</option>
                            <option <?php if( $settings->file_size_allowed == 153600 ): ?> selected="selected" <?php endif; ?> value="153600">150 MB</option>
                            <option <?php if( $settings->file_size_allowed == 256000 ): ?> selected="selected" <?php endif; ?> value="256000">250 MB</option>
                            <option <?php if( $settings->file_size_allowed == 307200 ): ?> selected="selected" <?php endif; ?> value="307200">300 MB</option>
                            <option <?php if( $settings->file_size_allowed == 512000 ): ?> selected="selected" <?php endif; ?> value="512000">500 MB</option>
                            <option <?php if( $settings->file_size_allowed == 716800 ): ?> selected="selected" <?php endif; ?> value="716800">700 MB</option>
                            <option <?php if( $settings->file_size_allowed == 819200 ): ?> selected="selected" <?php endif; ?> value="819200">800 MB</option>
                            <option <?php if( $settings->file_size_allowed == 1024000 ): ?> selected="selected" <?php endif; ?> value="1024000">1 GB</option>

                            <option <?php if( $settings->file_size_allowed == 2048000 ): ?> selected="selected" <?php endif; ?> value="2048000">2 GB</option>
                            <option <?php if( $settings->file_size_allowed == 3072000 ): ?> selected="selected" <?php endif; ?> value="3072000">3 GB</option>
                            <option <?php if( $settings->file_size_allowed == 5120000 ): ?> selected="selected" <?php endif; ?> value="5120000">5 GB</option>
                          </select>
                          <span class="help-block "><?php echo e(trans('admin.upload_max_filesize_info'), false); ?> <strong><?php echo str_replace('M', 'MB', ini_get('upload_max_filesize')) ?></strong></span>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group margin-zero">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.file_size_allowed'), false); ?> (<?php echo e(trans('general.verify_account'), false); ?>)</label>
                      <div class="col-sm-10">
                      	<select name="file_size_allowed_verify_account" class="form-control">
                            <option <?php if( $settings->file_size_allowed_verify_account == 1024 ): ?> selected="selected" <?php endif; ?> value="1024">1 MB</option>
            						  	<option <?php if( $settings->file_size_allowed_verify_account == 2048 ): ?> selected="selected" <?php endif; ?> value="2048">2 MB</option>
            						  	<option <?php if( $settings->file_size_allowed_verify_account == 3072 ): ?> selected="selected" <?php endif; ?> value="3072">3 MB</option>
            						  	<option <?php if( $settings->file_size_allowed_verify_account == 4096 ): ?> selected="selected" <?php endif; ?> value="4096">4 MB</option>
            						  	<option <?php if( $settings->file_size_allowed_verify_account == 5120 ): ?> selected="selected" <?php endif; ?> value="5120">5 MB</option>
            						  	<option <?php if( $settings->file_size_allowed_verify_account == 10240 ): ?> selected="selected" <?php endif; ?> value="10240">10 MB</option>
                            <option <?php if( $settings->file_size_allowed_verify_account == 15360 ): ?> selected="selected" <?php endif; ?> value="15360">15 MB</option>
                              <option <?php if( $settings->file_size_allowed_verify_account == 20480 ): ?> selected="selected" <?php endif; ?> value="20480">20 MB</option>
                                <option <?php if( $settings->file_size_allowed_verify_account == 30720 ): ?> selected="selected" <?php endif; ?> value="30720">30 MB</option>
                          </select>
                          <span class="help-block "><?php echo e(trans('admin.upload_max_filesize_info'), false); ?> <strong><?php echo str_replace('M', 'MB', ini_get('upload_max_filesize')) ?></strong></span>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group margin-zero">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.post_length'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="update_length" class="form-control">
                            <option <?php if( $settings->update_length == 100 ): ?> selected="selected" <?php endif; ?> value="100">100 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->update_length == 150 ): ?> selected="selected" <?php endif; ?> value="150">150 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->update_length == 200 ): ?> selected="selected" <?php endif; ?> value="200">200 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->update_length == 250 ): ?> selected="selected" <?php endif; ?> value="250">250 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->update_length == 300 ): ?> selected="selected" <?php endif; ?> value="300">300 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->update_length == 400 ): ?> selected="selected" <?php endif; ?> value="400">400 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 500 ): ?> selected="selected" <?php endif; ?> value="500">500 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 700 ): ?> selected="selected" <?php endif; ?> value="700">700 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 1000 ): ?> selected="selected" <?php endif; ?> value="1000">1000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 2000 ): ?> selected="selected" <?php endif; ?> value="2000">2000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 3000 ): ?> selected="selected" <?php endif; ?> value="3000">3000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 4000 ): ?> selected="selected" <?php endif; ?> value="4000">4000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->update_length == 5000 ): ?> selected="selected" <?php endif; ?> value="5000">5000 <?php echo e(trans('admin.characters'), false); ?></option>
                          </select>
                          <span class="help-block "><?php echo e(trans('admin.post_length_info'), false); ?></span>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group margin-zero">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.story_length'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="story_length" class="form-control">
                            <option <?php if( $settings->story_length == 100 ): ?> selected="selected" <?php endif; ?> value="100">100 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->story_length == 150 ): ?> selected="selected" <?php endif; ?> value="150">150 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->story_length == 200 ): ?> selected="selected" <?php endif; ?> value="200">200 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->story_length == 250 ): ?> selected="selected" <?php endif; ?> value="250">250 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->story_length == 300 ): ?> selected="selected" <?php endif; ?> value="300">300 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->story_length == 400 ): ?> selected="selected" <?php endif; ?> value="400">400 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 500 ): ?> selected="selected" <?php endif; ?> value="500">500 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 700 ): ?> selected="selected" <?php endif; ?> value="700">700 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 1000 ): ?> selected="selected" <?php endif; ?> value="1000">1000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 2000 ): ?> selected="selected" <?php endif; ?> value="2000">2000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 3000 ): ?> selected="selected" <?php endif; ?> value="3000">3000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 4000 ): ?> selected="selected" <?php endif; ?> value="4000">4000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->story_length == 5000 ): ?> selected="selected" <?php endif; ?> value="5000">5000 <?php echo e(trans('admin.characters'), false); ?></option>
                          </select>
                          <span class="help-block "><?php echo e(trans('admin.story_length_info'), false); ?></span>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group margin-zero">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.comment_length'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="comment_length" class="form-control">
                            <option <?php if( $settings->comment_length == 100 ): ?> selected="selected" <?php endif; ?> value="100">100 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->comment_length == 150 ): ?> selected="selected" <?php endif; ?> value="150">150 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->comment_length == 200 ): ?> selected="selected" <?php endif; ?> value="200">200 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->comment_length == 250 ): ?> selected="selected" <?php endif; ?> value="250">250 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->comment_length == 300 ): ?> selected="selected" <?php endif; ?> value="300">300 <?php echo e(trans('admin.characters'), false); ?></option>
            						  	<option <?php if( $settings->comment_length == 400 ): ?> selected="selected" <?php endif; ?> value="400">400 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 500 ): ?> selected="selected" <?php endif; ?> value="500">500 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 700 ): ?> selected="selected" <?php endif; ?> value="700">700 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 1000 ): ?> selected="selected" <?php endif; ?> value="1000">1000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 2000 ): ?> selected="selected" <?php endif; ?> value="2000">2000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 3000 ): ?> selected="selected" <?php endif; ?> value="3000">3000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 4000 ): ?> selected="selected" <?php endif; ?> value="4000">4000 <?php echo e(trans('admin.characters'), false); ?></option>
                            <option <?php if( $settings->comment_length == 5000 ): ?> selected="selected" <?php endif; ?> value="5000">5000 <?php echo e(trans('admin.characters'), false); ?></option>
                          </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group margin-zero">
                     <label class="col-sm-2 control-label"><?php echo e(trans('admin.number_posts_show'), false); ?></label>
                     <div class="col-sm-10">
                       <select name="number_posts_show" class="form-control">
                           <option <?php if( $settings->number_posts_show == 5 ): ?> selected="selected" <?php endif; ?> value="5">5</option>
                           <option <?php if( $settings->number_posts_show == 10 ): ?> selected="selected" <?php endif; ?> value="10">10</option>
                           <option <?php if( $settings->number_posts_show == 15 ): ?> selected="selected" <?php endif; ?> value="15">15</option>
                           <option <?php if( $settings->number_posts_show == 20 ): ?> selected="selected" <?php endif; ?> value="20">20</option>
                           <option <?php if( $settings->number_posts_show == 30 ): ?> selected="selected" <?php endif; ?> value="30">30</option>
                         </select>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                <div class="box-body">
                  <div class="form-group margin-zero">
                    <label class="col-sm-2 control-label"><?php echo e(trans('admin.number_comments_show'), false); ?></label>
                    <div class="col-sm-10">
                      <select name="number_comments_show" class="form-control">
                        <option <?php if( $settings->number_comments_show == 1 ): ?> selected="selected" <?php endif; ?> value="1">1</option>
                          <option <?php if( $settings->number_comments_show == 2 ): ?> selected="selected" <?php endif; ?> value="2">2</option>
                            <option <?php if( $settings->number_comments_show == 3 ): ?> selected="selected" <?php endif; ?> value="3">3</option>
                              <option <?php if( $settings->number_comments_show == 4 ): ?> selected="selected" <?php endif; ?> value="4">4</option>
                              <option <?php if( $settings->number_comments_show == 5 ): ?> selected="selected" <?php endif; ?> value="5">5</option>
                              <option <?php if( $settings->number_comments_show == 10 ): ?> selected="selected" <?php endif; ?> value="10">10</option>
                              <option <?php if( $settings->number_comments_show == 15 ): ?> selected="selected" <?php endif; ?> value="15">15</option>
                              <option <?php if( $settings->number_comments_show == 20 ): ?> selected="selected" <?php endif; ?> value="20">20</option>
                              <option <?php if( $settings->number_comments_show == 30 ): ?> selected="selected" <?php endif; ?> value="30">30</option>
                        </select>
                    </div>
                  </div>
                </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
        		</div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/limits.blade.php ENDPATH**/ ?>