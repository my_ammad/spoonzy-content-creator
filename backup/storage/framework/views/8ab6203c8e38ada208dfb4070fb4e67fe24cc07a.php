

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.payment_settings'), false); ?>

          </h4>

        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><strong><?php echo e(trans('admin.payment_settings'), false); ?></strong></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/payments'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <!-- Start Box Body -->
                <div class="box-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(trans('admin.currency_code'), false); ?></label>
                    <div class="col-sm-10">
                      <input type="text" value="<?php echo e($settings->currency_code, false); ?>" name="currency_code" class="form-control" placeholder="<?php echo e(trans('admin.currency_code'), false); ?>">
                    </div>
                  </div>
                </div><!-- /.box-body -->

                <div class="box-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(trans('admin.currency_symbol'), false); ?></label>
                    <div class="col-sm-10">
                      <input type="text" value="<?php echo e($settings->currency_symbol, false); ?>" name="currency_symbol" class="form-control" placeholder="<?php echo e(trans('admin.currency_symbol'), false); ?>">
                      <p class="help-block"><?php echo e(trans('admin.notice_currency'), false); ?></p>
                    </div>
                  </div>
                </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.fee_commission'), false); ?></label>
                      <div class="col-sm-10">
                      	<select name="fee_commission" class="form-control">
                          <?php for($i=0; $i <= 50; ++$i): ?>
                            <option <?php if( $settings->fee_commission == $i ): ?> selected="selected" <?php endif; ?> value="<?php echo e($i, false); ?>"><?php echo e($i, false); ?>%</option>
                            <?php endfor; ?>
                            </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.min_subscription_amount'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->min_subscription_amount, false); ?>" name="min_subscription_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('admin.min_subscription_amount'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.max_subscription_amount'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->max_subscription_amount, false); ?>" name="max_subscription_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('admin.max_subscription_amount'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.min_tip_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->min_tip_amount, false); ?>" name="min_tip_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.min_tip_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.max_tip_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->max_tip_amount, false); ?>" name="max_tip_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.max_tip_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.min_ppv_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->min_ppv_amount, false); ?>" name="min_ppv_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.min_ppv_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.max_ppv_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->max_ppv_amount, false); ?>" name="max_ppv_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.max_ppv_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.min_deposits_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->min_deposits_amount, false); ?>" name="min_deposits_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.min_deposits_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.max_deposits_amount'), false); ?></label>
                     <div class="col-sm-10">
                       <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->max_deposits_amount, false); ?>" name="max_deposits_amount" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.max_deposits_amount'), false); ?>">
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.amount_min_withdrawal'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="number" min="1" autocomplete="off" value="<?php echo e($settings->amount_min_withdrawal, false); ?>" name="amount_min_withdrawal" class="form-control onlyNumber" placeholder="<?php echo e(trans('general.amount_min_withdrawal'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('admin.currency_position'), false); ?></label>
                     <div class="col-sm-10">
                       <select name="currency_position" class="form-control">
                         <option <?php if( $settings->currency_position == 'left' ): ?> selected="selected" <?php endif; ?> value="left"><?php echo e($settings->currency_symbol, false); ?>99 - <?php echo e(trans('admin.left'), false); ?></option>
                         <option <?php if( $settings->currency_position == 'right' ): ?> selected="selected" <?php endif; ?> value="right">99<?php echo e($settings->currency_symbol, false); ?> <?php echo e(trans('admin.right'), false); ?></option>
                         </select>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                <div class="box-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(trans('general.decimal_format'), false); ?></label>
                    <div class="col-sm-10">
                      <select name="decimal_format" class="form-control input-lg">
                        <option <?php if( $settings->decimal_format == 'dot' ): ?> selected="selected" <?php endif; ?> value="dot">1,989.95</option>
                        <option <?php if( $settings->decimal_format == 'comma' ): ?> selected="selected" <?php endif; ?> value="comma">1.989,95</option>
                        </select>
                    </div>
                  </div>
                </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                <div class="box-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label"><?php echo e(trans('admin.days_process_withdrawals'), false); ?></label>
                    <div class="col-sm-10">
                      <select name="days_process_withdrawals" class="form-control">
                        <?php for($i=1; $i <= 30; ++$i): ?>
                          <option <?php if( $settings->days_process_withdrawals == $i ): ?> selected="selected" <?php endif; ?> value="<?php echo e($i, false); ?>"><?php echo e($i, false); ?> (<?php echo e(trans_choice('general.days', $i), false); ?>)</option>
                          <?php endfor; ?>
                          </select>
                    </div>
                  </div>
                </div><!-- /.box-body -->

                <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label"><?php echo e(trans('users.payout_method'), false); ?> (PayPal)</label>
                   <div class="col-sm-10">
                     <select name="payout_method_paypal" class="form-control">
                         <option <?php if( $settings->payout_method_paypal == 'on' ): ?> selected="selected" <?php endif; ?> value="on"><?php echo e(trans('general.enabled'), false); ?></option>
                           <option <?php if( $settings->payout_method_paypal == 'off' ): ?> selected="selected" <?php endif; ?> value="off"><?php echo e(trans('general.disabled'), false); ?></option>
                         </select>
                         <p class="help-block"><?php echo e(trans('general.payout_method_desc'), false); ?></p>
                   </div>
                 </div>
               </div><!-- /.box-body -->

               <!-- Start Box Body -->
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo e(trans('users.payout_method'), false); ?> (<?php echo e(trans('general.bank'), false); ?>)</label>
                  <div class="col-sm-10">
                    <select name="payout_method_bank" class="form-control">
                        <option <?php if( $settings->payout_method_bank == 'on' ): ?> selected="selected" <?php endif; ?> value="on"><?php echo e(trans('general.enabled'), false); ?></option>
                          <option <?php if( $settings->payout_method_bank == 'off' ): ?> selected="selected" <?php endif; ?> value="off"><?php echo e(trans('general.disabled'), false); ?></option>
                        </select>
                        <p class="help-block"><?php echo e(trans('general.payout_method_desc'), false); ?></p>
                  </div>
                </div>
              </div><!-- /.box-body -->
              
              <!-- Start Box Body -->
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><?php echo e(trans('users.payout_method'), false); ?> (Stripe)</label>
                  <div class="col-sm-10">
                    <select name="payout_method_stripe" class="form-control">
                        <option <?php if( $settings->payout_method_stripe == 'on' ): ?> selected="selected" <?php endif; ?> value="on"><?php echo e(trans('general.enabled'), false); ?></option>
                          <option <?php if( $settings->payout_method_stripe == 'off' ): ?> selected="selected" <?php endif; ?> value="off"><?php echo e(trans('general.disabled'), false); ?></option>
                        </select>
                        <p class="help-block"><?php echo e(trans('general.payout_method_desc'), false); ?></p>
                  </div>
                </div>
              </div><!-- /.box-body -->

               <div class="box-footer">
                 <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
               </div><!-- /.box-footer -->
               </form>

              </div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/payments-settings.blade.php ENDPATH**/ ?>