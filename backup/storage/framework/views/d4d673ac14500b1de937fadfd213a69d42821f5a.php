<?php $__env->startSection('title'); ?> <?php echo e(trans('users.payout_method'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="bi bi-credit-card mr-2"></i> <?php echo e(trans('users.payout_method'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('general.default_payout_method'), false); ?>:
            <?php if(Auth::user()->payment_gateway != ''): ?> <strong class="text-success"><?php echo e(Auth::user()->payment_gateway == 'PayPal' ? 'PayPal' : trans('users.bank_transfer'), false); ?></strong>
            <?php else: ?> <strong class="text-danger"><?php echo e(trans('general.none'), false); ?></strong> <?php endif; ?>
            </p>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">
          <?php if(session('status')): ?>
                  <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>
                    <?php echo e(session('status'), false); ?>

                  </div>
                <?php endif; ?>
                
            <?php if(session('error_msg')): ?>
                  <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>
                    <?php echo e(session('error_msg'), false); ?>

                  </div>
                <?php endif; ?>
                
                <?php if(session('info')): ?>
                  <div class="alert alert-info">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                			<span aria-hidden="true">×</span>
                			</button>
                    <?php echo e(session('info'), false); ?>

                  </div>
                <?php endif; ?>

          <?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <?php if(auth()->user()->verified_id != 'yes'): ?>
      <div class="alert alert-danger mb-3">
               <ul class="list-unstyled m-0">
                 <li><i class="fa fa-exclamation-triangle"></i> <?php echo e(trans('general.verified_account_info'), false); ?> <a href="<?php echo e(url('settings/verify/account'), false); ?>" class="text-white link-border"><?php echo e(trans('general.verify_account'), false); ?></a></li>
               </ul>
             </div>
             <?php endif; ?>

      <?php if(auth()->user()->verified_id == 'yes'): ?>
          <div class="row justify-content-center">

            <?php

            // PayPal
            $buttonPayPal = null;
            $formPayPal = null;

            // Bank
            $buttonBank = null;
            $formBank = null;
            
            // Stripe
            $formStripe = null;
            $buttonStripe=null;

            if ($errors->has('stripe_details')) {

                // Stripe
                $buttonStripe = ' active';
                $formStripe = ' active show';

                // PayPal
                $buttonPayPal = null;
                $formPayPal = null;
                
                // Bank
                $buttonBank = null;
                $formBank = null;


            }
            if ($errors->has('bank_details')) {

                // Bank
                $buttonBank = ' active';
                $formBank = ' active show';

                // PayPal
                $buttonPayPal = null;
                $formPayPal = null;
                
                // Stripe
                $buttonStripe =null;
                $formStripe = null;


            }

            if ($errors->has('email_paypal') || $errors->has('email_paypal_confirmation')) {

              // PayPal
              $buttonPayPal = ' active';
              $formPayPal = ' active show';

              // Bank
              $buttonBank = null;
              $formBank = null;
              
                // Stripe
                $buttonStripe =null;
                $formStripe = null;
            }

            ?>

            <div class="col-md-12">
              <div class="nav-wrapper">
                <ul class="nav nav-pills nav-fill flex-md-row" role="tablist">
                  <?php if( $settings->payout_method_paypal == 'on' ): ?>
                  <li class="nav-item">
                    <a class="nav-link link-nav mb-sm-4 mb-md-0 mb-2 p-4<?php echo e($buttonPayPal, false); ?>" id="btnPayPal" data-toggle="tab" href="#formPayPal" role="tab" aria-controls="formPayPal" aria-selected="true">
                      <i class="fab fa-paypal mr-2"></i> PayPal
                      <?php if(auth()->user()->payment_gateway == 'PayPal'): ?> <span class="badge badge-pill badge-success"><?php echo e(__('general.default'), false); ?></span> <?php endif; ?>
                    </a>
                  </li>
                <?php endif; ?>

                  <?php if( $settings->payout_method_bank == 'on' ): ?>
                  <li class="nav-item">
                    <a class="nav-link link-nav mb-sm-4 mb-md-0 p-4<?php echo e($buttonBank, false); ?>" id="btnBank" data-toggle="tab" href="#formBank" role="tab" aria-controls="formBank" aria-selected="false">
                      <i class="fa fa-university mr-2"></i> <?php echo e(trans('users.bank_transfer'), false); ?>

                      <?php if(auth()->user()->payment_gateway == 'Bank'): ?> <span class="badge badge-pill badge-success"><?php echo e(__('general.default'), false); ?></span> <?php endif; ?>
                    </a>
                  </li>
                <?php endif; ?>
                <?php if( $settings->payout_method_stripe == 'on' ): ?>
                  <li class="nav-item">
                    <a class="nav-link link-nav mb-sm-4 mb-md-0 p-4<?php echo e($buttonStripe, false); ?>" id="btnStripe" data-toggle="tab" href="#formStripe" role="tab" aria-controls="formBank" aria-selected="false">
                      <i class="fa fa-university mr-2"></i> Stripe
                      <?php if(auth()->user()->payment_gateway == 'Stripe'): ?> <span class="badge badge-pill badge-success"><?php echo e(__('general.default'), false); ?></span> <?php endif; ?>
                    </a>
                  </li>
                <?php endif; ?>
                </ul>
              </div><!-- END COL-MD-12 -->
            </div><!-- ./ ROW -->
          </div><!-- ./ nav-wrapper -->

        <div class="tab-content">

          <?php if( $settings->payout_method_paypal == 'on' ): ?>
          <!-- FORM PAYPAL -->
          <div id="formPayPal" class="tab-pane fade<?php echo e($formPayPal, false); ?>" role="tabpanel">
          <form method="POST" action="<?php echo e(url('settings/payout/method/paypal'), false); ?>">
            <?php echo csrf_field(); ?>

            <div class="form-group">
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fab fa-paypal"></i></span>
                  </div>
                  <input class="form-control" name="email_paypal" value="<?php echo e(Auth::user()->paypal_account == '' ? old('email_paypal') : Auth::user()->paypal_account, false); ?>" placeholder="<?php echo e(trans('general.email_paypal'), false); ?>" required type="email">
                </div>
              </div>

              <div class="form-group">
                  <div class="input-group mb-4">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-envelope"></i></span>
                    </div>
                    <input class="form-control" name="email_paypal_confirmation" placeholder="<?php echo e(trans('general.confirm_email_paypal'), false); ?>" required type="email">
                  </div>
                </div>
                <button class="btn btn-1 btn-success btn-block" type="submit"><?php echo e(trans('general.save_payout_method'), false); ?></button>
          </form>
        </div>
      <?php endif; ?>

      <?php if( $settings->payout_method_bank == 'on' ): ?>
          <!-- FORM BANK TRANSFER -->
          <div id="formBank" class="tab-pane fade<?php echo e($formBank, false); ?>" role="tabpanel">
              
            <div class="mt-3 col-md-8 offset-md-2">
                <?php 
                  $type = ['CHECKING', 'SAVINGS'];
                  $name = "";
                  $routing_no = "";
                  $ac_no = "";
                  $ac_type = "";
                  $country = "";
                  $city = "";
                  $address = "";
                  $post_code = "";
                  if(isset($wise)){
                      $name = $wise->name;
                      $routing_no = $wise->routing_no;
                      $ac_no = $wise->ac_no;
                      $ac_type = $wise->ac_type;
                      $country = $wise->country;
                      $city = $wise->city;
                      $address = $wise->address;
                      $post_code = $wise->post_code;
                  }
                ?>
                <form action="<?php echo e(url('settings/payments/wise'), false); ?>" method="POST">
                    <?php echo csrf_field(); ?>
                    <div class="form-group">
                        <label>Account Holder Name</label> 
                        <input class="form-control" required name="name" value="<?php echo e($name, false); ?>">
                    </div>
                    <div class="form-group">
                        <label>ACH Routing number</label> 
                        <input class="form-control" required name="abartn" value="<?php echo e($routing_no, false); ?>">
                    </div>
                    <div class="form-group">
                        <label>Bank account number</label>
                        <input class="form-control" name="ac_no" value="<?php echo e($ac_no, false); ?>" required>
                    </div>
                    <div class="form-group">
                        <label>Acount Type</label>
                        <select class="form-control" name="ac_type">
                            <option value="">Please Select</option>
                            <?php 
                              foreach($type as $type){
                                $selected = "";
                                if($ac_type == $type){
                                   $selected = "selected"; 
                                }
                                echo "<option $selected>$type</option>";   
                              }
                            ?>
                        </select>
                    </div>
                    <h5>Address</h5>
                    <div class="row">    
                    <div class="form-group col-md-6">
                        <label>Country</label>
                        <select class="form-control" name="country">
                            <option value="">Please Select</option>
                            <option value="US" <?php if($country): ?> selected <?php endif; ?>>United State</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>City</label>
                        <input class="form-control" name="city" value="<?php echo e($city, false); ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Post code</label>
                        <input class="form-control" name="post_code" value="<?php echo e($post_code, false); ?>" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Address</label>
                        <input class="form-control" name="address" value="<?php echo e($address, false); ?>" required>
                    </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success btn-sm">Submit</button>
                    </div>
                </form>
            </div>      
          <!--     
          <form method="POST"  action="<?php echo e(url('settings/payout/method/bank'), false); ?>">

            <?php echo csrf_field(); ?>
              <div class="form-group">
                <textarea name="bank_details" rows="5" cols="40" class="form-control" required placeholder="<?php echo e(trans('users.bank_details'), false); ?>"><?php echo e(Auth::user()->bank == '' ? old('bank_details') : Auth::user()->bank, false); ?></textarea>
                </div>
                <button class="btn btn-1 btn-success btn-block" type="submit"><?php echo e(trans('general.save_payout_method'), false); ?></button>
          </form>
          !-->
          </div>
        <?php endif; ?>
        
        <?php if( $settings->payout_method_stripe == 'on' ): ?>
          <!-- FORM BANK TRANSFER -->
          <div id="formStripe" class="tab-pane fade<?php echo e($formStripe, false); ?>" role="tabpanel">
          <form method="POST"  action="<?php echo e(url('settings/payout/method-stripe'), false); ?>">

            <?php echo csrf_field(); ?>
              <div class="form-group">
                <button class="btn btn-1 btn-success btn-block" type="submit"><?php echo e((!empty(auth()->user()->stripe_account_id))?'Change Stripe Account':'Connect Stripe', false); ?></button>
          </form>
          </div>
        <?php endif; ?>


        </div><!-- ./ TAB-CONTENT -->
      <?php endif; ?>

        </div><!-- end col-md-6 -->

      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/payout_method.blade.php ENDPATH**/ ?>