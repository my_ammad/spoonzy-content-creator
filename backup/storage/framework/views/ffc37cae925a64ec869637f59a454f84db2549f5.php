

<?php $__env->startSection('content'); ?>


    <section class="section section-sm">
        <div class="container pt-5">
            <div class="row">

                <?php if(auth()->user()->payPerView()->count() != 0): ?>
                    <div class="col-md-12">
                        <ul class="list-inline">
                            <li class="list-inline-item text-uppercase h5">
                                <a href="<?php echo e(url('/'), false); ?>"
                                   class="text-decoration-none <?php if(request()->is('/')): ?> link-border <?php else: ?> text-muted  <?php endif; ?>"><?php echo e(__('admin.home'), false); ?></a>
                            </li>
                            <li class="list-inline-item text-uppercase h5">
                                <a href="<?php echo e(url('my/purchases'), false); ?>"
                                   class="text-decoration-none <?php if(request()->is('my/purchases')): ?> link-border <?php else: ?> text-muted <?php endif; ?>"><?php echo e(__('general.purchased'), false); ?></a>
                            </li>
                        </ul>
                    </div>
                <?php endif; ?>

                <div class="col-md-8 second wrap-post">

                    <?php if(auth()->user()->verified_id == 'yes'): ?>
                        <?php echo $__env->make('includes.form-post', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <?php endif; ?>

                    <?php if($updates->total() != 0): ?>

                        <?php
                            $counterPosts = ($updates->total() - $settings->number_posts_show);
                        ?>

                        <div class="grid-updates position-relative" id="updatesPaginator">
                            <?php echo $__env->make('includes.updates', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>

                    <?php else: ?>
                        <div class="grid-updates position-relative" id="updatesPaginator"></div>

                        <div class="my-5 text-center no-updates">
          <span class="btn-block mb-3">
            <i class="fa fa-photo-video ico-no-result"></i>
          </span>
                            <h4 class="font-weight-light"><?php echo e(trans('general.no_posts_posted'), false); ?></h4>
                        </div>

                    <?php endif; ?>
                </div><!-- end col-md-12 -->

                <div class="col-md-4 mb-4 first">

                    <?php if($users->total() == 0): ?>
                        <div class="panel panel-default panel-transparent mb-4 d-lg-block d-none">
                            <div class="panel-body">
                                <div class="media none-overflow">
                                    <div class="d-flex my-2 align-items-center">
                                        <img class="rounded-circle mr-2"
                                             src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>"
                                             width="60" height="60">

                                        <div class="d-block">
                                            <strong><?php echo e(auth()->user()->name, false); ?></strong>


                                            <div class="d-block">
                                                <small class="media-heading text-muted btn-block margin-zero">
                                                    <a href="<?php echo e(url('settings/page'), false); ?>">
                                                        <?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?>

                                                        <small class="pl-1"><i
                                                                class="fa fa-long-arrow-alt-right"></i></small>
                                                    </a>
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(auth()->user()->role == 'admin'): ?>
                        <?php if($users->total() != 0): ?>
                            <button type="button" class="btn btn-primary btn-block mb-2 d-lg-none" type="button"
                                    data-toggle="collapse" data-target="#navbarUserHome" aria-controls="navbarCollapse"
                                    aria-expanded="false">
                                <i class="far	fa-compass mr-1"></i> <?php echo e(trans('general.explore_creators'), false); ?>

                            </button>
                        <?php endif; ?>
                    <?php endif; ?>

                    <div class="navbar-collapse collapse d-lg-block sticky-top" id="navbarUserHome">

                        <?php if($users->total() != 0): ?>
                            <?php echo $__env->make('includes.explore_creators', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php endif; ?>

                        <div class="d-lg-block d-none">
                            <?php echo $__env->make('includes.footer-tiny', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        </div>

                    </div><!-- navbarUserHome -->

                </div><!-- col-md -->

            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>

    <?php if(session('noty_error')): ?>
        <script type="text/javascript">
            swal({
                title: "<?php echo e(trans('general.error_oops'), false); ?>",
                text: "<?php echo e(trans('general.already_sent_report'), false); ?>",
                type: "error",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        </script>
    <?php endif; ?>

    <?php if(session('noty_success')): ?>
        <script type="text/javascript">
            swal({
                title: "<?php echo e(trans('general.thanks'), false); ?>",
                text: "<?php echo e(trans('general.reported_success'), false); ?>",
                type: "success",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        </script>
    <?php endif; ?>

    <?php if(session('success_verify')): ?>
        <script type="text/javascript">
            swal({
                title: "<?php echo e(trans('general.welcome'), false); ?>",
                text: "<?php echo e(trans('users.account_validated'), false); ?>",
                type: "success",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        </script>
    <?php endif; ?>

    <?php if(session('error_verify')): ?>
        <script type="text/javascript">
            swal({
                title: "<?php echo e(trans('general.error_oops'), false); ?>",
                text: "<?php echo e(trans('users.code_not_valid'), false); ?>",
                type: "error",
                confirmButtonText: "<?php echo e(trans('users.ok'), false); ?>"
            });
        </script>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/index/home-session.blade.php ENDPATH**/ ?>