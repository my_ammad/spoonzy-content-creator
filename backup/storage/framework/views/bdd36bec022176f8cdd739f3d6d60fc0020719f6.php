

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.storage'), false); ?>

          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(session('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(session('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo e(trans('admin.storage'), false); ?></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/storage'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

          <!-- Start Box Body -->
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label">App URL</label>
               <div class="col-sm-10">
                 <input type="text" value="<?php echo e(env('APP_URL'), false); ?>" name="APP_URL" class="form-control" placeholder="App URL">
                 <p class="help-block margin-bottom-zero"><?php echo e(trans('admin.notice_app_url'), false); ?> <strong><?php echo e(url('/'), false); ?></strong></p>
               </div>
             </div>
           </div><!-- /.box-body -->

              <!-- Start Box Body -->
               <div class="box-body">
                 <div class="form-group">
                   <label class="col-sm-2 control-label"><?php echo e(trans('admin.disk'), false); ?></label>
                   <div class="col-sm-10">
                     <select name="FILESYSTEM_DRIVER" class="form-control custom-select">
                       <option <?php if(env('FILESYSTEM_DRIVER') == 'default'): ?> selected <?php endif; ?> value="default"><?php echo e(trans('admin.disk_local'), false); ?></option>
                       <option <?php if(env('FILESYSTEM_DRIVER') == 's3'): ?> selected <?php endif; ?> value="s3">Amazon S3</option>
                       <option <?php if(env('FILESYSTEM_DRIVER') == 'dospace'): ?> selected <?php endif; ?> value="dospace">DigitalOcean</option>
                       <option <?php if(env('FILESYSTEM_DRIVER') == 'wasabi'): ?> selected <?php endif; ?> value="wasabi">Wasabi</option>
                       <option <?php if(env('FILESYSTEM_DRIVER') == 'backblaze'): ?> selected <?php endif; ?> value="backblaze">Backblaze B2</option>
                       <option <?php if(env('FILESYSTEM_DRIVER') == 'vultr'): ?> selected <?php endif; ?> value="vultr">Vultr</option>
                     </select>
                   </div>
                 </div>
               </div><!-- /.box-body -->

               <hr/>

                 <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Amazon Key</label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e(env('AWS_ACCESS_KEY_ID'), false); ?>" name="AWS_ACCESS_KEY_ID" class="form-control" placeholder="Amazon Key">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                   <div class="box-body">
                     <div class="form-group">
                       <label class="col-sm-2 control-label">Amazon Secret</label>
                       <div class="col-sm-10">
                         <input type="text" value="<?php echo e(env('AWS_SECRET_ACCESS_KEY'), false); ?>" name="AWS_SECRET_ACCESS_KEY" class="form-control" placeholder="Amazon Secret">
                       </div>
                     </div>
                   </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Amazon Region</label>
                        <div class="col-sm-10">
                          <input type="text" value="<?php echo e(env('AWS_DEFAULT_REGION'), false); ?>" name="AWS_DEFAULT_REGION" class="form-control" placeholder="Amazon Region">
                        </div>
                      </div>
                    </div><!-- /.box-body -->

                    <!-- Start Box Body -->
                     <div class="box-body">
                       <div class="form-group">
                         <label class="col-sm-2 control-label">Amazon Bucket</label>
                         <div class="col-sm-10">
                           <input type="text" value="<?php echo e(env('AWS_BUCKET'), false); ?>" name="AWS_BUCKET" class="form-control" placeholder="Amazon Bucket">
                         </div>
                       </div>
                     </div><!-- /.box-body -->

                    <hr />

                    <!-- Start Box Body -->
                     <div class="box-body">
                       <div class="form-group">
                         <label class="col-sm-2 control-label">DigitalOcean Key</label>
                         <div class="col-sm-10">
                           <input type="text" value="<?php echo e(env('DOS_ACCESS_KEY_ID'), false); ?>" name="DOS_ACCESS_KEY_ID" class="form-control" placeholder="DigitalOcean Key">
                         </div>
                       </div>
                     </div><!-- /.box-body -->

                     <!-- Start Box Body -->
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">DigitalOcean Secret</label>
                          <div class="col-sm-10">
                            <input type="text" value="<?php echo e(env('DOS_SECRET_ACCESS_KEY'), false); ?>" name="DOS_SECRET_ACCESS_KEY" class="form-control" placeholder="DigitalOcean Secret">
                          </div>
                        </div>
                      </div><!-- /.box-body -->

                      <!-- Start Box Body -->
                       <div class="box-body">
                         <div class="form-group">
                           <label class="col-sm-2 control-label">DigitalOcean Region</label>
                           <div class="col-sm-10">
                             <input type="text" value="<?php echo e(env('DOS_DEFAULT_REGION'), false); ?>" name="DOS_DEFAULT_REGION" class="form-control" placeholder="DigitalOcean Region">
                           </div>
                         </div>
                       </div><!-- /.box-body -->

                       <!-- Start Box Body -->
                        <div class="box-body">
                          <div class="form-group">
                            <label class="col-sm-2 control-label">DigitalOcean Bucket</label>
                            <div class="col-sm-10">
                              <input type="text" value="<?php echo e(env('DOS_BUCKET'), false); ?>" name="DOS_BUCKET" class="form-control" placeholder="DigitalOcean Bucket">
                            </div>
                          </div>
                        </div><!-- /.box-body -->

                        <hr />

                        <!-- Start Box Body -->
                         <div class="box-body">
                           <div class="form-group">
                             <label class="col-sm-2 control-label">Wasabi Key</label>
                             <div class="col-sm-10">
                               <input type="text" value="<?php echo e(env('WAS_ACCESS_KEY_ID'), false); ?>" name="WAS_ACCESS_KEY_ID" class="form-control" placeholder="Wasabi Key">
                               <p class="help-block margin-bottom-zero"><strong>Important:</strong> Wasabi in trial mode does not allow public files, you must send an email to <strong>support@wasabi.com</strong> to enable public files, or avoid trial mode.</p>
                             </div>
                           </div>
                         </div><!-- /.box-body -->

                         <!-- Start Box Body -->
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Wasabi Secret</label>
                              <div class="col-sm-10">
                                <input type="text" value="<?php echo e(env('WAS_SECRET_ACCESS_KEY'), false); ?>" name="WAS_SECRET_ACCESS_KEY" class="form-control" placeholder="Wasabi Secret">
                              </div>
                            </div>
                          </div><!-- /.box-body -->

                          <!-- Start Box Body -->
                           <div class="box-body">
                             <div class="form-group">
                               <label class="col-sm-2 control-label">Wasabi Region</label>
                               <div class="col-sm-10">
                                 <input type="text" value="<?php echo e(env('WAS_DEFAULT_REGION'), false); ?>" name="WAS_DEFAULT_REGION" class="form-control" placeholder="Wasabi Region">
                               </div>
                             </div>
                           </div><!-- /.box-body -->

                           <!-- Start Box Body -->
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Wasabi Bucket</label>
                                <div class="col-sm-10">
                                  <input type="text" value="<?php echo e(env('WAS_BUCKET'), false); ?>" name="WAS_BUCKET" class="form-control" placeholder="Wasabi Bucket">
                                </div>
                              </div>
                            </div><!-- /.box-body -->

                  <hr />

                  <!-- Start Box Body -->
                   <div class="box-body">
                     <div class="form-group">
                       <label class="col-sm-2 control-label">Backblaze Account ID</label>
                       <div class="col-sm-10">
                         <input type="text" value="<?php echo e(env('BACKBLAZE_ACCOUNT_ID'), false); ?>" name="BACKBLAZE_ACCOUNT_ID" class="form-control" placeholder="Backblaze Account ID">
                       </div>
                     </div>
                   </div><!-- /.box-body -->

                   <!-- Start Box Body -->
                    <div class="box-body">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Backblaze Master Application Key</label>
                        <div class="col-sm-10">
                          <input type="text" value="<?php echo e(env('BACKBLAZE_APP_KEY'), false); ?>" name="BACKBLAZE_APP_KEY" class="form-control" placeholder="Backblaze Master Application Key">
                        </div>
                      </div>
                    </div><!-- /.box-body -->

                    <!-- Start Box Body -->
                     <div class="box-body">
                       <div class="form-group">
                         <label class="col-sm-2 control-label">Backblaze Bucket Name</label>
                         <div class="col-sm-10">
                           <input type="text" value="<?php echo e(env('BACKBLAZE_BUCKET'), false); ?>" name="BACKBLAZE_BUCKET" class="form-control" placeholder="Backblaze Bucket Name">
                         </div>
                       </div>
                     </div><!-- /.box-body -->

                     <!-- Start Box Body -->
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Backblaze Bucket ID</label>
                          <div class="col-sm-10">
                            <input type="text" value="<?php echo e(env('BACKBLAZE_BUCKET_ID'), false); ?>" name="BACKBLAZE_BUCKET_ID" class="form-control" placeholder="Backblaze Bucket ID">
                          </div>
                        </div>
                      </div><!-- /.box-body -->

                     <!-- Start Box Body -->
                      <div class="box-body">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Backblaze Bucket Endpoint</label>
                          <div class="col-sm-10">
                            <input type="text" value="<?php echo e(env('BACKBLAZE_BUCKET_REGION'), false); ?>" name="BACKBLAZE_BUCKET_REGION" class="form-control" placeholder="s3.us-west-000.backblazeb2.com">
                          </div>
                        </div>
                      </div><!-- /.box-body -->

                      <hr/>

                        <!-- Start Box Body -->
                         <div class="box-body">
                           <div class="form-group">
                             <label class="col-sm-2 control-label">Vultr Key</label>
                             <div class="col-sm-10">
                               <input type="text" value="<?php echo e(env('VULTR_ACCESS_KEY'), false); ?>" name="VULTR_ACCESS_KEY" class="form-control" placeholder="Vultr Key">
                             </div>
                           </div>
                         </div><!-- /.box-body -->

                         <!-- Start Box Body -->
                          <div class="box-body">
                            <div class="form-group">
                              <label class="col-sm-2 control-label">Vultr Secret</label>
                              <div class="col-sm-10">
                                <input type="text" value="<?php echo e(env('VULTR_SECRET_KEY'), false); ?>" name="VULTR_SECRET_KEY" class="form-control" placeholder="Vultr Secret">
                              </div>
                            </div>
                          </div><!-- /.box-body -->

                          <!-- Start Box Body -->
                           <div class="box-body">
                             <div class="form-group">
                               <label class="col-sm-2 control-label">Vultr Region</label>
                               <div class="col-sm-10">
                                 <input type="text" value="<?php echo e(env('VULTR_REGION'), false); ?>" name="VULTR_REGION" class="form-control" placeholder="Vultr Region">
                               </div>
                             </div>
                           </div><!-- /.box-body -->

                           <!-- Start Box Body -->
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-2 control-label">Vultr Bucket</label>
                                <div class="col-sm-10">
                                  <input type="text" value="<?php echo e(env('VULTR_BUCKET'), false); ?>" name="VULTR_BUCKET" class="form-control" placeholder="Vultr Bucket">
                                </div>
                              </div>
                            </div><!-- /.box-body -->


                  <div class="box-footer">
                    <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
        		</div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/storage.blade.php ENDPATH**/ ?>