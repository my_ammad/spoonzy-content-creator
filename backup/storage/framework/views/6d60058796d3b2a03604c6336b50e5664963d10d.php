<?php $__env->startSection('title'); ?><?php echo e(trans('general.messages'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
        <?php if(session('success_message')): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check margin-separator"></i> <?php echo e(session('success_message'), false); ?>

            </div>
        <?php endif; ?>

      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="feather icon-send mr-2"></i> <?php echo e(trans('general.messages'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('general.messages_subtitle'), false); ?></p>
          <?php if($messages->count() != 0): ?>
          <button class="btn btn-primary btn-sm w-small-100" data-toggle="modal" data-target="#newMessageForm">
            <i class="fa fa-plus"></i> <?php echo e(trans('general.new_message'), false); ?>

          </button>
          <button class="btn btn-info btn-sm w-small-100 mt-2 mt-lg-0" data-toggle="modal" data-target="#multipleMessageForm">
             <i class="fa fa-plus-square"></i> Message Subscribers
          </button>
        <?php endif; ?>
        </div>
      </div>
      <div class="row">

        <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <div class="col-md-6 col-lg-9 mb-5 mb-lg-0" id="messagesContainer">

    <?php if($messages->count() != 0): ?>

      <?php echo $__env->make('includes.messages-inbox', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php else: ?>

      <div class="my-5 text-center no-updates">
        <span class="btn-block mb-3">
          <i class="fa fa-comment-slash ico-no-result"></i>
        </span>
      <h4 class="font-weight-light"><?php echo e(trans('general.no_messages'), false); ?></h4>

      <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newMessageForm">
        <i class="fa fa-plus"></i> <?php echo e(trans('general.new_message'), false); ?>

      </button>

      <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#multipleMessageForm">
          <i class="fa fa-plus-square"></i> Multiple Message
      </button>

      </div>
    <?php endif; ?>
    </div><!-- end col-md-6 -->

      </div>
    </div>
  </section>


   <div class="modal fade" id="multipleMessageForm" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-0">
                <div class="card bg-white shadow border-0">

                    <div class="card-body px-lg-3 py-lg-3">

                        <div class="mb-2">
                            <h5 class="position-relative" style="border-bottom: 1px solid;padding-bottom: 7px;">Multiple Message
                                <small data-dismiss="modal" class="btn-cancel-msg"><?php echo e(trans('admin.cancel'), false); ?></small>
                            </h5>

                        </div>

                        <div class="text-center">
                            <button class="btn btn-sm btn-primary mb-3" id="sendMultipleMsg">Send Message to the selected users <i class="fa fa-envelope"></i>
                            </button>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped m-0">
                                <thead>
                                <tr>
                                    <th width="10px"><input style="zoom:1.5" type="checkbox" id="master"></th>
                                    <th scope="col"><?php echo e(trans('general.subscriber'), false); ?></th>
                                    <th scope="col"><?php echo e(trans('admin.status'), false); ?></th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php $__currentLoopData = $subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><input style="zoom:1.5" type="checkbox" class="sub_chk"
                                                   data-id="<?php echo e($subscription->user()->id, false); ?>">
                                        </td>
                                        <td>
                                            <a href="<?php echo e(url($subscription->user()->username), false); ?>">
                                                <img src="<?php echo e(Helper::getFile(config('path.avatar').$subscription->user()->avatar), false); ?>" width="40" height="40" class="rounded-circle mr-2">

                                                <?php echo e($subscription->user()->hide_name == 'yes' ? $subscription->user()->username : $subscription->user()->name, false); ?>

                                            </a>
                                        </td>
                                        <td>
                                            <?php if($subscription->stripe_id == ''
                                              && strtotime($subscription->ends_at) > strtotime(now()->format('Y-m-d H:i:s'))
                                              && $subscription->cancelled == 'no'
                                                || $subscription->stripe_id != '' && $subscription->stripe_status == 'active'
                                                || $subscription->stripe_id == '' && $subscription->free == 'yes'
                                              ): ?>
                                                <span class="badge badge-pill badge-success text-uppercase"><?php echo e(trans('general.active'), false); ?></span>
                                            <?php elseif($subscription->stripe_id != '' && $subscription->stripe_status == 'incomplete'): ?>
                                                <span class="badge badge-pill badge-warning text-uppercase"><?php echo e(trans('general.incomplete'), false); ?></span>
                                            <?php else: ?>
                                                <span class="badge badge-pill badge-danger text-uppercase"><?php echo e(trans('general.cancelled'), false); ?></span>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- End Modal new Message -->

  <div class="modal fade" id="newMessageForm" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-body p-0">
          <div class="card bg-white shadow border-0">

            <div class="card-body px-lg-5 py-lg-5">

              <div class="mb-2">
                <h5 class="position-relative"><?php echo e(trans('general.new_message'), false); ?>

                  <small data-dismiss="modal" class="btn-cancel-msg"><?php echo e(trans('admin.cancel'), false); ?></small>
                </h5>

              </div>

              <div class="position-relative">
                <span class="my-sm-0 btn-new-msg">
                  <i class="fa fa-search"></i>
                </span>

                <input class="form-control input-new-msg rounded mb-2" id="searchCreator" type="text" name="q" autocomplete="off" placeholder="<?php echo e(trans('general.find_user'), false); ?>" aria-label="Search">
              </div>

              <div class="w-100 text-center mt-3 display-none" id="spinner">
                <span class="spinner-border align-middle text-primary"></span>
              </div>

              <div id="containerUsers" class="text-center"></div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div><!-- End Modal new Message -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#master').on('click', function (e) {
                if ($(this).is(':checked', true)) {
                    $(".sub_chk").prop('checked', true);
                } else {
                    $(".sub_chk").prop('checked', false);
                }
            });

            $('#sendMultipleMsg').on('click', function (e) {
                e.preventDefault();

                var allVals = [];
                $(".sub_chk:checked").each(function () {
                    allVals.push($(this).attr('data-id'));
                });
                var ids = allVals.join("+");

                if(ids == ''){
                    alert('Warning! \nYou didn\'t select any users');
                    return;
                }

                window.location = "<?php echo e(url('/'), false); ?>/mutipleMsgBox/" + ids;
            });
        });
    </script>
    <script src="<?php echo e(asset('public/js/paginator-messages.js'), false); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/users/messages.blade.php ENDPATH**/ ?>