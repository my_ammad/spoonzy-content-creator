

<?php $__env->startSection('title'); ?> <?php echo e($title, false); ?> -<?php $__env->stopSection(); ?>

    <?php $__env->startSection('description_custom'); ?><?php echo e($description ? $description : trans('seo.description'), false); ?><?php $__env->stopSection(); ?>
    <?php $__env->startSection('keywords_custom'); ?><?php echo e($keywords ? $keywords.',' : null, false); ?><?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-12 py-5">
          <h2 class="mb-0 font-montserrat">
            <img src="<?php echo e(url('public/img-category', $image), false); ?>" class="mr-2 rounded" width="30" /> <?php echo e($title, false); ?></h2>
          <p class="lead text-muted mt-0">(<?php echo e($users->total(), false); ?>) <?php echo e(trans_choice('users.creators_in_this_category',$users->total() ), false); ?></p>
        </div>
      </div>

      	<div class="btn-block mb-3 text-right">
      		<span>
      			<?php echo e(trans('general.filter_by'), false); ?>


      			<select class="ml-2 custom-select w-auto" id="filter">
      					<option <?php if(request()->is('category', $slug)): ?> selected <?php endif; ?> value="<?php echo e(url('category', $slug), false); ?>"><?php echo e(trans('general.featured_creators'), false); ?></option>
      					<option <?php if(request()->is('category/'.$slug.'/new')): ?> selected <?php endif; ?> value="<?php echo e(url('category/'.$slug.'','new'), false); ?>"><?php echo e(trans('general.new_creators'), false); ?></option>
                  <option <?php if(request()->is('category/'.$slug.'/free')): ?> selected <?php endif; ?> value="<?php echo e(url('category/'.$slug.'','free'), false); ?>"><?php echo e(trans('general.free_subscription'), false); ?></option>
      				</select>
      		</span>
      	</div>

      <?php echo $__env->make('includes.listing-categories', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <div class="row">
        <?php if($users->total() != 0): ?>

          <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $response): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-md-4 mb-4">
            <?php echo $__env->make('includes.listing-creators', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
          </div><!-- end col-md-4 -->
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          <?php if($users->lastPage() > 1): ?>
            <div class="w-100 d-block">
              <?php echo e($users->links(), false); ?>

            </div>
          <?php endif; ?>

        <?php else: ?>
          <div class="col-md-12">
            <div class="my-5 text-center no-updates">
              <span class="btn-block mb-3">
                <i class="fa fa-user-slash ico-no-result"></i>
              </span>
            <h4 class="font-weight-light"><?php echo e(trans('general.not_found_creators_category'), false); ?></h4>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/index/categories.blade.php ENDPATH**/ ?>