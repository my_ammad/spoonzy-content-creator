<?php $__env->startSection('title'); ?> <?php echo e(trans('admin.dashboard'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-8 py-5">
          <h2 class="mb-0 font-montserrat"><i class="bi bi-speedometer2 mr-2"></i> <?php echo e(trans('admin.dashboard'), false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('general.dashboard_desc'), false); ?></p>
        </div>
      </div>
      <div class="row">

        <div class="col-lg-12 mb-5 mb-lg-0">

          <div class="content">
            <div class="row">
              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h4><i class="fas fa-hand-holding-usd mr-2 text-primary"></i> <?php echo e(Helper::amountFormatDecimal($earningNetUser), false); ?></h4>
                    <small><?php echo e(trans('admin.earnings_net'), false); ?></small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h4><i class="fas fa-wallet mr-2 text-primary"></i> <?php echo e(Helper::amountFormatDecimal(Auth::user()->balance), false); ?></h4>
                    <small><?php echo e(trans('general.balance'), false); ?>

                      <?php if(Auth::user()->balance >= $settings->amount_min_withdrawal): ?>
                      <a href="<?php echo e(url('settings/withdrawals'), false); ?>" class="link-border"> <?php echo e(trans('general.make_withdrawal'), false); ?></a>
                    <?php endif; ?>
                    </small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h4><i class="fas fa-users mr-2 text-primary"></i> <span title="<?php echo e($subscriptionsActive, false); ?>"><?php echo e(Helper::formatNumber($subscriptionsActive), false); ?></span></h4>
                    <small><?php echo e(trans('general.subscriptions_active'), false); ?></small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h6 class="<?php echo e($stat_revenue_today > 0 ? 'text-success' : 'text-danger', false); ?>">
                      <?php echo e(Helper::amountFormatDecimal($stat_revenue_today), false); ?>

                        <?php echo Helper::PercentageIncreaseDecrease($stat_revenue_today, $stat_revenue_yesterday); ?>

                    </h6>
                    <small><?php echo e(trans('general.revenue_today'), false); ?></small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h6 class="<?php echo e($stat_revenue_week > 0 ? 'text-success' : 'text-danger', false); ?>">
                      <?php echo e(Helper::amountFormatDecimal($stat_revenue_week), false); ?>

                        <?php echo Helper::PercentageIncreaseDecrease($stat_revenue_week, $stat_revenue_last_week); ?>

                    </h6>
                    <small><?php echo e(trans('general.revenue_week'), false); ?></small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-4 mb-2">
                <div class="card">
                  <div class="card-body">
                    <h6 class="<?php echo e($stat_revenue_month > 0 ? 'text-success' : 'text-danger', false); ?>">
                      <?php echo e(Helper::amountFormatDecimal($stat_revenue_month), false); ?>

                        <?php echo Helper::PercentageIncreaseDecrease($stat_revenue_month, $stat_revenue_last_month); ?>

                    </h6>
                    <small><?php echo e(trans('general.revenue_month'), false); ?></small>
                  </div>
                </div><!-- card 1 -->
              </div><!-- col-lg-4 -->

              <div class="col-lg-12 mt-3 py-4">
                <h4 class="mb-4"><?php echo e(trans('general.earnings_this_month'), false); ?> (<?php echo e($month, false); ?>)</h4>
                <canvas id="Chart"></canvas>
              </div>
            </div><!-- end row -->
          </div><!-- end content -->

        </div><!-- end col-md-6 -->

      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
  <script src="<?php echo e(asset('public/js/Chart.min.js'), false); ?>"></script>

  <script type="text/javascript">

function decimalFormat(nStr)
{
  <?php if($settings->decimal_format == 'dot'): ?>
	 $decimalDot = '.';
	 $decimalComma = ',';
	 <?php else: ?>
	 $decimalDot = ',';
	 $decimalComma = '.';
	 <?php endif; ?>

   <?php if($settings->currency_position == 'left'): ?>
   currency_symbol_left = '<?php echo e($settings->currency_symbol, false); ?>';
   currency_symbol_right = '';
   <?php else: ?>
   currency_symbol_right = '<?php echo e($settings->currency_symbol, false); ?>';
   currency_symbol_left = '';
   <?php endif; ?>

    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? $decimalDot + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + $decimalComma + '$2');
    }
    return currency_symbol_left + x1 + x2 + currency_symbol_right;
  }

  function transparentize(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}

  var init = document.getElementById("Chart").getContext('2d');
  var ChartArea = new Chart(init, {
      type: 'line',
      data: {
          labels: [<?php echo $label; ?>],
          datasets: [{
              label: '<?php echo e(trans('general.earnings'), false); ?> ',
              backgroundColor: transparentize('<?php echo e($settings->color_default, false); ?>'),
              borderColor: '<?php echo e($settings->color_default, false); ?>',
              data: [<?php echo $data; ?>],
              borderWidth: 2,
              fill: true,
              lineTension: 0.4,
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      min: 0, // it is for ignoring negative step.
                      beginAtZero: true,
                      callback: function(value, index, values) {
                          return '<?php if($settings->currency_position == 'left'): ?><?php echo e($settings->currency_symbol, false); ?><?php endif; ?>' + value + '<?php if($settings->currency_position == 'right'): ?><?php echo e($settings->currency_symbol, false); ?><?php endif; ?>';
                      }
                  }
              }]
          },
          tooltips: {
              callbacks: {
                  label: function(t, d) {
                      var xLabel = d.datasets[t.datasetIndex].label;
                      var yLabel = decimalFormat(t.yLabel);
                      return xLabel + ': ' + yLabel;
                  }
              }
          },
          legend: {
              display: false
          }
      }
  });
  </script>
  <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/dashboard.blade.php ENDPATH**/ ?>