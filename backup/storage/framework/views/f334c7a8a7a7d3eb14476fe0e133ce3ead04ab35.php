<div class="col-md-6 col-lg-3 mb-3">

<button type="button" class="btn btn-primary btn-block mb-2 d-lg-none" type="button" data-toggle="collapse" data-target="#navbarUserHome" aria-controls="navbarCollapse" aria-expanded="false">
		<i class="fa fa-bars myicon-right"></i> <?php echo e(trans('general.menu'), false); ?>

	</button>

	<div class="navbar-collapse collapse d-lg-block" id="navbarUserHome">
	<div class="card shadow-sm card-settings">
			<div class="list-group list-group-sm list-group-flush">

					<a href="<?php echo e(url(Auth::user()->username), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between url-user">
							<div>
									<i class="feather icon-user mr-2"></i>
									<span><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.my_page') : trans('users.my_profile'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<?php if(auth()->user()->verified_id == 'yes'): ?>
					<a href="<?php echo e(url('dashboard'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('dashboard')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-speedometer2 mr-2"></i>
									<span><?php echo e(trans('admin.dashboard'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
				<?php endif; ?>
					<a href="<?php echo e(url('settings/page'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/page')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-pencil mr-2"></i>
									<span><?php echo e(auth()->user()->verified_id == 'yes' ? trans('general.edit_my_page') : trans('users.edit_profile'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<a href="<?php echo e(url('privacy/security'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('privacy/security')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-shield-check mr-2"></i>
									<span><?php echo e(trans('general.privacy_security'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<?php if(auth()->user()->verified_id == 'yes'): ?>
					<a href="<?php echo e(url('settings/subscription'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/subscription')): ?> active <?php endif; ?>">
							<div>
									<i class="feather icon-refresh-cw mr-2"></i>
									<span><?php echo e(trans('general.subscription'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
				<?php endif; ?>

				<?php if($settings->disable_wallet == 'off'): ?>
					<a href="<?php echo e(url('my/wallet'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('my/wallet')): ?> active <?php endif; ?>">
							<div>
									<i class="iconmoon icon-Wallet mr-2"></i>
									<span><?php echo e(trans('general.wallet'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
				<?php endif; ?>

				<?php if(Helper::showSectionMyCards()): ?>
					<a href="<?php echo e(url('my/cards'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('my/cards')): ?> active <?php endif; ?>">
							<div>
									<i class="feather icon-credit-card mr-2"></i>
									<span><?php echo e(trans('general.my_cards'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<?php endif; ?>
					<a href="<?php echo e(url('settings/verify/account'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/verify/account')): ?> active <?php endif; ?>">
							<div>
									<i class="feather icon-check-circle mr-2"></i>
									<span><?php echo e(trans('general.verify_account'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<a href="<?php echo e(url('settings/password'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/password')): ?> active <?php endif; ?>">
							<div>
									<i class="iconmoon icon-Key mr-2"></i>
									<span><?php echo e(trans('auth.password'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<?php if(auth()->user()->verified_id == 'yes'): ?>
					<a href="<?php echo e(url('my/subscribers'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('my/subscribers')): ?> active <?php endif; ?>">
							<div>
									<i class="feather icon-users mr-2"></i>
									<span><?php echo e(trans('users.my_subscribers'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
				<?php endif; ?>

					<a href="<?php echo e(url('my/subscriptions'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('my/subscriptions')): ?> active <?php endif; ?>">
							<div>
									<i class="feather icon-user-check mr-2"></i>
									<span><?php echo e(trans('users.my_subscriptions'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>

					<a href="<?php echo e(url('my/payments'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('my/payments') || request()->is('my/payments/received')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-receipt mr-2"></i>
									<span><?php echo e(trans('general.payments'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
					<?php if(auth()->user()->verified_id == 'yes'): ?>
					<a href="<?php echo e(url('settings/payout/method'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/payout/method')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-credit-card mr-2"></i>
									<span><?php echo e(trans('users.payout_method'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>

					<a href="<?php echo e(url('settings/withdrawals'), false); ?>" class="list-group-item list-group-item-action d-flex justify-content-between <?php if(request()->is('settings/withdrawals')): ?> active <?php endif; ?>">
							<div>
									<i class="bi bi-arrow-left-right mr-2"></i>
									<span><?php echo e(trans('general.withdrawals'), false); ?></span>
							</div>
							<div>
									<i class="feather icon-chevron-right"></i>
							</div>
					</a>
				<?php endif; ?>
			</div>
	</div>
</div><!-- End Card -->
</div><!-- navbarUserHome -->
<?php /**PATH /home/allajwno/public_html/resources/views/includes/cards-settings.blade.php ENDPATH**/ ?>