<div class="mb-2">
  <h6 class="mb-3 text-muted font-weight-light">
    <?php echo e(trans('general.explore_creators'), false); ?>


<?php if(auth()->guard()->check()): ?>
  <?php if($users->total() > 3): ?>
    <a href="javascript:void(0);" class="float-right refresh_creators">
      <i class="fa fa-sync mr-2"></i>
    </a>
    <?php endif; ?>

  <?php else: ?>
    <a href="<?php echo e(url('creators'), false); ?>" class="float-right"><?php echo e(__('general.view_all'), false); ?> <small class="pl-1"><i class="fa fa-long-arrow-alt-right"></i></small></a>
<?php endif; ?>
  </h6>

  <ul class="list-group">

    <div id="containerRefreshCreators">
      <?php echo $__env->make('includes.listing-explore-creators', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div><!-- containerRefreshCreators -->
  </ul>
</div><!-- d-lg-none -->
<?php /**PATH /home/allajwno/public_html/resources/views/includes/explore_creators.blade.php ENDPATH**/ ?>