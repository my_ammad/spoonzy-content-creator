<?php $__env->startSection('title'); ?><?php echo e(trans('general.notifications'), false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <section class="section section-sm">
        <div class="container">
            <div class="row justify-content-center text-center mb-sm">
                <div class="col-lg-8 py-5">
                    <h2 class="mb-0 font-montserrat">
                        <i class="far fa-bell mr-2"></i> <?php echo e(trans('general.notifications'), false); ?>


                        <small class="font-tiny">
                            <a href="javascript:;" data-toggle="modal" data-target="#notifications"><i
                                    class="fa fa-cog mr-2"></i></a>

                            <?php if(count($notifications) != 0): ?>
                                <?php echo Form::open([
                                              'method' => 'POST',
                                              'url' => "notifications/delete",
                                              'class' => 'd-inline'
                                          ]); ?>


                                <?php echo Form::button('<i class="fa fa-trash-alt"></i>', ['class' => 'btn btn-lg  align-baseline p-0 e-none btn-link actionDeleteNotify']); ?>

                                <?php echo Form::close(); ?>

                            <?php endif; ?>
                        </small>
                    </h2>
                    <p class="lead text-muted mt-0"><?php echo e(trans('general.notifications_subtitle'), false); ?></p>
                </div>
            </div>
            <div class="row">

                <?php echo $__env->make('includes.cards-settings', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

                    <?php if(session('status')): ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>

                            <?php echo e(session('status'), false); ?>

                        </div>
                    <?php endif; ?>

                    <?php

                    foreach ($notifications as $key) {

                    switch ($key->type) {
                        case 1:
                            $action = trans('users.has_subscribed');
                            $linkDestination = false;
                            break;
                        case 2:
                            $action = trans('users.like_you');
                            $linkDestination = url($key->usernameAuthor, 'post') . '/' . $key->id;
                            $text_post = Str::limit($key->description, 50, '...');
                            break;
                        case 3:
                            $action = trans('users.comment_you');
                            $linkDestination = url($key->usernameAuthor, 'post') . '/' . $key->id;
                            $text_post = Str::limit($key->description, 50, '...');
                            break;

                        case 5:
                            $action = trans('general.he_sent_you_tip');
                            $linkDestination = url('my/payments/received');
                            $text_post = trans('general.tip');
                            break;

                        case 6:
                            $action = trans('general.has_bought_your_message');
                            $linkDestination = url('messages', $key->userId);
                            $text_post = Str::limit($key->message, 50, '...');
                            break;

                        case 7:
                            $action = trans('general.has_bought_your_content');
                            $linkDestination = url($key->usernameAuthor, 'post') . '/' . $key->id;
                            $text_post = Str::limit($key->description, 50, '...');
                            break;

                        case 8:
                            $user = \App\Models\User::find($key->target);
                            if (!$user){
                                $author = '';
                            }else{
                                $user->name ? $name=$user->name : $name = $user->username;
                                $author = " of (author:" . $name .")";
                            }

                            $action = "tried to take screenshot $author from ";
                            $linkDestination = $key->info;
                            $text_post = Str::limit($key->info, 50, '...');
                            break;
                        case 9:
                            // $user = \App\Models\User::find($key->author);
                            $action = "";
                            $linkDestination = url('settings/withdrawals');
                            $text_post = Str::limit($key->info, 50, '...');
                            break;
                    }

                    ?>

                    <div class="card mb-3 card-updates">
                        <div class="card-body">
                            <div class="media">
        		<span class="rounded-circle mr-3">
        			<a href="<?php echo e(url($key->username), false); ?>">
        				<img src="<?php echo e(Helper::getFile(config('path.avatar').$key->avatar), false); ?>" class="rounded-circle"
                             width="60" height="60">
        				</a>
        		</span>
                                <div class="media-body">
                                    <h6 class="mb-0 font-montserrat">
                                        <a href="<?php echo e(url($key->username), false); ?>">
                                            <?php echo e($key->hide_name == 'yes' ? $key->username : $key->name, false); ?>

                                        </a> <?php echo e($action, false); ?> <?php if( $linkDestination != false ): ?> <a
                                            href="<?php echo e(url($linkDestination), false); ?>"><?php echo e($text_post, false); ?></a> <?php endif; ?>
                                    </h6>
                                    <small class="timeAgo text-muted"
                                           data="<?php echo e(date('c', strtotime($key->created_at)), false); ?>"></small>
                                </div><!-- media body -->
                            </div><!-- media -->
                        </div><!-- card body -->
                    </div>

                    <?php } //foreach ?>

                    <?php if(count($notifications) == 0): ?>

                        <div class="my-5 text-center">
        <span class="btn-block mb-3">
          <i class="far fa-bell-slash ico-no-result"></i>
        </span>
                            <h4 class="font-weight-light"><?php echo e(trans('general.no_notifications'), false); ?></h4>
                        </div>
                    <?php endif; ?>

                    <?php if($notifications->hasPages()): ?>
                        <?php echo e($notifications->links(), false); ?>

                    <?php endif; ?>

                </div><!-- end col-md-6 -->

            </div>
        </div>
    </section>

    <div class="modal fade" id="notifications" tabindex="-1" role="dialog" aria-labelledby="modal-form"
         aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-dialog-scrollable modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="card bg-white shadow border-0">

                        <div class="card-body px-lg-5 py-lg-5">

                            <div class="mb-3">
                                <h6 class="position-relative"><?php echo e(trans('general.receive_notifications_when'), false); ?>

                                    <small data-dismiss="modal" class="btn-cancel-msg"><i
                                            class="fa fa-times"></i></small>
                                </h6>
                            </div>

                            <form method="POST" action="<?php echo e(url('notifications/settings'), false); ?>" id="form">

                                <?php echo csrf_field(); ?>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="notify_new_subscriber"
                                           value="yes" <?php if(auth()->user()->notify_new_subscriber == 'yes'): ?> checked
                                           <?php endif; ?> id="customSwitch1">
                                    <label class="custom-control-label switch"
                                           for="customSwitch1"><?php echo e(trans('general.someone_subscribed_content'), false); ?></label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="notify_liked_post"
                                           value="yes" <?php if(auth()->user()->notify_liked_post == 'yes'): ?> checked
                                           <?php endif; ?> id="customSwitch2">
                                    <label class="custom-control-label switch"
                                           for="customSwitch2"><?php echo e(trans('general.someone_liked_post'), false); ?></label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="notify_commented_post"
                                           value="yes" <?php if(auth()->user()->notify_commented_post == 'yes'): ?> checked
                                           <?php endif; ?> id="customSwitch3">
                                    <label class="custom-control-label switch"
                                           for="customSwitch3"><?php echo e(trans('general.someone_commented_post'), false); ?></label>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="notify_new_tip"
                                           value="yes" <?php if(auth()->user()->notify_new_tip == 'yes'): ?> checked
                                           <?php endif; ?> id="customSwitch5">
                                    <label class="custom-control-label switch"
                                           for="customSwitch5"><?php echo e(trans('general.someone_sent_tip'), false); ?></label>
                                </div>

                                <div class="mt-3">
                                    <h6 class="position-relative"><?php echo e(trans('general.email_notification'), false); ?>

                                    </h6>
                                </div>

                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="email_new_subscriber"
                                           value="yes" <?php if(auth()->user()->email_new_subscriber == 'yes'): ?> checked
                                           <?php endif; ?> id="customSwitch4">
                                    <label class="custom-control-label switch"
                                           for="customSwitch4"><?php echo e(trans('general.someone_subscribed_content'), false); ?></label>
                                </div>

                                <button type="submit" id="save" data-msg-success="<?php echo e(trans('admin.success_update'), false); ?>"
                                        class="btn btn-primary btn-sm mt-3 w-100" data-msg="<?php echo e(trans('admin.save'), false); ?>">
                                    <?php echo e(trans('admin.save'), false); ?>

                                </button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- End Modal new Message -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/users/notifications.blade.php ENDPATH**/ ?>