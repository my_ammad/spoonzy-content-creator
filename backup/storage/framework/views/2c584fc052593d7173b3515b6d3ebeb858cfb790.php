<?php $__env->startSection('content'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>
                <?php echo e(trans('admin.admin'), false); ?> <i
                    class="fa fa-angle-right margin-separator"></i> Post Inspections (<?php echo e($data->total(), false); ?>)
            </h4>

        </section>

        <!-- Main content -->
        <section class="content">

            <?php if(Session::has('info_message')): ?>
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="fa fa-warning margin-separator"></i> <?php echo e(Session::get('info_message'), false); ?>

                </div>
            <?php endif; ?>

            <?php if(Session::has('success_message')): ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

                </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                Post Inspections
                            </h3>

                        </div><!-- /.box-header -->

                        <div class="box-body table-responsive">

                            <table class="table table-hover table-striped table-bordered">
                                <tbody>

                                <?php if( $data->total() !=  0 && $data->count() != 0 ): ?>
                                    <tr>
                                        <th class="active">#</th>

                                        <th class="active">Details</th>
                                        <th class="active">Author</th>
                                        <th class="active">Date</th>
                                    </tr>

                                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->iteration, false); ?></td>









                                            <td style="font-weight:600">The <a target="_blank" href="<?php echo e($item->media_link, false); ?>"><?php echo e($item->media_type, false); ?></a> from the
                                                <a target="_blank" href="<?php echo e($item->post, false); ?>">post</a> is viewed by
                                                <a target="_blank" href="<?php echo e(url($item->username), false); ?>"><?php echo e($item->user_fullname, false); ?></a>
                                            </td>
                                            <td>
                                                <a href="<?php echo e(url($item->post_author_username), false); ?>"><?php echo e($item->post_author_fullname, false); ?></a>
                                            </td>
                                            <td><?php echo e($item->created_at, false); ?></td>
                                        </tr><!-- /.TR -->
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <?php else: ?>
                                    <hr/>
                                    <h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>

                                    <?php if( isset( $query ) ): ?>
                                        <div class="col-md-12 text-center padding-bottom-15">
                                            <a href="<?php echo e(url('panel/admin/post_inspection'), false); ?>"
                                               class="btn btn-sm btn-danger"><?php echo e(trans('auth.back'), false); ?></a>
                                        </div>

                                    <?php endif; ?>
                                <?php endif; ?>

                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <?php if($data->hasPages()): ?>
                        <?php echo e($data->links(), false); ?>

                    <?php endif; ?>
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/naim/post_inspection.blade.php ENDPATH**/ ?>