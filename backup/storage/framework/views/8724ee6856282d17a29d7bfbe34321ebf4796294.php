	<div class="card card-updates h-100">
	<div class="card-cover" style="background: <?php if($response->cover != ''): ?> url(<?php echo e(Helper::getFile(config('path.cover').$response->cover), false); ?>)  <?php endif; ?> #505050 center center; background-size: cover;">
		<?php if($response->free_subscription == 'yes'): ?>
		<span class="badge-free px-2 py-1 text-uppercase position-absolute rounded"><?php echo e(__('general.free'), false); ?></span>
	<?php endif; ?>
	</div>
	<div class="card-avatar">
		<a href="<?php echo e(url($response->username), false); ?>">
		<img src="<?php echo e(Helper::getFile(config('path.avatar').$response->avatar), false); ?>" width="95" height="95" alt="<?php echo e($response->name, false); ?>" class="img-user-small">
		</a>
	</div>
	<div class="card-body text-center">
			<h6 class="card-title pt-4">
				<?php echo e($response->hide_name == 'yes' ? $response->username : $response->name, false); ?>


				<?php if($response->verified_id == 'yes'): ?>
					<small class="verified mr-1" title="<?php echo e(trans('general.verified_account'), false); ?>"data-toggle="tooltip" data-placement="top">
						<i class="feather icon-check-circle"></i>
					</small>
				<?php endif; ?>

				<?php if($response->featured == 'yes'): ?>
				<small class="text-featured" title="<?php echo e(trans('users.creator_featured'), false); ?>" data-toggle="tooltip" data-placement="top">
					<i class="fas fa fa-award"></i>
				</small>
			<?php endif; ?>
			</h6>
			<small class="text-muted">
				<?php if($response->profession != ''): ?>
				<?php echo e($response->profession, false); ?>


				<?php elseif(isset($response->country()->country_name) && $response->profession == ''): ?>
						<i class="fa fa-map-marker-alt mr-1"></i>	<?php echo e($response->country()->country_name, false); ?>


				<?php endif; ?>
			</small>

			<ul class="list-inline m-0">
				<li class="list-inline-item small"><i class="feather icon-file-text"></i> <?php echo e(Helper::formatNumber($response->updates()->count()), false); ?></li>
				<li class="list-inline-item small"><i class="feather icon-image"></i> <?php echo e(Helper::formatNumber($response->updates()->where('image', '<>', '')->count()), false); ?></li>
				<li class="list-inline-item small"><i class="feather icon-video"></i> <?php echo e(Helper::formatNumber($response->updates()->where('video', '<>', '')->orWhere('video_embed', '<>', '')->whereUserId($response->id)->count()), false); ?></li>
				<li class="list-inline-item small"><i class="feather icon-mic"></i> <?php echo e(Helper::formatNumber($response->updates()->where('music', '<>', '')->count()), false); ?></li>
				<li class="list-inline-item small"><i class="far fa-file-archive"></i> <?php echo e(Helper::formatNumber($response->updates()->where('file', '<>', '')->count()), false); ?></li>
			</ul>

			<p class="m-0 py-3 text-muted card-text">
				<?php echo e(Str::limit($response->story, 100, '...'), false); ?>

			</p>
			<a href="<?php echo e(url($response->username), false); ?>" class="btn btn-1 btn-sm btn-outline-primary"><?php echo e(trans('general.go_to_page'), false); ?></a>
	</div>
</div><!-- End Card -->
<?php /**PATH /home/allajwno/public_html/backup/resources/views/includes/listing-creators.blade.php ENDPATH**/ ?>