

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
           <?php echo e(trans('admin.admin'), false); ?> <i class="fa fa-angle-right margin-separator"></i> <?php echo e(trans('admin.transactions'), false); ?> (<?php echo e($data->total(), false); ?>)
          </h4>

        </section>

        <!-- Main content -->
        <section class="content">
          <?php if(session('success_message')): ?>
      		    <div class="alert alert-success">
      		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      								<span aria-hidden="true">×</span>
      								</button>
      		       <i class="fa fa-check margin-separator"></i>  <?php echo e(session('success_message'), false); ?>

      		    </div>
      		<?php endif; ?>

        	<div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">
                  		<?php echo e(trans('admin.transactions'), false); ?>

                  	</h3>

                  <div class="box-tools">
                   <?php if( $data->total() !=  0 ): ?>
                      <!-- form -->
                      <form role="search" autocomplete="off" action="<?php echo e(url('panel/admin/transactions'), false); ?>" method="get">
  	                 <div class="input-group input-group-sm w-150">
  	                  <input type="text" name="q" class="form-control pull-right" placeholder="<?php echo e(trans('admin.transaction_id'), false); ?>">

  	                  <div class="input-group-btn">
  	                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
  	                  </div>
  	                </div>
                  </form><!-- form -->
                  <?php endif; ?>
                </div>

                </div><!-- /.box-header -->

                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
               <tbody>

               	<?php if($data->total() !=  0 && $data->count() != 0): ?>
                   <tr>
                      <th class="active">ID</th>
                      <th class="active"><?php echo e(trans('admin.transaction_id'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.user'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.type'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.amount'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.earnings_admin'), false); ?></th>
                      <th class="active"><?php echo e(trans('general.payment_gateway'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.date'), false); ?></th>
                      <th class="active"><?php echo e(trans('admin.status'), false); ?></th>
                    </tr><!-- /.TR -->

                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $transaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr>
                      <td><?php echo e(str_pad($transaction->id, 4, "0", STR_PAD_LEFT), false); ?></td>
                      <td><?php echo e($transaction->txn_id, false); ?></td>
                      <td>
                        <?php if( ! isset($transaction->user()->username)): ?>
                          <?php echo e(trans('general.no_available'), false); ?>

                        <?php else: ?>
                          <a href="<?php echo e(url($transaction->user()->username), false); ?>" target="_blank">
                          <?php echo e($transaction->user()->name, false); ?> <i class="fa fa-external-link-square"></i>
                        </a>
                        <?php endif; ?>
                    </td>
                      <td><?php echo e(__('general.'.$transaction->type), false); ?>

                      </td>
                      <td><?php echo e(Helper::amountFormatDecimal($transaction->amount), false); ?></td>
                      <td><?php echo e(Helper::amountFormatDecimal($transaction->earning_net_admin), false); ?></td>
                      <td><?php echo e($transaction->payment_gateway, false); ?></td>
                      <td><?php echo e(Helper::formatDate($transaction->created_at), false); ?></td>
                      <td>
                        <?php if($transaction->approved == '0'): ?>
                        <span class="label label-warning"><?php echo e(trans('admin.pending'), false); ?></span>
                      <?php elseif($transaction->approved == '1'): ?>
                        <span class="label label-success"><?php echo e(trans('admin.approved'), false); ?></span>
                      <?php else: ?>
                        <span class="label label-danger"><?php echo e(trans('general.canceled'), false); ?></span>
                      <?php endif; ?>

                    <?php if($transaction->approved == '1'): ?>
                          <?php echo Form::open([
        			            'method' => 'POST',
        			            'url' => url('panel/admin/transactions/cancel', $transaction->id),
        			            'class' => 'displayInline'
  				              ]); ?>

  	                   <?php echo Form::submit(trans('admin.cancel'), ['class' => 'btn btn-danger btn-xs padding-btn cancel_payment']); ?>


  	        	           <?php echo Form::close(); ?>

                       <?php endif; ?>
                       </td>
                    </tr><!-- /.TR -->
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php else: ?>
                    <hr />
                    	<h3 class="text-center no-found"><?php echo e(trans('general.no_results_found'), false); ?></h3>

                    <?php endif; ?>

                  </tbody>

                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php if($data->hasPages()): ?>
             <?php echo e($data->links(), false); ?>

             <?php endif; ?>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/admin/transactions.blade.php ENDPATH**/ ?>