<div class="menuMobile w-100 bg-white shadow-lg p-3 border-top">
    <ul class="list-inline d-flex bd-highlight m-0 text-center">

        <li class="list-inline-item flex-fill bd-highlight">
            <a class="p-3 btn-mobile" href="<?php echo e(url('/'), false); ?>" title="<?php echo e(trans('admin.home'), false); ?>">
                <i class="feather icon-home icon-navbar"></i>
            </a>
        </li>

        <?php if(auth()->user()->role == 'admin'): ?>
            <li class="list-inline-item flex-fill bd-highlight">
                <a class="p-3 btn-mobile" href="<?php echo e(url('creators'), false); ?>" title="<?php echo e(trans('general.explore'), false); ?>">
                    <i class="far	fa-compass icon-navbar"></i>
                </a>
            </li>
        <?php endif; ?>

        <li class="list-inline-item flex-fill bd-highlight">
            <a href="<?php echo e(url('messages'), false); ?>" class="p-3 btn-mobile position-relative"
               title="<?php echo e(trans('general.messages'), false); ?>">

					<span class="noti_msg notify <?php if(auth()->user()->messagesInbox() != 0): ?> d-block <?php endif; ?>">
						<?php echo e(auth()->user()->messagesInbox(), false); ?>

						</span>

                <i class="feather icon-send icon-navbar"></i>
            </a>
        </li>

        <li class="list-inline-item flex-fill bd-highlight">
            <a href="<?php echo e(url('notifications'), false); ?>" class="p-3 btn-mobile position-relative"
               title="<?php echo e(trans('general.notifications'), false); ?>">

					<span
                        class="noti_notifications notify <?php if(auth()->user()->notifications()->where('status', '0')->count()): ?> d-block <?php endif; ?>">
						<?php echo e(auth()->user()->notifications()->where('status', '0')->count(), false); ?>

						</span>

                <i class="far fa-bell icon-navbar"></i>
            </a>
        </li>

        <li class="list-inline-item flex-fill bd-highlight">
            <a class="p-3 btn-mobile navbar-toggler-mobile" href="#" data-toggle="collapse"
               data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" role="button">
                <i class="far fa-user-circle icon-navbar"></i>
            </a>
        </li>
    </ul>
</div>
<?php /**PATH /home/allajwno/public_html/resources/views/includes/menu-mobile.blade.php ENDPATH**/ ?>