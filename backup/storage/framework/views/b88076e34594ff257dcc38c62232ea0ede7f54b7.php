

<?php $__env->startSection('title'); ?> <?php echo e($title, false); ?> -<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <section class="section section-sm">
    <div class="container">
      <div class="row justify-content-center text-center mb-sm">
        <div class="col-lg-12 py-5">
          <h2 class="mb-0 text-break"><?php echo e($title, false); ?></h2>
          <p class="lead text-muted mt-0"><?php echo e(trans('users.the_best_creators_is_here'), false); ?>

            <?php if(auth()->guard()->guest()): ?>
              <?php if($settings->registration_active == '1'): ?>
                <a href="<?php echo e(url('signup'), false); ?>" class="link-border"><?php echo e(trans('general.join_now'), false); ?></a>
              <?php endif; ?>
          <?php endif; ?></p>
        </div>
      </div>

      <?php if(! request()->get('q')): ?>
      	<div class="btn-block mb-3 text-right">
      		<span>
      			<?php echo e(trans('general.filter_by'), false); ?>


      			<select class="ml-2 custom-select w-auto" id="filter">
      					<option <?php if(request()->is('creators')): ?> selected <?php endif; ?> value="<?php echo e(url('creators'), false); ?>"><?php echo e(trans('general.featured_creators'), false); ?></option>
      					<option <?php if(request()->is('creators/new')): ?> selected <?php endif; ?> value="<?php echo e(url('creators/new'), false); ?>"><?php echo e(trans('general.new_creators'), false); ?></option>
                  <option <?php if(request()->is('creators/free')): ?> selected <?php endif; ?> value="<?php echo e(url('creators/free'), false); ?>"><?php echo e(trans('general.free_subscription'), false); ?></option>
      				</select>
      		</span>
      	</div>
      <?php endif; ?>

      <?php echo $__env->make('includes.listing-categories', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <div class="row">
        <?php if( $users->total() != 0 ): ?>

          <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $response): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-md-4 mb-4">
            <?php echo $__env->make('includes.listing-creators', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
          </div><!-- end col-md-4 -->
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          <?php if($users->hasPages()): ?>
            <div class="w-100 d-block">
              <?php echo e($users->appends(['q' => request('q')])->links(), false); ?>

            </div>
          <?php endif; ?>

        <?php else: ?>
          <div class="col-md-12">
            <div class="my-5 text-center no-updates">
              <span class="btn-block mb-3">
                <i class="fa fa-user-slash ico-no-result"></i>
              </span>
            <h4 class="font-weight-light"><?php echo e(trans('general.no_results_found'), false); ?></h4>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/resources/views/index/creators.blade.php ENDPATH**/ ?>