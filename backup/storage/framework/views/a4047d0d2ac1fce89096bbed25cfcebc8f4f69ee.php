<script>
window.paceOptions = {
    ajax: false,
    restartOnRequestAfter: false,
};
</script>
<script src="<?php echo e(asset('public/js/core.min.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<script src="<?php echo e(asset('public/js/bootstrap.bundle.min.js'), false); ?>"></script>
<script src="<?php echo e(asset('public/js/jqueryTimeago_'.Lang::locale().'.js'), false); ?>"></script>
<script src="<?php echo e(asset('public/js/lazysizes.min.js'), false); ?>" async=""></script>
<script src="<?php echo e(asset('public/js/plyr/plyr.min.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<script src="<?php echo e(asset('public/js/plyr/plyr.polyfilled.min.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<script src="<?php echo e(asset('public/js/app-functions.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<script src="<?php echo e(asset('public/js/smartphoto.min.js'), false); ?>"></script>

<?php if(auth()->guard()->check()): ?>
<script src="https://js.stripe.com/v3/"></script>
<script src='https://checkout.razorpay.com/v1/checkout.js'></script>
<script src='https://js.paystack.co/v1/inline.js'></script>
<?php if(request()->is('my/wallet')): ?>
<script src="<?php echo e(asset('public/js/add-funds.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<?php else: ?>
<script src="<?php echo e(asset('public/js/payment.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<script src="<?php echo e(asset('public/js/payments-ppv.js'), false); ?>?v=<?php echo e($settings->version, false); ?>"></script>
<?php endif; ?>
<?php endif; ?>

<?php if($settings->custom_js): ?>
  <script type="text/javascript">
  <?php echo $settings->custom_js; ?>

  </script>
<?php endif; ?>

<?php if(auth()->guest()
    && ! request()->is('password/reset')
    && ! request()->is('password/reset/*')
    && ! request()->is('contact')
    ): ?>
<script type="text/javascript">

	//<---------------- Login Register ----------->>>>

	_submitEvent = function() {
		  sendFormLoginRegister();
		};

	if (captcha == false) {

	    $(document).on('click','#btnLoginRegister',function(s) {

 		 s.preventDefault();
		 sendFormLoginRegister();

 		 });//<<<-------- * END FUNCTION CLICK * ---->>>>
	}

	function sendFormLoginRegister()
	{
		var element = $(this);
		$('#btnLoginRegister').attr({'disabled' : 'true'});
		$('#btnLoginRegister').find('i').addClass('spinner-border spinner-border-sm align-middle mr-1');

		(function(){
			 $("#formLoginRegister").ajaxForm({
			 dataType : 'json',
			 success:  function(result) {

				 // success
				 if (result.success == true) {

           if (result.isModal && result.isLoginRegister) {
             window.location.reload();
           }

					 if (result.url_return && ! result.isModal) {
					 	window.location.href = result.url_return;
					 }

					 if (result.check_account) {
					 	$('#checkAccount').html(result.check_account).fadeIn(500);

						$('#btnLoginRegister').removeAttr('disabled');
						$('#btnLoginRegister').find('i').removeClass('spinner-border spinner-border-sm align-middle mr-1');
						$('#errorLogin').fadeOut(100);
						$("#formLoginRegister").reset();
					 }

				 }  else {

					 if (result.errors) {

						 var error = '';
						 var $key = '';

					for ($key in result.errors) {
							 error += '<li><i class="far fa-times-circle"></i> ' + result.errors[$key] + '</li>';
						 }

						 $('#showErrorsLogin').html(error);
						 $('#errorLogin').fadeIn(500);
						 $('#btnLoginRegister').removeAttr('disabled');
						 $('#btnLoginRegister').find('i').removeClass('spinner-border spinner-border-sm align-middle mr-1');
					 }
				 }

				},
				error: function(responseText, statusText, xhr, $form) {
						// error
						$('#btnLoginRegister').removeAttr('disabled');
						$('#btnLoginRegister').find('i').removeClass('spinner-border spinner-border-sm align-middle mr-1');
						swal({
								type: 'error',
								title: error_oops,
								text: error_occurred+' ('+xhr+')',
							});
				}
			}).submit();
		})(); //<--- FUNCTION %
	}// End function sendFormLoginRegister
</script>
<?php endif; ?>
<?php /**PATH /home/allajwno/public_html/resources/views/includes/javascript_general.blade.php ENDPATH**/ ?>