

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('public/plugins/iCheck/all.css'), false); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('public/plugins/tagsinput/jquery.tagsinput.min.css'), false); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo e(asset('public/plugins/select2/select2.min.css'), false); ?>?v=<?php echo e($settings->version, false); ?>" rel="stylesheet" type="text/css" />
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h4>
            <?php echo e(trans('admin.admin'), false); ?>

            	<i class="fa fa-angle-right margin-separator"></i>
            		<?php echo e(trans('admin.general_settings'), false); ?>

          </h4>
        </section>

        <!-- Main content -->
        <section class="content">

        	 <?php if(Session::has('success_message')): ?>
		    <div class="alert alert-success">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
								</button>
		       <i class="fa fa-check margin-separator"></i> <?php echo e(Session::get('success_message'), false); ?>

		    </div>
		<?php endif; ?>

        	<div class="content">

        		<div class="row">

        	<div class="box">
                <div class="box-header">
                  <h3 class="box-title"><?php echo e(trans('admin.general_settings'), false); ?></h3>
                </div><!-- /.box-header -->

                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?php echo e(url('panel/admin/settings'), false); ?>" enctype="multipart/form-data">

                	<input type="hidden" name="_token" value="<?php echo e(csrf_token(), false); ?>">

					<?php echo $__env->make('errors.errors-forms', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                 <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.name_site'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->title, false); ?>" name="title" class="form-control" placeholder="<?php echo e(trans('admin.title'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.email_admin'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->email_admin, false); ?>" name="email_admin" class="form-control" placeholder="<?php echo e(trans('admin.email_admin'), false); ?>">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.link_terms'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->link_terms, false); ?>" name="link_terms" class="form-control" placeholder="https://yousite.com/p/terms">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.link_privacy'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->link_privacy, false); ?>" name="link_privacy" class="form-control" placeholder="https://yousite.com/p/privacy">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.link_cookies'), false); ?></label>
                      <div class="col-sm-10">
                        <input type="text" value="<?php echo e($settings->link_cookies, false); ?>" name="link_cookies" class="form-control" placeholder="https://yousite.com/p/cookies">
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('admin.date_format'), false); ?></label>
                     <div class="col-sm-10">
                       <select name="date_format" class="form-control">
                         <option <?php if( $settings->date_format == 'M d, Y' ): ?> selected="selected" <?php endif; ?> value="M d, Y"><?php echo date('M d, Y'); ?></option>
                           <option <?php if( $settings->date_format == 'd M, Y' ): ?> selected="selected" <?php endif; ?> value="d M, Y"><?php echo date('d M, Y'); ?></option>
                         <option <?php if( $settings->date_format == 'Y-m-d' ): ?> selected="selected" <?php endif; ?> value="Y-m-d"><?php echo date('Y-m-d'); ?></option>
                           <option <?php if( $settings->date_format == 'm/d/Y' ): ?> selected="selected" <?php endif; ?>  value="m/d/Y"><?php echo date('m/d/Y'); ?></option>
                             <option <?php if( $settings->date_format == 'd/m/Y' ): ?> selected="selected" <?php endif; ?>  value="d/m/Y"><?php echo date('d/m/Y'); ?></option>
                         </select>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.genders'), false); ?></label>
                      <div class="col-sm-10">
                        <select name="genders[]" multiple="multiple" class="form-control custom-select select2Multiple">
                          <option <?php if(in_array('male', $genders)): ?> selected="selected" <?php endif; ?> value="male"><?php echo e(__('general.male'), false); ?></option>
                          <option <?php if(in_array('female', $genders)): ?> selected="selected" <?php endif; ?> value="female"><?php echo e(__('general.female'), false); ?></option>
                          <option <?php if(in_array('gay', $genders)): ?> selected="selected" <?php endif; ?> value="gay"><?php echo e(__('general.gay'), false); ?></option>
                          <option <?php if(in_array('lesbian', $genders)): ?> selected="selected" <?php endif; ?> value="lesbian"><?php echo e(__('general.lesbian'), false); ?></option>
                          <option <?php if(in_array('bisexual', $genders)): ?> selected="selected" <?php endif; ?> value="bisexual"><?php echo e(__('general.bisexual'), false); ?></option>
                          <option <?php if(in_array('transgender', $genders)): ?> selected="selected" <?php endif; ?> value="transgender"><?php echo e(__('general.transgender'), false); ?></option>
                          <option <?php if(in_array('metrosexual', $genders)): ?> selected="selected" <?php endif; ?> value="metrosexual"><?php echo e(__('general.metrosexual'), false); ?></option>
                        </select>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('general.show_errors'), false); ?></label>
                     <div class="col-sm-10">
                       <div class="radio">
                       <label class="padding-zero">
                         <input type="radio" value="true" name="app_debug" <?php if(env('APP_DEBUG') == true): ?> checked="checked" <?php endif; ?> checked>
                         On (<?php echo e(trans('general.info_show_errors'), false); ?>)
                       </label>
                     </div>
                     <div class="radio">
                       <label class="padding-zero">
                         <input type="radio" value="false" name="app_debug" <?php if(env('APP_DEBUG') == false): ?> checked="checked" <?php endif; ?>>
                         Off
                       </label>
                     </div>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                 <!-- Start Box Body -->
                 <div class="box-body">
                   <div class="form-group">
                     <label class="col-sm-2 control-label"><?php echo e(trans('admin.account_verification'), false); ?></label>
                     <div class="col-sm-10">

                       <div class="radio">
                       <label class="padding-zero">
                         <input type="radio" name="account_verification" <?php if( $settings->account_verification == '1' ): ?> checked="checked" <?php endif; ?> value="1" checked>
                         <?php echo e(trans('general.yes'), false); ?>

                       </label>
                     </div>

                     <div class="radio">
                       <label class="padding-zero">
                         <input type="radio" name="account_verification" <?php if( $settings->account_verification == '0' ): ?> checked="checked" <?php endif; ?> value="0">
                         <?php echo e(trans('general.no'), false); ?>

                       </label>
                     </div>
                     </div>
                   </div>
                 </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Captcha</label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="captcha" <?php if( $settings->captcha == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="captcha" <?php if( $settings->captcha == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.new_registrations'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="registration_active" <?php if( $settings->registration_active == '1' ): ?> checked="checked" <?php endif; ?> value="1" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="registration_active" <?php if( $settings->registration_active == '0' ): ?> checked="checked" <?php endif; ?> value="0">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('admin.email_verification'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="email_verification" <?php if( $settings->email_verification == '1' ): ?> checked="checked" <?php endif; ?> value="1" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="email_verification" <?php if( $settings->email_verification == '0' ): ?> checked="checked" <?php endif; ?> value="0">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.show_counter'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="radio">
                          <label class="padding-zero">
                            <input type="radio" name="show_counter" <?php if($settings->show_counter == 'on'): ?> checked="checked" <?php endif; ?> value="on">
                            <?php echo e(trans('general.yes'), false); ?>

                          </label>
                        </div>

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="show_counter" <?php if($settings->show_counter == 'off'): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.show_widget_creators'), false); ?></label>
                      <div class="col-sm-10">

                        <div class="radio">
                          <label class="padding-zero">
                            <input type="radio" name="widget_creators_featured" <?php if($settings->widget_creators_featured == 'on'): ?> checked="checked" <?php endif; ?> value="on">
                            <?php echo e(trans('general.yes'), false); ?>

                          </label>
                        </div>

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="widget_creators_featured" <?php if($settings->widget_creators_featured == 'off'): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.show_earnings_simulator'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="earnings_simulator" <?php if( $settings->earnings_simulator == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="earnings_simulator" <?php if( $settings->earnings_simulator == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.receive_verification_requests'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="requests_verify_account" <?php if( $settings->requests_verify_account == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="requests_verify_account" <?php if( $settings->requests_verify_account == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.hide_admin_profile'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="hide_admin_profile" <?php if( $settings->hide_admin_profile == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="hide_admin_profile" <?php if( $settings->hide_admin_profile == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.watermark_on_images'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="watermark" <?php if( $settings->watermark == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="watermark" <?php if( $settings->watermark == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.show_alert_adult'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="alert_adult" <?php if( $settings->alert_adult == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="alert_adult" <?php if( $settings->alert_adult == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.who_can_see_content'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="who_can_see_content" <?php if( $settings->who_can_see_content == 'all' ): ?> checked="checked" <?php endif; ?> value="all" checked>
                          <?php echo e(trans('general.all'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="who_can_see_content" <?php if( $settings->who_can_see_content == 'users' ): ?> checked="checked" <?php endif; ?> value="users">
                          <?php echo e(trans('admin.only_users'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.users_can_edit_post'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="users_can_edit_post" <?php if( $settings->users_can_edit_post == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="users_can_edit_post" <?php if( $settings->users_can_edit_post == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <!-- Start Box Body -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-2 control-label"><?php echo e(trans('general.disable_wallet'), false); ?></label>
                      <div class="col-sm-10">

                      	<div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="disable_wallet" <?php if( $settings->disable_wallet == 'on' ): ?> checked="checked" <?php endif; ?> value="on" checked>
                          <?php echo e(trans('general.yes'), false); ?>

                        </label>
                      </div>

                      <div class="radio">
                        <label class="padding-zero">
                          <input type="radio" name="disable_wallet" <?php if( $settings->disable_wallet == 'off' ): ?> checked="checked" <?php endif; ?> value="off">
                          <?php echo e(trans('general.no'), false); ?>

                        </label>
                      </div>
                      </div>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-success"><?php echo e(trans('admin.save'), false); ?></button>
                  </div><!-- /.box-footer -->
                </form>
              </div>
        		</div><!-- /.row -->
        	</div><!-- /.content -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/allajwno/public_html/backup/resources/views/admin/settings.blade.php ENDPATH**/ ?>