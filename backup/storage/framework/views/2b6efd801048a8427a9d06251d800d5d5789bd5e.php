<div class="progress-wrapper px-3 px-lg-0 display-none mb-3" id="progress">
    <div class="progress-info">
      <div class="progress-percentage">
        <span class="percent">0%</span>
      </div>
    </div>
    <div class="progress progress-xs">
      <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
    </div>
  </div>

      <form method="POST" action="<?php echo e(url('update/create'), false); ?>" enctype="multipart/form-data" id="formUpdateCreate">
        <?php echo csrf_field(); ?>
      <div class="card mb-4 card-form-post">
        <div class="blocked display-none"></div>
        <div class="card-body pb-0">

          <div class="media">
          <span class="rounded-circle mr-3">
      				<img src="<?php echo e(Helper::getFile(config('path.avatar').auth()->user()->avatar), false); ?>" class="rounded-circle avatarUser" width="60" height="60">
      		</span>

          <div class="media-body">
            <textarea name="description" id="updateDescription" data-post-length="<?php echo e($settings->update_length, false); ?>" rows="4" cols="40" placeholder="<?php echo e(trans('general.write_something'), false); ?>" class="form-control textareaAutoSize border-0"></textarea>
          </div>
        </div><!-- media -->

            <input class="custom-control-input d-none" id="customCheckLocked" type="checkbox" <?php echo e(auth()->user()->post_locked == 'yes' ? 'checked' : '', false); ?> name="locked" value="yes">

          <!-- Alert -->
          <div class="alert alert-danger my-3 display-none" id="errorUdpate">
           <ul class="list-unstyled m-0" id="showErrorsUdpate"></ul>
         </div><!-- Alert -->

        </div>
        <div class="card-footer bg-white border-0 pt-0">
          <div class="justify-content-between align-items-center">

            <div class="form-group display-none" id="price" >
              <div class="input-group mb-2">
              <div class="input-group-prepend">
                <span class="input-group-text"><?php echo e($settings->currency_symbol, false); ?></span>
              </div>
                  <input class="form-control isNumber" autocomplete="off" name="price" placeholder="<?php echo e(trans('general.price'), false); ?>" type="text">
              </div>
            </div><!-- End form-group -->

            <div class="w-100">
              <span id="previewImage"></span>
              <a href="javascript:void(0)" id="removePhoto" class="text-danger p-1 px-2 display-none btn-tooltip" data-toggle="tooltip" data-placement="top" title="<?php echo e(trans('general.delete'), false); ?>"><i class="fa fa-times-circle"></i></a>
            </div>

            <input type="file" name="photo" id="filePhoto" accept="image/*,video/mp4,video/x-m4v,video/quicktime,audio/mp3" class="visibility-hidden">

            <button type="button" class="btn btn-upload btn-tooltip e-none align-bottom <?php if(auth()->user()->dark_mode == 'off'): ?> text-primary <?php else: ?> text-white <?php endif; ?> rounded-pill" data-toggle="tooltip" data-placement="top" title="<?php echo e(trans('general.upload_media'), false); ?>" onclick="$('#filePhoto').trigger('click')">
              <i class="feather icon-image f-size-25"></i>
            </button>

            <input type="file" name="zip" id="fileZip" accept="application/x-zip-compressed" class="visibility-hidden">

            <button type="button" class="btn btn-upload btn-tooltip e-none align-bottom <?php if(auth()->user()->dark_mode == 'off'): ?> text-primary <?php else: ?> text-white <?php endif; ?> rounded-pill" data-toggle="tooltip" data-placement="top" title="<?php echo e(trans('general.upload_file_zip'), false); ?>" onclick="$('#fileZip').trigger('click')">
              <i class="bi bi-file-earmark-zip f-size-25"></i>
            </button>

            <button type="button" id="setPrice" class="btn btn-upload btn-tooltip e-none align-bottom <?php if(auth()->user()->dark_mode == 'off'): ?> text-primary <?php else: ?> text-white <?php endif; ?> rounded-pill" data-toggle="tooltip" data-placement="top" title="<?php echo e(trans('general.set_price_for_post'), false); ?>">
              <i class="feather icon-tag f-size-25"></i>
            </button>

            <button type="button" id="contentLocked" class="btn btn-upload btn-tooltip e-none align-bottom <?php if(auth()->user()->dark_mode == 'off'): ?> text-primary <?php else: ?> text-white <?php endif; ?> rounded-pill <?php echo e(auth()->user()->post_locked == 'yes' ? '' : 'unlock', false); ?>" data-toggle="tooltip" data-placement="top" title="<?php echo e(trans('users.locked_content'), false); ?>">
              <i class="feather icon-<?php echo e(auth()->user()->post_locked == 'yes' ? '' : 'un', false); ?>lock f-size-25"></i>
            </button>

            <div class="d-inline-block float-right mt-3">
              <button type="submit" disabled class="btn btn-sm btn-primary rounded-pill float-right e-none" data-empty="<?php echo e(trans('general.empty_post'), false); ?>" data-error="<?php echo e(trans('general.error'), false); ?>" data-msg-error="<?php echo e(trans('general.error_internet_disconnected'), false); ?>" id="btnCreateUpdate">
                <i></i> <?php echo e(trans('general.publish'), false); ?>

              </button>

              <div id="the-count" class="float-right my-2 mr-2">
                <small id="maximum"><?php echo e($settings->update_length, false); ?></small>
              </div>
            </div>

          </div>
        </div><!-- card footer -->
      </div><!-- card -->
    </form>
<?php /**PATH /home/allajwno/public_html/resources/views/includes/form-post.blade.php ENDPATH**/ ?>