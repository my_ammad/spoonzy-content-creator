$( document ).ready(function() {
    $('video.post-videos').bind('play', function (e) {
        PostAuthor = $(this).data('userId');
        let post_id = $(this).data('id');
        let media_type = $(this).data('type');
        let media_link = $(this).find('Source:first').attr('src');
        inspectPost(post_id, 'Video', media_link)
    });
})

function inspectPost(post_id, media_type, media_link, userId){
    //console.log(post_id + type + link);
    PostAuthor = userId;

    axios.post('/ajax/post_inspection', {
        post_id: post_id,
        media_type: media_type,
        media_link: media_link
    }).then(res => {
        console.log(res);
    });
}
