@extends('layouts.app')

@section('title')Single Post Inspection -@endsection

@section('content')
    <section class="section section-sm">
        <div class="container">
            <div class="row justify-content-center text-center mb-sm">
                <div class="col-lg-8 py-5">
                    <h2 class="mb-0 font-montserrat">
                        <i class="far fa-eye mr-2"></i> Post Inspection
                    </h2>
                    <p class="lead text-muted mt-0">See Who Viewed Your Post</p>
                </div>
            </div>
            <div class="row">

                @include('includes.cards-settings')

                <div class="col-md-6 col-lg-9 mb-5 mb-lg-0">

                    @if (session('status'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>

                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card mb-3 card-updates">
                        <div class="card-body">
                            <table class="table table-hover table-striped table-bordered">
                                <tbody>

                                @if( $data->total() !=  0 && $data->count() != 0 )
                                    <tr>
                                        <th class="active">#</th>
                                        {{--                                        <th class="active">Media</th>--}}
                                        <th class="active">Details</th>
                                        <th class="active">Author</th>
                                        <th class="active">Date</th>
                                    </tr>

                                    @foreach( $data as $item )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td style="font-weight:600">The <a target="_blank"
                                                                               href="{{ $item->media_link }}">{{ $item->media_type }}</a>
                                                from the
                                                <a target="_blank" href="{{ $item->post }}">post</a> is viewed by
                                                <a target="_blank"
                                                   href="{{ url($item->username) }}">{{ $item->user_fullname }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ url($item->post_author_username) }}">{{ $item->post_author_fullname }}</a>
                                            </td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr><!-- /.TR -->
                                    @endforeach

                                @else
                                    <hr/>
                                    <h3 class="text-center no-found">{{ trans('general.no_results_found') }}</h3>

                                    @if( isset( $query ) )
                                        <div class="col-md-12 text-center padding-bottom-15">
                                            <a href="{{url('panel/admin/post_inspection')}}"
                                               class="btn btn-sm btn-danger">{{ trans('auth.back') }}</a>
                                        </div>

                                    @endif
                                @endif

                                </tbody>
                            </table>
                        </div>

                        @if($data->hasPages())
                            {{ $data->links() }}
                        @endif
                    </div>

                </div><!-- end col-md-6 -->

            </div>
        </div>
    </section>

@endsection
