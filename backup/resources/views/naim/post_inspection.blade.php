@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h4>
                {{ trans('admin.admin') }} <i
                    class="fa fa-angle-right margin-separator"></i> Post Inspections ({{$data->total()}})
            </h4>

        </section>

        <!-- Main content -->
        <section class="content">

            @if(Session::has('info_message'))
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="fa fa-warning margin-separator"></i> {{ Session::get('info_message') }}
                </div>
            @endif

            @if(Session::has('success_message'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <i class="fa fa-check margin-separator"></i> {{ Session::get('success_message') }}
                </div>
            @endif

            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                Post Inspections
                            </h3>

                        </div><!-- /.box-header -->

                        <div class="box-body table-responsive">

                            <table class="table table-hover table-striped table-bordered">
                                <tbody>

                                @if( $data->total() !=  0 && $data->count() != 0 )
                                    <tr>
                                        <th class="active">#</th>
{{--                                        <th class="active">Media</th>--}}
                                        <th class="active">Details</th>
                                        <th class="active">Author</th>
                                        <th class="active">Date</th>
                                    </tr>

                                    @foreach( $data as $item )
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
{{--                                            <td>--}}
{{--                                                <a href="{{ $item->post }}">--}}
{{--                                                @if($item->media_type == 'Image')--}}
{{--                                                    <img src="{{ $item->media_link }}" style="max-width: 70px;max-height: 50px" alt="image">--}}
{{--                                                @else--}}
{{--                                                    <img src="{{ asset('public/naim/img/video_thumb.jpg') }}" style="max-width: 70px;max-height: 50px" alt="video">--}}
{{--                                                @endif--}}
{{--                                                </a>--}}
{{--                                            </td>--}}
                                            <td style="font-weight:600">The <a target="_blank" href="{{ $item->media_link }}">{{ $item->media_type }}</a> from the
                                                <a target="_blank" href="{{ $item->post }}">post</a> is viewed by
                                                <a target="_blank" href="{{ url($item->username) }}">{{ $item->user_fullname }}</a>
                                            </td>
                                            <td>
                                                <a href="{{ url($item->post_author_username) }}">{{ $item->post_author_fullname }}</a>
                                            </td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr><!-- /.TR -->
                                    @endforeach

                                @else
                                    <hr/>
                                    <h3 class="text-center no-found">{{ trans('general.no_results_found') }}</h3>

                                    @if( isset( $query ) )
                                        <div class="col-md-12 text-center padding-bottom-15">
                                            <a href="{{url('panel/admin/post_inspection')}}"
                                               class="btn btn-sm btn-danger">{{ trans('auth.back') }}</a>
                                        </div>

                                    @endif
                                @endif

                                </tbody>

                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    @if ($data->hasPages())
                        {{ $data->links() }}
                    @endif
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection
