<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wise extends Model {

	protected $guarded = array();
	public $timestamps = false;
}
