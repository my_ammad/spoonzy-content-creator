<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StripeAccount extends Model
{

    use SoftDeletes;

    protected $fillable = [
        "user_type", "user_id", "account_id", "customer_id", "charges_enabled"
    ];


    public function user() {
        return $this->morphTo();
    }
}