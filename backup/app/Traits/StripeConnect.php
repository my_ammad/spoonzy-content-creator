<?php


namespace App\Traits;

use Stripe\Customer;
use Stripe\Account;
use Stripe\Stripe;
use Stripe\AccountLink;
use Stripe\StripeClient;
use Stripe\OAuth;
use App\Models\PaymentGateways;
use Illuminate\Support\Facades\DB;
class StripeConnect
{


    public static function prepare()
    {
        $payment = PaymentGateways::whereName('Stripe')->whereEnabled(1)->firstOrFail();
        Stripe::setApiKey($payment->key_secret);
    }

    
    public static function getOrCreateAccount($user, $params = [], $config = [])
    {
        self::prepare();

        $params = array_merge([
            "type" => $config['account_type'] ?? 'standard'
        ], $params);

        return self::create($user, 'account_id', function () use ($params) {
            return Account::create($params);
        });
    }

    
    public static function getOrCreateCustomer($token, $user, $params = [])
    {
        self::prepare();

        $params = array_merge([
            "email" => $user->email,
            'source' => $token,
        ], $params);

        return self::create($user, 'customer_id', function () use ($params) {
            return Customer::create($params);
        });
    }

    public static function deleteAccount( String $account_id) {
        try {
            $stripe = new StripeClient( env("STRIPE_SECRET") );
            $stripe->accounts->delete($account_id, [] );

            return true;
        } catch (\Throwable $th) {
            throw $th;
            return false;
        }
    }


    public static function createAccountLink( $vendorId, $config = [] ) {
        self::prepare();

        $account_links = AccountLink::create([
            'account' => $vendorId,
            'refresh_url' => $config['refresh_url'] ?? '',
            'return_url' => $config['return_url'] ?? '',
            'type' => 'account_onboarding',
          ]);

        return  $account_links;
    }

    public static function getVendor( $vendorId ) {
        self::prepare();

        return Account::retrieve($vendorId);
    }

    public static function getCustomer( $customerId ) {
        self::prepare();

        return Customer::retrieve($customerId);
    }

    public static function disconnectStripeAccount( $account_id ) {
        self::prepare();

        return OAuth::deauthorize([
            'client_id' => 'CLIENT_ID',
            'stripe_user_id' => $account_id,
          ]);
    }


    
    private static function create($user, $id_key, $callback) {
        $vendor = DB::table('stripe_accounts')->where('user_id',$user->id)->first();
        
        if (!$vendor || !$vendor->$id_key) {
            $id = call_user_func($callback)->id;

            if (!$vendor) {
                $vendor = $user->stripeAccount()->create([$id_key => $id,'user_id'=>$user->id]);
            } else {
                $vendor->$id_key = $id;
                $vendor->save();
            }

        }

        return $vendor;
    }

}