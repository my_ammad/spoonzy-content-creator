<?php

namespace App\Http\Controllers;

use App\Models\PostInspection;
use App\Models\Updates;
use Illuminate\Http\Request;

class PostInspectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('naim.post_inspection', [
            'data' => PostInspection::latest()->paginate(20)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $post_author = Updates::find($request->post_id)->user();
        PostInspection::create([
            'post_id' => $request->post_id,
            'username' => $user->username,
            'user_fullname' => is_null($user->name) ? $user->username : $user->name,
            'post' => route('profile', [$post_author->username, $request->post_id]),
            'post_author_username' => $post_author->username,
            'post_author_fullname' => is_null($post_author->name) ? $post_author->username : $post_author->name,
            'media_type' => $request->media_type,
            'media_link' => $request->media_link
        ]);
        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\PostInspection $postInspection
     * @return \Illuminate\Http\Response
     */
    public function show($post_id, $userId)
    {
        abort_if(auth()->user()->id != $userId, 403);
        return view('naim.single_post_inspection', [
            'data' => PostInspection::where('post_id', $post_id)->latest()->paginate(20)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\PostInspection $postInspection
     * @return \Illuminate\Http\Response
     */
    public function edit(PostInspection $postInspection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PostInspection $postInspection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostInspection $postInspection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\PostInspection $postInspection
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostInspection $postInspection)
    {
        //
    }
}
