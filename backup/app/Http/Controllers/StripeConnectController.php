<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\StripeConnect;
use App\Models\StripeAccount;
use Illuminate\Support\Facades\DB;
use App\Models\PaymentGateways;

class StripeConnectController extends Controller
{
    public function startOnBoardProcess() {
        $user = auth()->user();
        $ac=$user->stripeAccount;
        if($ac && isset($ac->account_id)) {
            $payment = PaymentGateways::whereName('Stripe')->whereEnabled(1)->firstOrFail();
            $stripe = new \Stripe\StripeClient($payment->key_secret);
            try {
                $account=$stripe->accounts->retrieve($ac->account_id,[]);
                if(!$account){
                    DB::table('stripe_accounts')->where('user_id',$user->id)->delete();
                    $user->stripe_account_id='';
                    $user->save();
                }
            }catch(\Exception $e) {
                DB::table('stripe_accounts')->where('user_id',$user->id)->delete();
                $user->stripe_account_id='';
                    $user->save();
            }
        }
        try {
            $vendor = StripeConnect::getOrCreateAccount($user, ["email" => $user->email]);
            $getAccountLink = StripeConnect::createAccountLink($vendor->account_id, [
                "return_url" => url()->current()."/$vendor->account_id/return",
                "refresh_url" => url()->previous()
            ]);

            return redirect($getAccountLink->url);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function returnFromOnBoardProcess(Request $request, $vendor_id) {

        $vendor = StripeConnect::getVendor($vendor_id);
        $account = StripeAccount::where("account_id", $vendor->id)->first();
        $account->charges_enabled = (int) $vendor->charges_enabled;
        $account->save();

        if ( $account->charges_enabled ) {
            $user=auth()->user();
            $user->stripe_account_id=$account->account_id;
            $user->payment_gateway='Stripe';
            $user->save();
            \Session::flash('status', "Stripe connect account is successfuly added!");
        } else if ( $vendor->details_submitted === false ) {
            \Session::flash('error_msg', "Please try again, You haven't complete the stripe connect process!");
        } else {
            \Session::flash('info', 'Stripe connect account is under review!');
        }

        return redirect('settings/payout/method');
    }

    public function destroy( $account_id ) {
        try {
            DB::beginTransaction();

            $user = auth()->user();
            $user->stripeAccount->where('account_id', $account_id)->delete();

            $user->plans()->where('is_offline', 0)->delete();

            DB::commit();

            return redirect()->back()->with(['success' => 'Successfully disconnected your stripe account!']);
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
            return redirect()->back()->with(['error' => 'Stripe account failed to disconnect!']);
        }
    }
}