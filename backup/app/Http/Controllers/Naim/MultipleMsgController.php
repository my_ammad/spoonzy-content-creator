<?php

namespace App\Http\Controllers\Naim;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Models\AdminSettings;
use App\Models\Conversations;
use App\Models\Messages;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class MultipleMsgController extends Controller
{
    public function mutipleMsgBox($ids)
    {
        $selectedUsers = User::whereIn('id', explode("+", $ids))->get();
        return view('naim.multiple_msg_box', compact('selectedUsers', 'ids'));
    }

    public function sendMutipleMsg(Request $req)
    {
        if (!Auth::check()) {
            return response()->json(array('session_null' => true));
        }

        $settings = AdminSettings::first();
        $path = config('path.messages');
        $users = explode("+", $req->ids);

        foreach ($users as $id){
            $this->sendMsg($req, $id, $settings, $path);
        }

        \Session::flash('success_message', "Messaages sent successfully to the selected users!");
        $route = route('messages');
        return ['redirectTo' => $route];
    }

    protected function sendMsg(Request $request, $id, $settings, $path)
    {
        $sizeAllowed = $settings->file_size_allowed * 1024;
        $dimensions = explode('x', $settings->min_width_height_image);

        // Find user in Database
        $user = User::findOrFail($id);

        if ($request->hasFile('photo')) {

            $requiredMessage = null;

            $originalExtension = strtolower($request->file('photo')->getClientOriginalExtension());
            $getMimeType = $request->file('photo')->getMimeType();

            if ($originalExtension == 'mp3' && $getMimeType == 'application/octet-stream') {
                $audio = ',application/octet-stream';
            } else {
                $audio = null;
            }

            if ($originalExtension == 'mp4'
                || $originalExtension == 'mov'
                || $originalExtension == 'mp3'
            ) {
                $isImage = null;
            } else {
                $isImage = '|dimensions:min_width=' . $dimensions[0] . '';
            }
        } else {
            $isImage = null;
            $audio = null;
            $originalExtension = null;
            $requiredMessage = 'required|';
        }

        if ($request->hasFile('zip')) {
            $requiredMessage = null;
        }

        // Currency Position
        if ($settings->currency_position == 'right') {
            $currencyPosition = 2;
        } else {
            $currencyPosition = null;
        }

        $messages = [
            "required" => trans('validation.required'),
            "message.max" => trans('validation.max.string'),
            'photo.dimensions' => trans('general.validate_dimensions'),
            'photo.mimetypes' => trans('general.formats_available'),
            'price.min' => trans('general.amount_minimum' . $currencyPosition, ['symbol' => $settings->currency_symbol, 'code' => $settings->currency_code]),
            'price.max' => trans('general.amount_maximum' . $currencyPosition, ['symbol' => $settings->currency_symbol, 'code' => $settings->currency_code]),
        ];

        // Setup the validator
        $rules = [
            'photo' => 'mimetypes:image/jpeg,image/gif,image/png,video/mp4,video/quicktime,audio/mpeg,video/3gpp' . $audio . '|max:' . $settings->file_size_allowed . ',' . $isImage . '',
            'message' => $requiredMessage . '|min:1|max:' . $settings->comment_length . '',
            'zip' => 'mimes:zip|max:' . $settings->file_size_allowed . '',
            'price' => 'numeric|min:' . $settings->min_ppv_amount . '|max:' . $settings->max_ppv_amount,
        ];

        $validator = Validator::make($request->all(), $rules, $messages);


        // Validate the input and return correct response
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
            ));
        }

        // Upload File Zip
        if ($request->hasFile('zip')) {

            $fileZip = $request->file('zip');
            $extension = $fileZip->getClientOriginalExtension();
            $size = Helper::formatBytes($fileZip->getSize(), 1);
            $originalName = Helper::fileNameOriginal($fileZip->getClientOriginalName());
            $file = strtolower(Auth::user()->id . time() . Str::random(20) . '.' . $extension);
            $format = 'zip';

            $fileZip->storePubliclyAs($path, $file);

        }

        //============= Upload Media
        if ($request->hasFile('photo') && $isImage != null) {

            $photo = $request->file('photo');
            $extension = $photo->getClientOriginalExtension();
            $mimeType = $request->file('photo')->getMimeType();
            $widthHeight = getimagesize($photo);
            $file = strtolower(Auth::user()->id . time() . Str::random(20) . '.' . $extension);
            $size = Helper::formatBytes($request->file('photo')->getSize(), 1);
            $format = 'image';
            $originalName = $request->file('photo')->getClientOriginalName();
            $url = ucfirst(Helper::urlToDomain(url('/')));

            set_time_limit(0);
            ini_set('memory_limit', '512M');

            if ($extension == 'gif' && $mimeType == 'image/gif') {
                $request->file('photo')->storePubliclyAs($path, $file);
            } else {
                //=============== Image Large =================//
                $img = Image::make($photo);

                $width = $img->width();
                $height = $img->height();
                $max_width = $width < $height ? 800 : 1400;

                if ($width > $max_width) {
                    $scale = $max_width;
                } else {
                    $scale = $width;
                }

                // Calculate font size
                if ($width >= 400 && $width < 900) {
                    $fontSize = 16;
                } elseif ($width >= 800 && $width < 1200) {
                    $fontSize = 20;
                } elseif ($width >= 1200 && $width < 2000) {
                    $fontSize = 24;
                } elseif ($width >= 2000) {
                    $fontSize = 32;
                } else {
                    $fontSize = 0;
                }

                if ($settings->watermark == 'on') {
                    $imageResize = $img->orientate()->resize($scale, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->text($url . '/' . auth()->user()->username, $img->width() - 20, $img->height() - 10, function ($font)
                    use ($fontSize) {
                        $font->file(public_path('webfonts/arial.TTF'));
                        $font->size($fontSize);
                        $font->color('#eaeaea');
                        $font->align('right');
                        $font->valign('bottom');
                    })->encode($extension);
                } else {
                    $imageResize = $img->orientate()->resize($scale, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->encode($extension);
                }


                // Storage Image
                Storage::put($path . $file, $imageResize, 'public');
            }

        }//<====== End Upload Image

        //<----------- UPLOAD VIDEO
        if ($request->hasFile('photo')
            && $isImage == null
            && $originalExtension == 'mp4'
            || $originalExtension == 'mov'
        ) {

            $extension = $request->file('photo')->getClientOriginalExtension();
            $file = strtolower(Auth::user()->id . time() . Str::random(20) . '.' . $extension);
            $size = Helper::formatBytes($request->file('photo')->getSize(), 1);
            $format = 'video';
            $originalName = $request->file('photo')->getClientOriginalName();
            set_time_limit(0);

            //======= Storage Video
            $request->file('photo')->storePubliclyAs($path, $file);

        }//<====== End UPLOAD VIDEO

        //<----------- UPLOAD MUSIC
        if ($request->hasFile('photo')
            && $isImage == null
            && $originalExtension == 'mp3'
        ) {

            $extension = $request->file('photo')->getClientOriginalExtension();
            $file = strtolower(Auth::user()->id . time() . Str::random(20) . '.' . $extension);
            $size = Helper::formatBytes($request->file('photo')->getSize(), 1);
            $format = 'music';
            $originalName = $request->file('photo')->getClientOriginalName();
            set_time_limit(0);

            //======= Storage Video
            $request->file('photo')->storePubliclyAs($path, $file);

        }//<====== End UPLOAD MUSIC

        // Verify Conversation Exists
        $conversation = Conversations::where('user_1', Auth::user()->id)
            ->where('user_2', $id)
            ->orWhere('user_1', $id)
            ->where('user_2', Auth::user()->id)->first();

        $time = Carbon::now();

        if (!isset($conversation)) {
            $newConversation = new Conversations;
            $newConversation->user_1 = Auth::user()->id;
            $newConversation->user_2 = $id;
            $newConversation->updated_at = $time;
            $newConversation->save();

            $conversationID = $newConversation->id;

        } else {
            $conversation->updated_at = $time;
            $conversation->save();

            $conversationID = $conversation->id;
        }

        if ($request->hasFile('photo') || $request->hasFile('zip')) {
            $message = new Messages;
            $message->conversations_id = $conversationID;
            $message->from_user_id = Auth::user()->id;
            $message->to_user_id = $id;
            $message->message = trim(Helper::checkTextDb($request->get('message')));
            $message->file = $file;
            $message->original_name = $originalName;
            $message->format = $format;
            $message->size = $size;
            $message->updated_at = $time;
            $message->price = $request->price;
            $message->save();

            return response()->json(array(
                'success' => true,
                'last_id' => $message->id,
            ), 200);
        }

        if ($request->get('message')) {
            $message = new Messages;
            $message->conversations_id = $conversationID;
            $message->from_user_id = Auth::user()->id;
            $message->to_user_id = $id;
            $message->message = trim(Helper::checkTextDb($request->get('message')));
            $message->updated_at = $time;
            $message->price = $request->price;
            $message->save();

            return response()->json(array(
                'success' => true,
                'last_id' => $message->id,
            ), 200);
        }
    }//<<--- End Method send()

}
