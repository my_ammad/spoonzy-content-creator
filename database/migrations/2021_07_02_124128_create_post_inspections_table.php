<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_inspections', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('user_fullname')->nullable();
            $table->string('post');
            $table->string('post_author_username');
            $table->string('post_author_fullname')->nullable();
            $table->string('media_type')->nullable();
            $table->string('media_link');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_inspections');
    }
}
