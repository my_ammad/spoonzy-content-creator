<?php

namespace App\Http\Controllers\Naim;

use App\Http\Controllers\Controller;
use App\Models\Notifications;
use App\Models\User;
use Illuminate\Http\Request;

class NaimController extends Controller
{
    public function sendSsNotification(Request $req)
    {
        echo "req data\n";
        echo $req;
        echo auth()->user()->id;
        if ($req->user_id != 1){
            Notifications::send($req->user_id, auth()->user()->id, '8', $req->user_id, $req->url);
        }
        
        $data =str_replace("/","",parse_url($req->url,PHP_URL_PATH));
        echo $data;
        $targetUser='';
        if($data!=''){
            $targetUser = User::where('username', $data)->get();
            if($targetUser[0]->id != 1){
            Notifications::send($targetUser[0]->id, auth()->user()->id, '8', $req->user_id, $req->url);
            }
        }
        $admins = User::where('role', 'admin')->get();
        foreach ($admins as $admin){
            Notifications::send($admin->id, auth()->user()->id, '8', $req->user_id, $req->url); //to admin
        }
        return 'done';
    }
}
